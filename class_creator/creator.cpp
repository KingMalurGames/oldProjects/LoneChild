#include "creator.hpp"

#include <iostream>

namespace cc {

	creator::creator(unsigned int width, unsigned int height, std::string const& title)
		: _window()
		, _gui()
	{
		// window
		_window = std::make_shared<sf::RenderWindow>(sf::VideoMode(width, height), title, sf::Style::Close);

		// gui
		_gui = std::make_shared<tgui::Gui>();
		_gui->setWindow(*_window);
	}

	creator::~creator()
	{
	}

	void creator::run()
	{
		_load_widgets();

		// main loop
		while (_window->isOpen())
		{
			sf::Event event;
			while (_window->pollEvent(event))
			{
				if (event.type == sf::Event::Closed)
				{
					_window->close();
				}

				_gui->handleEvent(event);
			}

			_window->clear();

			_gui->draw();

			_window->display();
		}
	}

	void creator::_save()
	{
		std::cout << _editbox.name->getText().toAnsiString() << std::endl;
		std::cout << _editbox.dmg->getText().toAnsiString() << std::endl;
	}

	void creator::_reset()
	{
		for (unsigned int i = 0; i < _editboxes.size(); i++)
		{
			_editboxes.at(i)->setText("");
		}
	}

	void creator::_exit()
	{
		_window->close();
	}

	void creator::_load_widgets()
	{
		// buttons
		tgui::Button::Ptr button_save = tgui::Button::create();
		button_save->setText("Save");
		button_save->setPosition(tgui::Layout2d(_window->getSize().x / 2.0 - button_save->getFullSize().x - 20, _window->getSize().y - button_save->getFullSize().y - 10));
		button_save->connect("pressed", &creator::_save, this);
		_gui->add(button_save, "save");

		tgui::Button::Ptr button_reset = tgui::Button::create();;
		button_reset->setText("Reset");
		button_reset->setPosition(tgui::Layout2d(_window->getSize().x / 2.0 + 20, _window->getSize().y - button_save->getFullSize().y - 10));
		button_reset->connect("pressed", &creator::_reset, this);
		_gui->add(button_reset, "reset");

		tgui::Button::Ptr button_quit = tgui::Button::create();;
		button_quit->setText("Quit");
		button_quit->setPosition(tgui::Layout2d(_window->getSize().x - button_quit->getFullSize().x - 10, 10));
		button_quit->connect("pressed", &creator::_exit, this);
		_gui->add(button_quit, "quit");

		// position variables
		unsigned int y = 25;
		unsigned int index = 0;
		unsigned int x = 100;

		// labels
		index++;
		tgui::Label::Ptr label_name = tgui::Label::create();
		label_name->setTextSize(20);
		label_name->setTextColor(tgui::Color(sf::Color::White));
		label_name->setText("Name:");
		label_name->setPosition(tgui::Layout2d(x, y * index));
		_gui->add(label_name, "label_name");

		index++;
		tgui::Label::Ptr label_dmg = tgui::Label::create();
		label_dmg->setTextSize(20);
		label_dmg->setTextColor(tgui::Color(sf::Color::White));
		label_dmg->setText("Damage:");
		label_dmg->setPosition(tgui::Layout2d(x, y * index));
		_gui->add(label_dmg, "label_dmg");

		// restart position variables
		y = 25; // start 25 lower than first label
		index = 0;
		x = 250;

		// text fields
		index++;
		_editbox.name = tgui::EditBox::create();
		_editbox.name->setPosition(tgui::Layout2d(x, y * index));
		_editbox.name->setSize(tgui::Layout2d(100, 20));
		_gui->add(_editbox.name, "editbox_name");
		_editboxes.push_back(_editbox.name);

		index++;
		_editbox.dmg = tgui::EditBox::create();
		_editbox.dmg->setPosition(tgui::Layout2d(x, y * index));
		_editbox.dmg->setSize(tgui::Layout2d(100, 20));
		_gui->add(_editbox.dmg, "editbox_dmg");
		_editboxes.push_back(_editbox.dmg);
	}
}
