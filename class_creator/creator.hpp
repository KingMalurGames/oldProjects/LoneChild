#ifndef __CC__CREATOR__
#define __CC__CREATOR__

#include <string>
#include <memory>
#include <SFML/Graphics.hpp>
#include <TGUI/TGUI.hpp>

namespace cc {

	struct editboxes
	{
		tgui::EditBox::Ptr name;
		tgui::EditBox::Ptr dmg;
	};

	class creator
	{
		public:
			creator(unsigned int width, unsigned int height, std::string const& title);
			~creator();

			void run();

		private:
			std::shared_ptr<sf::RenderWindow> _window;
			std::shared_ptr<tgui::Gui> _gui;
			std::vector<tgui::EditBox::Ptr> _editboxes;

			editboxes _editbox;

			void _save();
			void _reset();
			void _exit();

			void _load_widgets();
	};

}

#endif
