#include "creator.hpp"

#include <memory>

int main()
{
	std::unique_ptr<cc::creator> creator = std::make_unique<cc::creator>(800, 600, "Lone Child Class Creator");

	creator->run();

	return 0;
}
