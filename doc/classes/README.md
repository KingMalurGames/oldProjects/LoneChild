Concept for Classes
===================

Table of Contents:
* Base Class
* Derived Class
* Skill
* Stats

# Base Class
The base class for any rpg class has to have virtual functions for:
* Moving (Depending on classes base stats and skill level defined by character skills)
* Attacking (Depending on skill level definded by equipped weapon)
* Defending (Depending on skill level definded by equipped weapon)
* Healing (Depending on skill level definded by equipped weapon)
* Using Skills (Defined by Skill action)
* Using Items (Consumables)
* Collecting Items (Consumables, Statics)
* Dropping Items (Consumables, Statics)
* Equipping Items (weapons, armour and skins)
* Interacting (with object in front of it)

The base class for any rpg class has to have these members for:
* Character Name
* Level
* List of possible Skills
* Stats struct

# Derived Abstract Classes
The base class for any rpg class has to implement all base virtual functions.
The base class for any rpg class has to have access to the base members.

# Skill
A Skill consists of various information:
* Name
* Stat increase and decrease (eg. mage with close combat lowers his Strength)
* List of Classes that can use it
* Attack Type
* Attack Power
* Defense Power
* Healing Power
* Attack Rate
A Skill consists of various functions:
* Applying effects
* Upgrading

# Stats
The possible stats in Lone Child are:
* Base
  * Strength (affects melee damage)
  * Agility (affects range damage)
  * Intelligence (affects magic damage)
  * Constitution (affects hit points)
  * Defense (affects taken damage)
* Extension
  * _strength (expands melee damage)
  * _agility (expands range damage)
  * Wisdom (expands magic damage)
  * Charisma (affects npc prices)
  * Luck (affects drop rates and critical hits)
