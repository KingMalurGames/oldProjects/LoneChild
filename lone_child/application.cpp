// lc 
#include "definitions.hpp"
#include "application.hpp"
#include "splash.hpp"
#include "main_menu.hpp"

// std
#include <iostream>

// lib

// NEngine

namespace lc {

	application::application()
		: _data(std::make_shared<game_data>())
		, _dt(1.f / 60.f)
		, _dtc()
	{
		// TODO: load base config here
#if _USINGDB
		if (!_data->config.load_db(_data->settings.paths.config_path, _data->settings, _data->input))
#elif _USINGINI
		if (!_data->config.load_ini(_data->settings.paths.config_path, _data->settings, _data->input))
#endif
		{
			lc::utils::log(_LOGPATH, "From application::application: Error occured loading configuration file.. \n Using standard configuration!");
		}

		// create window
		switch (_data->settings.graphics.style)
		{
		case lc::style::Default:
			_data->window.create(sf::VideoMode(_data->settings.graphics.width, _data->settings.graphics.height), _data->settings.general.title, sf::Style::Default, _data->settings.context_settings);
			break;
		case lc::style::Close:
			_data->window.create(sf::VideoMode(_data->settings.graphics.width, _data->settings.graphics.height), _data->settings.general.title, sf::Style::Close, _data->settings.context_settings);
			break;
		case lc::style::Fullscreen:
			_data->window.create(sf::VideoMode(_data->settings.graphics.width, _data->settings.graphics.height), _data->settings.general.title, sf::Style::Fullscreen, _data->settings.context_settings);
			break;
		case lc::style::None:
			_data->window.create(sf::VideoMode(_data->settings.graphics.width, _data->settings.graphics.height), _data->settings.general.title, sf::Style::None, _data->settings.context_settings);
			break;
		case lc::style::Resize:
			_data->window.create(sf::VideoMode(_data->settings.graphics.width, _data->settings.graphics.height), _data->settings.general.title, sf::Style::Resize, _data->settings.context_settings);
			break;
		case lc::style::Titlebar:
			_data->window.create(sf::VideoMode(_data->settings.graphics.width, _data->settings.graphics.height), _data->settings.general.title, sf::Style::Titlebar, _data->settings.context_settings);
			break;
		default:
			_data->window.create(sf::VideoMode(_data->settings.graphics.width, _data->settings.graphics.height), _data->settings.general.title, sf::Style::Close, _data->settings.context_settings);
			lc::utils::log(_LOGPATH, "From application::init: Error occured trying to access 'window style'!");
			break;
		}
		_data->window.setVerticalSyncEnabled(_data->settings.graphics.vertical_sync);
		
		// bind gui after window was created
		_data->gui.setWindow(_data->window);

		// add splash state
		if(_data->settings.general.splash_time > 0.f)
		{
			_data->states.add(std::unique_ptr<nengine::nstate_manager::nstate>(new lc::splash(_data)), true);
		}
		else
		{
			_data->states.add(std::unique_ptr<nengine::nstate_manager::nstate>(new lc::main_menu(_data)), true);
		}
	}

	int application::run()
	{
		float new_time = 0.f;
		float frame_time = 0.f; // the time the last "frame" (all processing, drawing, etc) took
		float accumulator = 0.f; // stores the amount of time necessary for handling physics etc independent of cpu speed
		float interpolarization = 0.f;
		float current_time = _dtc.getElapsedTime().asSeconds();

		while (_data->window.isOpen())
		{
			_data->states.process(); // process all changes in last state

			if(_clrclock.getElapsedTime().asSeconds() >= _CLRINTERVALL) // intervall defined in definitions.hpp
			{
				// only clear after a state has been removed
#if _DEBUG
				std::cerr << "clearing resources" << std::endl;
#endif
				_data->resources.clr_unused(); // clear all unused resources

				_clrclock.restart(); // restart timer
			}

			new_time = _dtc.getElapsedTime().asSeconds();
			frame_time = new_time - current_time; // get the time the last frame took

			if (frame_time > 0.25)
			{
				frame_time = 0.25; // to prevent massive lags
			}

			current_time = new_time;

			accumulator += frame_time;

			while (accumulator >= _dt)
			{
				_data->states.get()->handle(); // handle all input

				_data->states.get()->update(_dt); // update everything of the current state

				accumulator -= _dt;
			}

			_data->states.get()->draw(interpolarization);
		}

		return 0;
	}
}
