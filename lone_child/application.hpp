#ifndef __LC__APPLICATION__
#define __LC__APPLICATION__

// lc
#include "configurator.hpp"

// std
#include <memory>
#include <string>

// lib
#include <SFML/Graphics.hpp>
#include <TGUI/TGUI.hpp>

// NEngine
#include "../include/NEngine_1.0.0/ninput_manager/ninput_manager.hpp"
#include "../include/NEngine_1.0.0/nparticle_system/nparticle_system.hpp"
#include "../include/NEngine_1.0.0/nphysics/nphysics.hpp"
#include "../include/NEngine_1.0.0/nresource_manager/nresource_wrapper.hpp"
#include "../include/NEngine_1.0.0/nstate_manager/nstate_manager.hpp"

namespace lc {

	struct game_data
	{
		sf::RenderWindow window;
		tgui::Gui gui; // gui needs clear in all destructors
		settings settings;
		configurator config;
		// NEngine
		nengine::ninput_manager::ninput_manager input;
		nengine::nparticle_system::nparticle_system particles;
		nengine::nphysics::ncollision_manager physics;
		nengine::nresource_manager::nresource_wrapper<int /* placeholder --> maybe for animated sprites or hitboxes */> resources; // TODO: Evaluate what's needed here
		nengine::nstate_manager::nstate_manager states;
	};

	class application
	{
	public:
		application();

		int run();

	private:
		std::shared_ptr<game_data> _data;

		const float _dt; // delta time for calculating physics independent of cpu power
		sf::Clock _dtc; // delta time clock
		sf::Clock _clrclock; // clock for periodically clearing resources
	};

}

#endif
