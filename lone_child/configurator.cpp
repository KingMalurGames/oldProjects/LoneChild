// lc
#include "configurator.hpp"

// std
#include <string>
#include <exception>

#if _DEBUG
#include <iostream>
#include <chrono>
#endif

// lib

// NEngine

namespace lc {

	configurator::configurator()
	{
	}

	bool configurator::load_ini(std::string const& path_to_config, settings& settings, nengine::ninput_manager::ninput_manager& input)
	{
#if _DEBUG
		std::chrono::steady_clock::time_point begin = std::chrono::steady_clock::now();
#endif
		// format install path
		std::string string_path_to_config = path_to_config;
		lc::utils::replace_all_substrings(string_path_to_config, "\\", "/");

		// open and read ini file
		CSimpleIniA conf;
		conf.SetUnicode();
		SI_Error rc = conf.LoadFile(string_path_to_config.c_str());

		if (rc < 0) // no ini found
		{
			// create new one
			if (!_fill_ini(string_path_to_config, conf))
			{
				return false;
			}
		}

		// load ini
		bool rb = _load_ini(string_path_to_config, settings, conf, input);

#if _DEBUG
		std::chrono::steady_clock::time_point end = std::chrono::steady_clock::now();
		std::cerr << "Loading Ini took: " <<std::chrono::duration_cast<std::chrono::milliseconds>(end - begin).count() / 1000.f << " seconds" << std::endl;
#endif
		return rb;
	}

	bool configurator::load_db(std::string const& path_to_config, settings& settings, nengine::ninput_manager::ninput_manager& input)
	{
#if _DEBUG
		std::chrono::steady_clock::time_point begin = std::chrono::steady_clock::now();
#endif
		// test if settings table exists
		bool exists;
		if (_access(path_to_config.c_str(), 0) != -1)
		{ // exists
			exists = true;
		}
		else
		{ // does not exists
			exists = false;
		}

		sqlite3 *db; // open connection
		int db_handle = sqlite3_open(path_to_config.c_str(), &db);
		if (db_handle) // if 0 --> false
		{
			return false;
		}

		if (!exists)
		{
			if (!_fill_db(db))
			{ // standard configuration could not be created
				return false;
			}
		}

		// store in boolean, no return possible because db should not be closed in sub-function!
		bool rb = _load_db(db, settings, input);

		// close and return true
		sqlite3_close(db);

#if _DEBUG
		std::chrono::steady_clock::time_point end = std::chrono::steady_clock::now();
		std::cerr << "Loading DB took: " << std::chrono::duration_cast<std::chrono::milliseconds>(end - begin).count() / 1000.f << " seconds" << std::endl;
#endif

		return rb;
	}

	bool configurator::upd_ini_key(std::string const& path_to_config, sf::Keyboard::Key const& setting, std::string const& table, std::string const& option)
	{
#if _DEBUG
		std::chrono::steady_clock::time_point begin = std::chrono::steady_clock::now();
#endif
		// format install path
		std::string string_path_to_config = path_to_config;
		lc::utils::replace_all_substrings(string_path_to_config, "\\", "/");

		// open and read ini file
		CSimpleIniA conf;
		conf.SetUnicode();
		SI_Error rc = conf.LoadFile(string_path_to_config.c_str());

		if (rc < 0) // no ini found
		{
			return false;
		}

		// update ini
		bool rb = _upd_ini_key(conf, setting, table, option);

		if (rc < 0)
		{
			return false;
		}
		else
		{
			conf.SaveFile(path_to_config.c_str());
		}

#if _DEBUG
		std::chrono::steady_clock::time_point end = std::chrono::steady_clock::now();
		std::cerr << "Updating Ini took: " << std::chrono::duration_cast<std::chrono::milliseconds>(end - begin).count() / 1000.f << " seconds" << std::endl;
#endif
		return rb;
	}

	bool configurator:: upd_db_key(std::string const& path_to_config, sf::Keyboard::Key const& setting, std::string const& table, std::string const& option)
	{
#if _DEBUG
		std::chrono::steady_clock::time_point begin = std::chrono::steady_clock::now();
#endif
		// test if settings table exists
		bool exists;
		if (_access(path_to_config.c_str(), 0) != -1)
		{ // exists
			exists = true;
		}
		else
		{ // does not exists
			exists = false;
		}

		sqlite3 *db; // open connection
		int db_handle = sqlite3_open(path_to_config.c_str(), &db);
		if (db_handle) // if 0 --> false
		{
			return false;
		}

		if (!exists)
		{
			return false;
		}

		// update db
		bool rb = _upd_db_key(db, setting, table, option);

		// close and return true
		sqlite3_close(db);

#if _DEBUG
		std::chrono::steady_clock::time_point end = std::chrono::steady_clock::now();
		std::cerr << "Updating DB took: " << std::chrono::duration_cast<std::chrono::milliseconds>(end - begin).count() / 1000.f << " seconds" << std::endl;
#endif

		return rb;
	}
	
	bool configurator::_load_ini_string(std::string const& table, std::string const& option, std::string& setting, CSimpleIniA& conf)
	{
		std::string result;
		try
		{
			result = conf.GetValue(table.c_str(), option.c_str(), NULL);
		}
		catch (...)
		{
			lc::utils::log(_LOGPATH, "From configurator::_load_ini_string: Error occured trying to load string with value '" + table + ": " + option + "'!");
#if _DEBUG
			std::cerr << "Error loading: " << table << "." << option << std::endl;
#endif
			return false;
		}

		if (!result.empty())
		{
			setting = result;
		}

		return true;
	}

	bool configurator::_load_ini_float(std::string const& table, std::string const& option, float& setting, CSimpleIniA& conf, float min, float max)
	{
		float result;
		try
		{
			result = std::stof(conf.GetValue(table.c_str(), option.c_str(), NULL));
		}
		catch (...)
		{
			lc::utils::log(_LOGPATH, "From configurator::_load_ini_float: Error occured trying to load float with value '" + table + ": " + option + "'!");
#if _DEBUG
			std::cerr << "Error loading: " << table << "." << option << std::endl;
#endif
			return false;
		}

		if ((result >= min) && (result <= max))
		{
			setting = result;
		}

		return true;
	}

	bool configurator::_load_ini_int(std::string const& table, std::string const& option, unsigned int& setting, CSimpleIniA& conf, unsigned int min, unsigned int max)
	{
		float result;
		try
		{
			result = std::stoi(conf.GetValue(table.c_str(), option.c_str(), NULL));
		}
		catch (...)
		{
			lc::utils::log(_LOGPATH, "From configurator::_load_ini_int: Error occured trying to load int with value '" + table + ": " + option + "'!");
#if _DEBUG
			std::cerr << "Error loading: " << table << "." << option << std::endl;
#endif
			return false;
		}

		if ((result >= min) && (result <= max))
		{
			setting = result;
		}

		return true;
	}

	bool configurator::_load_ini_bool(std::string const& table, std::string const& option, bool& setting, CSimpleIniA& conf)
	{
		try
		{
			setting = conf.GetValue(table.c_str(), option.c_str(), NULL);
		}
		catch (...)
		{
			lc::utils::log(_LOGPATH, "From configurator::_load_ini_bool: Error occured trying to load bool with value '" + table + ": " + option + "'!");
#if _DEBUG
			std::cerr << "Error loading: " << table << "." << option << std::endl;
#endif
			return false;
		}

		return true;
	}

	bool configurator::_load_ini_key(std::string const& table, std::string const& option, std::string const& bind_name, sf::Keyboard::Key default, CSimpleIniA& conf, nengine::ninput_manager::ninput_manager& input, settings& settings)
	{
		sf::Keyboard::Key tmp;
		try
		{
			std::string key = conf.GetValue(table.c_str(), option.c_str(), NULL);
			tmp = _convert_str_to_key(key);
		}
		catch (...)
		{
			lc::utils::log(_LOGPATH, "From configurator::_load_ini_key: Error occured trying to load key with value '" + table + ": " + option + "'!");
#if _DEBUG
			std::cerr << "Error loading: " << table << "." << option << std::endl;
#endif
			return false;
		}

		if (tmp != sf::Keyboard::Unknown)
		{
			input.add_bind(bind_name, tmp);
			settings.keyboard.keybindings.insert(std::make_pair(bind_name, tmp));
		}
		else
		{
			input.add_bind(bind_name, default);
			settings.keyboard.keybindings.insert(std::make_pair(bind_name, default));
		}
		
		return true;
	}

	bool configurator::_load_ini(std::string const& path_to_config, settings& settings, CSimpleIniA& conf, nengine::ninput_manager::ninput_manager& input)
	{
		// GENERAL
		// title
		if (!_load_ini_string("GENERAL", "title", settings.general.title, conf))
		{
			return false;
		}

		// splash_time
		if (!_load_ini_float("GENERAL", "splash_time", settings.general.splash_time, conf, 0.f, 5.f))
		{
			return false;
		}

		// AUDIO
		// general
		if (!_load_ini_int("AUDIO", "general", settings.audio.general, conf, 0, 100))
		{
			return false;
		}

		// music
		if (!_load_ini_int("AUDIO", "music", settings.audio.music, conf, 0, 100))
		{
			return false;
		}

		// effects
		if (!_load_ini_int("AUDIO", "effects", settings.audio.effects, conf, 0, 100))
		{
			return false;
		}

		// GRAPHICS
		// antialiasing
		if (!_load_ini_int("GRAPHICS", "antialiasing", settings.graphics.antialiasing, conf, 0, 16))
		{
			return false;
		}

		// width
		if (!_load_ini_int("GRAPHICS", "width", settings.graphics.width, conf, 1280, 1920))
		{
			return false;
		}

		// height
		if (!_load_ini_int("GRAPHICS", "height", settings.graphics.height, conf, 720, 1080))
		{
			return false;
		}

		// vertical_sync
		if (!_load_ini_bool("GRAPHICS", "vertical_sync", settings.graphics.vertical_sync, conf))
		{
			return false;
		}

		// style
		std::string style_string = "";
		if (!_load_ini_string("GRAPHICS", "style", style_string, conf))
		{
			return false;
		}

		if (style_string == "None")
		{
			settings.graphics.style = lc::style::None;
		}
		else if (style_string == "Titlebar")
		{
			settings.graphics.style = lc::style::Titlebar;
		}
		else if (style_string == "Resize")
		{
			settings.graphics.style = lc::style::Resize;
		}
		else if (style_string == "Close")
		{
			settings.graphics.style = lc::style::Close;
		}
		else if (style_string == "Fullscreen")
		{
			settings.graphics.style = lc::style::Fullscreen;
		}
		else if (style_string == "Default")
		{
			settings.graphics.style = lc::style::Default;
		}
		else
		{
			settings.graphics.style = lc::style::Close;
		}

		// KEYBOARD
		// up
		if (!_load_ini_key("KEYBOARD", "up", "up", sf::Keyboard::W, conf, input, settings))
		{
			return false;
		}

		// down
		if (!_load_ini_key("KEYBOARD", "down", "down", sf::Keyboard::S, conf, input, settings))
		{
			return false;
		}

		// left
		if (!_load_ini_key("KEYBOARD", "left", "left", sf::Keyboard::A, conf, input, settings))
		{
			return false;
		}

		// right
		if (!_load_ini_key("KEYBOARD", "right", "right", sf::Keyboard::D, conf, input, settings))
		{
			return false;
		}

		// jump
		if (!_load_ini_key("KEYBOARD", "jump", "jump", sf::Keyboard::Space, conf, input, settings))
		{
			return false;
		}

		// interact
		if (!_load_ini_key("KEYBOARD", "interact", "interact", sf::Keyboard::F, conf, input, settings))
		{
			return false;
		}

		// character
		if (!_load_ini_key("KEYBOARD", "character", "character", sf::Keyboard::C, conf, input, settings))
		{
			return false;
		}

		// backpack
		if (!_load_ini_key("KEYBOARD", "backpack", "backpack", sf::Keyboard::I, conf, input, settings))
		{
			return false;
		}

		// skill_1
		if (!_load_ini_key("KEYBOARD", "skill_1", "skill_1", sf::Keyboard::Num1, conf, input, settings))
		{
			return false;
		}

		// skill_2
		if (!_load_ini_key("KEYBOARD", "skill_2", "skill_2", sf::Keyboard::Num2, conf, input, settings))
		{
			return false;
		}

		// skill_3
		if (!_load_ini_key("KEYBOARD", "skill_3", "skill_3", sf::Keyboard::Num3, conf, input, settings))
		{
			return false;
		}

		// skill_4
		if (!_load_ini_key("KEYBOARD", "skill_4", "skill_4", sf::Keyboard::Num4, conf, input, settings))
		{
			return false;
		}

		// skill_5
		if (!_load_ini_key("KEYBOARD", "skill_5", "skill_5", sf::Keyboard::Num5, conf, input, settings))
		{
			return false;
		}

		// skill_6
		if (!_load_ini_key("KEYBOARD", "skill_6", "skill_6", sf::Keyboard::Num6, conf, input, settings))
		{
			return false;
		}

		// skill_7
		if (!_load_ini_key("KEYBOARD", "skill_7", "skill_7", sf::Keyboard::Num7, conf, input, settings))
		{
			return false;
		}

		// skill_8
		if (!_load_ini_key("KEYBOARD", "skill_8", "skill_8", sf::Keyboard::Num8, conf, input, settings))
		{
			return false;
		}

		// skill_9
		if (!_load_ini_key("KEYBOARD", "skill_9", "skill_9", sf::Keyboard::Num9, conf, input, settings))
		{
			return false;
		}

		// skill_10
		if (!_load_ini_key("KEYBOARD", "skill_10", "skill_10", sf::Keyboard::Num0, conf, input, settings))
		{
			return false;
		}

		// pause
		if (!_load_ini_key("KEYBOARD", "pause", "pause", sf::Keyboard::Escape, conf, input, settings))
		{
			return false;
		}

		return true;
	}

	void configurator::_load_db_string(std::string const& table, sqlite3_stmt* stmt, std::string const& key, std::string const& key_to_read, unsigned int index, std::string& setting)
	{
		if (key != key_to_read)
		{
			// not correct key
			return;
		}

		const char * value = reinterpret_cast<const char *>(sqlite3_column_text(stmt, index));
		std::string tmp_value(value);
		if (tmp_value != "")
		{
			setting = std::string(value);
		}
		else
		{
			lc::utils::log(_LOGPATH, "From configurator::_load_db_string: Error occured trying to load string with value '" + table + ": " + key_to_read + "'!");
#if _DEBUG
			std::cerr << "Error loading: " << table << "." << key << std::endl;
#endif
			return;
		}

		return;
	}

	void configurator::_load_db_float(std::string const& table, sqlite3_stmt* stmt, std::string const& key, std::string const& key_to_read, unsigned int index, float& setting, float min, float max)
	{
		if (key != key_to_read)
		{
			// not correct key
			return;
		}

		const char * value = reinterpret_cast<const char *>(sqlite3_column_text(stmt, 2));
		std::string tmp_value(value);
		if (tmp_value != "")
		{
			float result;
			try
			{
				result = std::stof(value);
				if ((result >= min) && (result <= max))
				{
					setting = result;
				}
			}
			catch (...)
			{
				lc::utils::log(_LOGPATH, "From configurator::_load_db_float: Error occured trying to load float with value '" + table + ": " + key_to_read + "'!");
#if _DEBUG
				std::cerr << "Error loading: " << table << "." << key << std::endl;
#endif
				return;
			}
			if ((result >= min) && (result <= max))
			{
				setting = result;
			}
		}
		else
		{
#if _DEBUG
			std::cerr << "Error loading: " << table << "." << key << std::endl;
#endif
			return;
		}
	}
	
	void configurator::_load_db_int(std::string const& table, sqlite3_stmt* stmt, std::string const& key, std::string const& key_to_read, unsigned int index, unsigned int& setting, unsigned int min, unsigned int max)
	{
		if (key != key_to_read)
		{
			// not correct key
			return;
		}

		const char * value = reinterpret_cast<const char *>(sqlite3_column_text(stmt, 2));
		std::string tmp_value(value);
		if (tmp_value != "")
		{
			unsigned int result;
			try
			{
				result = std::stoi(value);
			}
			catch (...)
			{
				lc::utils::log(_LOGPATH, "From configurator::_load_db_int: Error occured trying to load int with value '" + table + ": " + key_to_read + "'!");
#if _DEBUG
				std::cerr << "Error loading: " << table << "." << key << std::endl;
#endif
				return;
			}
			if ((result >= min) && (result <= max))
			{
				setting = result;
			}
		}
		else
		{
#if _DEBUG
			std::cerr << "Error loading: " << table << "." << key << std::endl;
#endif
			return;
		}
	}

	void configurator::_load_db_bool(std::string const& table, sqlite3_stmt* stmt, std::string const& key, std::string const& key_to_read, unsigned int index, bool& setting)
	{
		if (key != key_to_read)
		{
			// not correct key
			return;
		}

		const char * value = reinterpret_cast<const char *>(sqlite3_column_text(stmt, 2));
		std::string tmp_value(value);
		if ((tmp_value != "") && ((tmp_value == "0") || (tmp_value == "1")))
		{
			bool result;
			try
			{
				result = value;
			}
			catch (...)
			{
				lc::utils::log(_LOGPATH, "From configurator::_load_db_bool: Error occured trying to load bool with value '" + table + ": " + key_to_read + "'!");
#if _DEBUG
				std::cerr << "Error loading: " << table << "." << key << std::endl;
#endif
				return;
			}
			setting = result;
		}
		else
		{
			lc::utils::log(_LOGPATH, "From configurator::_load_db_bool: Error occured trying to load bool with value '" + table + ": " + key_to_read + "'!");
#if _DEBUG
			std::cerr << "Error loading: " << table << "." << key << std::endl;
#endif
			return;
		}
	}

	void configurator::_load_db_key(std::string const& table, sqlite3_stmt* stmt, std::string const& key, std::string const& key_to_read, std::string const& bind_name, sf::Keyboard::Key default, nengine::ninput_manager::ninput_manager& input, settings& settings)
	{
		if (key != key_to_read)
		{
			// not correct key
			return;
		}

		const char * value = reinterpret_cast<const char *>(sqlite3_column_text(stmt, 2));
		std::string tmp_value(value);
		if (tmp_value != "")
		{
			sf::Keyboard::Key result;
			try
			{
				result = _convert_str_to_key(tmp_value);
			}
			catch (...)
			{
				lc::utils::log(_LOGPATH, "From configurator::_load_db_key: Error occured trying to load key with value '" + table + ": " + key_to_read + "'!");
#if _DEBUG
				std::cerr << "Error loading: " << table << "." << key << std::endl;
#endif
				return;
			}

			if (result != sf::Keyboard::Unknown)
			{
				input.add_bind(bind_name, result);
				settings.keyboard.keybindings.insert(std::make_pair(bind_name, result));
			}
			else
			{
				input.add_bind(bind_name, default);
				settings.keyboard.keybindings.insert(std::make_pair(bind_name, default));
			}
		}
		else
		{
			lc::utils::log(_LOGPATH, "From configurator::_load_db_key: Error occured trying to load key with value '" + table + ": " + key_to_read + "'!");
#if _DEBUG
			std::cerr << "Error loading: " << table << "." << key << std::endl;
#endif
			return;
		}
	}

	bool configurator::_load_db(sqlite3 *db, settings& settings, nengine::ninput_manager::ninput_manager& input)
	{
		// sql statement tmp
		std::string sql;

		// sqlite3 stmt storage
		sqlite3_stmt *stmt;

		// return code for testing if successful
		int rc;

		// GENERAL
		sql =
			// select all from table GENERAL
			"SELECT * FROM GENERAL;";
		// execute
		rc = sqlite3_prepare_v2(db, sql.c_str(), -1, &stmt, NULL);
		if (rc != SQLITE_OK)
		{
			lc::utils::log(_LOGPATH, "From configurator::_load_db: Error occured trying to select from: 'GENERAL'!");
#if _DEBUG
			std::cerr << "Error selecting GENERAL.*" << std::endl;
#endif
			return false;
		}

		while ((rc = sqlite3_step(stmt)) == SQLITE_ROW)
		{
			const char * name = reinterpret_cast<const char *>(sqlite3_column_text(stmt, 1));
			// title
			_load_db_string("GENERAL", stmt, std::string(name), "title", 2, settings.general.title);

			// splash_time
			_load_db_float("GENERAL", stmt, std::string(name), "splash_time", 2, settings.general.splash_time, 0.f, 4.f);
		}

		if (rc != SQLITE_DONE)
		{
			lc::utils::log(_LOGPATH, "From configurator::_load_db: Error occured selecting from: 'GENERAL'!");
#if _DEBUG
			std::cerr << "SELECT failed: " << sqlite3_errmsg(db) << std::endl;
#endif
			// finalize if error before return
			sqlite3_finalize(stmt);

			return false;
		}

		// finalize if no error before new prepare
		sqlite3_finalize(stmt);

		// AUDIO
		sql =
			// select all from table AUDIO
			"SELECT * FROM AUDIO;";
		// execute
		rc = sqlite3_prepare_v2(db, sql.c_str(), -1, &stmt, NULL);
		if (rc != SQLITE_OK)
		{
			lc::utils::log(_LOGPATH, "From configurator::_load_db: Error occured trying to select from: 'AUDIO'!");
#if _DEBUG
			std::cerr << "Error selecting AUDIO.*" << std::endl;
#endif
			return false;
		}

		while ((rc = sqlite3_step(stmt)) == SQLITE_ROW)
		{
			const char * name = reinterpret_cast<const char *>(sqlite3_column_text(stmt, 1));
			// general
			_load_db_int("AUDIO", stmt, std::string(name), "general", 2, settings.audio.general, 0, 100);

			// music
			_load_db_int("AUDIO", stmt, std::string(name), "music", 2, settings.audio.music, 0, 100);

			// effects
			_load_db_int("AUDIO", stmt, std::string(name), "effects", 2, settings.audio.effects, 0, 100);
		}

		if (rc != SQLITE_DONE)
		{
			lc::utils::log(_LOGPATH, "From configurator::_load_db: Error occured selecting from: 'AUDIO'!");
#if _DEBUG
			std::cerr << "SELECT failed: " << sqlite3_errmsg(db) << std::endl;
#endif
			// finalize if error before return
			sqlite3_finalize(stmt);

			return false;
		}

		// finalize if no error before new prepare
		sqlite3_finalize(stmt);

		// GRAPHICS
		sql =
			// select all from table GRAPHICS
			"SELECT * FROM GRAPHICS;";
		// execute
		rc = sqlite3_prepare_v2(db, sql.c_str(), -1, &stmt, NULL);
		if (rc != SQLITE_OK)
		{
			lc::utils::log(_LOGPATH, "From configurator::_load_db: Error occured trying to select from: 'GRAPHICS'!");
#if _DEBUG
			std::cerr << "Error selecting GRAPHICS.*" << std::endl;
#endif
			return false;
		}

		while ((rc = sqlite3_step(stmt)) == SQLITE_ROW)
		{
			const char * name = reinterpret_cast<const char *>(sqlite3_column_text(stmt, 1));
			// antialiasing
			_load_db_int("GRAPHICS", stmt, std::string(name), "antialiasing", 2, settings.graphics.antialiasing, 0, 16);

			// width
			_load_db_int("GRAPHICS", stmt, std::string(name), "width", 2, settings.graphics.width, 1280, 1920);

			// height
			_load_db_int("GRAPHICS", stmt, std::string(name), "height", 2, settings.graphics.height, 720, 1080);

			// vertical_sync
			_load_db_bool("GRAPHICS", stmt, std::string(name), "vertical_sync", 2, settings.graphics.vertical_sync);
		}

		if (rc != SQLITE_DONE)
		{
			lc::utils::log(_LOGPATH, "From configurator::_load_db: Error occured selecting from: 'GRAPHICS'!");
#if _DEBUG
			std::cerr << "SELECT failed: " << sqlite3_errmsg(db) << std::endl;
#endif
			// finalize if error before return
			sqlite3_finalize(stmt);

			return false;
		}

		// finalize if no error before new prepare
		sqlite3_finalize(stmt);

		// KEYBOARD
		sql =
			// select all from table KEYBOARD
			"SELECT * FROM KEYBOARD;";
		// execute
		rc = sqlite3_prepare_v2(db, sql.c_str(), -1, &stmt, NULL);
		if (rc != SQLITE_OK)
		{
			lc::utils::log(_LOGPATH, "From configurator::_load_db: Error occured trying to select from: 'KEYBOARD'!");
#if _DEBUG
			std::cerr << "Error selecting KEYBOARD.*" << std::endl;
#endif
			return false;
		}

		while ((rc = sqlite3_step(stmt)) == SQLITE_ROW)
		{
			const char * name = reinterpret_cast<const char *>(sqlite3_column_text(stmt, 1));
			// up
			_load_db_key("KEYBOARD", stmt, std::string(name), "up", "up", sf::Keyboard::W, input, settings);

			// down
			_load_db_key("KEYBOARD", stmt, std::string(name), "down", "down", sf::Keyboard::S, input, settings);

			// left
			_load_db_key("KEYBOARD", stmt, std::string(name), "left", "left", sf::Keyboard::A, input, settings);

			// right
			_load_db_key("KEYBOARD", stmt, std::string(name), "right", "right", sf::Keyboard::D, input, settings);

			// jump
			_load_db_key("KEYBOARD", stmt, std::string(name), "jump", "jump", sf::Keyboard::Space, input, settings);

			// interact
			_load_db_key("KEYBOARD", stmt, std::string(name), "interact", "interact", sf::Keyboard::F, input, settings);

			// character
			_load_db_key("KEYBOARD", stmt, std::string(name), "character", "character", sf::Keyboard::C, input, settings);

			// backpack
			_load_db_key("KEYBOARD", stmt, std::string(name), "backpack", "backpack", sf::Keyboard::I, input, settings);

			// skill_1
			_load_db_key("KEYBOARD", stmt, std::string(name), "skill_1", "skill_1", sf::Keyboard::Num1, input, settings);

			// skill_2
			_load_db_key("KEYBOARD", stmt, std::string(name), "skill_2", "skill_2", sf::Keyboard::Num2, input, settings);

			// skill_3
			_load_db_key("KEYBOARD", stmt, std::string(name), "skill_3", "skill_3", sf::Keyboard::Num3, input, settings);

			// skill_4
			_load_db_key("KEYBOARD", stmt, std::string(name), "skill_4", "skill_4", sf::Keyboard::Num4, input, settings);

			// skill_5
			_load_db_key("KEYBOARD", stmt, std::string(name), "skill_5", "skill_5", sf::Keyboard::Num5, input, settings);

			// skill_6
			_load_db_key("KEYBOARD", stmt, std::string(name), "skill_6", "skill_6", sf::Keyboard::Num6, input, settings);

			// skill_7
			_load_db_key("KEYBOARD", stmt, std::string(name), "skill_7", "skill_7", sf::Keyboard::Num7, input, settings);

			// skill_8
			_load_db_key("KEYBOARD", stmt, std::string(name), "skill_8", "skill_8", sf::Keyboard::Num8, input, settings);

			// skill_9
			_load_db_key("KEYBOARD", stmt, std::string(name), "skill_9", "skill_9", sf::Keyboard::Num9, input, settings);

			// skill_10
			_load_db_key("KEYBOARD", stmt, std::string(name), "skill_10", "skill_10", sf::Keyboard::Num0, input, settings);

			// pause
			_load_db_key("KEYBOARD", stmt, std::string(name), "pause", "pause", sf::Keyboard::Escape, input, settings);

		}

		if (rc != SQLITE_DONE)
		{
			lc::utils::log(_LOGPATH, "From configurator::_load_db: Error selecting from: 'KEYBOARD'!");
#if _DEBUG
			std::cerr << "SELECT failed: " << sqlite3_errmsg(db) << std::endl;
#endif
			// finalize if error before return
			sqlite3_finalize(stmt);

			return false;
		}

		// finalize if no error before new prepare
		sqlite3_finalize(stmt);

		return true;
	}

	bool configurator::_fill_ini(std::string const& path_to_config, CSimpleIniA& conf)
	{
		SI_Error rc;

		// GENERAL
		// title
		rc = conf.SetValue( /* section */ "GENERAL", /* name */ "title", /* value */ "Lone Child");
		if (rc < 0)
		{
			return false;
		}

		// splash_time
		rc = conf.SetValue( /* section */ "GENERAL", /* name */ "splash_time", /* value */ "2.5");
		if (rc < 0)
		{
			return false;
		}

		// AUDIO
		// general
		rc = conf.SetValue( /* section */ "AUDIO", /* name */ "general", /* value */ "100");
		if (rc < 0)
		{
			return false;
		}

		// music
		rc = conf.SetValue( /* section */ "AUDIO", /* name */ "music", /* value */ "100");
		if (rc < 0)
		{
			return false;
		}

		// effects
		rc = conf.SetValue( /* section */ "AUDIO", /* name */ "effects", /* value */ "100");
		if (rc < 0)
		{
			return false;
		}

		// GRAPHICS
		// antialiasing
		rc = conf.SetValue( /* section */ "GRAPHICS", /* name */ "antialiasing", /* value */ "4");
		if (rc < 0)
		{
			return false;
		}

		// width
		rc = conf.SetValue( /* section */ "GRAPHICS", /* name */ "width", /* value */ "1280");
		if (rc < 0)
		{
			return false;
		}

		// height
		rc = conf.SetValue( /* section */ "GRAPHICS", /* name */ "height", /* value */ "720");
		if (rc < 0)
		{
			return false;
		}

		// vertical_sync
		rc = conf.SetValue( /* section */ "GRAPHICS", /* name */ "vertical_sync", /* value */ "0");
		if (rc < 0)
		{
			return false;
		}

		// style
		rc = conf.SetValue( /* section */ "GRAPHICS", /* name */ "style", /* value */ "Close");
		if (rc < 0)
		{
			return false;
		}
		
		// KEYBOARD
		// up
		rc = conf.SetValue( /* section */ "KEYBOARD", /* name */ "up", /* value */ "W");
		if (rc < 0)
		{
			return false;
		}

		// down
		rc = conf.SetValue( /* section */ "KEYBOARD", /* name */ "down", /* value */ "S");
		if (rc < 0)
		{
			return false;
		}

		// left
		rc = conf.SetValue( /* section */ "KEYBOARD", /* name */ "left", /* value */ "A");
		if (rc < 0)
		{
			return false;
		}

		// right
		rc = conf.SetValue( /* section */ "KEYBOARD", /* name */ "right", /* value */ "D");
		if (rc < 0)
		{
			return false;
		}

		// jump
		rc = conf.SetValue( /* section */ "KEYBOARD", /* name */ "jump", /* value */ "Space");
		if (rc < 0)
		{
			return false;
		}

		// interact
		rc = conf.SetValue( /* section */ "KEYBOARD", /* name */ "interact", /* value */ "F");
		if (rc < 0)
		{
			return false;
		}

		// character
		rc = conf.SetValue( /* section */ "KEYBOARD", /* name */ "character", /* value */ "C");
		if (rc < 0)
		{
			return false;
		}

		// backpack
		rc = conf.SetValue( /* section */ "KEYBOARD", /* name */ "backpack", /* value */ "I");
		if (rc < 0)
		{
			return false;
		}

		// skill_1
		rc = conf.SetValue( /* section */ "KEYBOARD", /* name */ "skill_1", /* value */ "1");
		if (rc < 0)
		{
			return false;
		}

		// skill_2
		rc = conf.SetValue( /* section */ "KEYBOARD", /* name */ "skill_2", /* value */ "2");
		if (rc < 0)
		{
			return false;
		}

		// skill_3
		rc = conf.SetValue( /* section */ "KEYBOARD", /* name */ "skill_3", /* value */ "3");
		if (rc < 0)
		{
			return false;
		}

		// skill_4
		rc = conf.SetValue( /* section */ "KEYBOARD", /* name */ "skill_4", /* value */ "4");
		if (rc < 0)
		{
			return false;
		}

		// skill_5
		rc = conf.SetValue( /* section */ "KEYBOARD", /* name */ "skill_5", /* value */ "5");
		if (rc < 0)
		{
			return false;
		}

		// skill_6
		rc = conf.SetValue( /* section */ "KEYBOARD", /* name */ "skill_6", /* value */ "6");
		if (rc < 0)
		{
			return false;
		}

		// skill_7
		rc = conf.SetValue( /* section */ "KEYBOARD", /* name */ "skill_7", /* value */ "7");
		if (rc < 0)
		{
			return false;
		}

		// skill_8
		rc = conf.SetValue( /* section */ "KEYBOARD", /* name */ "skill_8", /* value */ "8");
		if (rc < 0)
		{
			return false;
		}

		// skill_9
		rc = conf.SetValue( /* section */ "KEYBOARD", /* name */ "skill_9", /* value */ "9");
		if (rc < 0)
		{
			return false;
		}

		// skill_10
		rc = conf.SetValue( /* section */ "KEYBOARD", /* name */ "skill_10", /* value */ "0");
		if (rc < 0)
		{
			return false;
		}

		// pause
		rc = conf.SetValue( /* section */ "KEYBOARD", /* name */ "pause", /* value */ "Escape");
		if (rc < 0)
		{
			return false;
		}

		conf.SaveFile(path_to_config.c_str());

		return true;
	}

	bool configurator::_fill_db(sqlite3 *db)
	{
		// sql statement tmp
		std::string sql;

		// error message
		char *error_msg;

		// return code for testing if successful
		int rc;

		sql =
		// create table GENERAL
			"CREATE TABLE GENERAL ("\
			"ID INT PRIMARY KEY NOT NULL,"\
			"NAME TEXT,"
			"VALUE TEXT);"\
			// add value title
			"INSERT INTO GENERAL (ID, NAME, VALUE) "\
			"VALUES (1, 'title', 'Lone Child');"\
			// add value splash_time
			"INSERT INTO GENERAL (ID, NAME, VALUE) "\
			"VALUES (2, 'splash_time', '2.5');";
		// execute
		rc = sqlite3_exec(db, sql.c_str(), NULL, 0, &error_msg);
		if (rc != SQLITE_OK)
		{
			lc::utils::log(_LOGPATH, "From configurator::_fill_db: Error occured trying to insert to: 'GENERAL'!");
#if _DEBUG
			std::cout << error_msg << std::endl;
#endif
			return false;
		}

		sql =
		// create table AUDIO
			"CREATE TABLE AUDIO ("\
			"ID INT PRIMARY KEY NOT NULL,"\
			"NAME TEXT,"
			"VALUE REAL);"\
		// add value general
			"INSERT INTO AUDIO (ID, NAME, VALUE) "\
			"VALUES (1, 'general', 100);"\
		// add value music
			"INSERT INTO AUDIO (ID, NAME, VALUE) "\
			"VALUES (2, 'music', 100);"\
		// add value effects
			"INSERT INTO AUDIO (ID, NAME, VALUE) "\
			"VALUES (3, 'effects', 100)";
		// execute
		rc = sqlite3_exec(db, sql.c_str(), NULL, 0, &error_msg);
		if (rc != SQLITE_OK)
		{
			lc::utils::log(_LOGPATH, "From configurator::_fill_db: Error occured trying to insert to: 'AUDIO'!");
#if _DEBUG
			std::cout << error_msg << std::endl;
#endif
			return false;
		}

		sql =
		// create table GRAPHICS
			"CREATE TABLE GRAPHICS ("\
			"ID INT PRIMARY KEY NOT NULL,"\
			"NAME TEXT,"
			"VALUE INT32);"\
		// add value antialiasing
			"INSERT INTO GRAPHICS (ID, NAME, VALUE) "\
			"VALUES (1, 'antialiasing', 4);"\
			// add value width
			"INSERT INTO GRAPHICS (ID, NAME, VALUE) "\
			"VALUES (2, 'width', 1280);"\
			// add value height
			"INSERT INTO GRAPHICS (ID, NAME, VALUE) "\
			"VALUES (3, 'height', 720);"\
			// add value vertical_sync
			"INSERT INTO GRAPHICS (ID, NAME, VALUE) "\
			"VALUES (4, 'vertical_sync', 0);"\
			// add value style
			"INSERT INTO GRAPHICS (ID, NAME, VALUE) "\
			"VALUES (5, 'style', 'Close');"; // 0 --> false
		rc = sqlite3_exec(db, sql.c_str(), NULL, 0, &error_msg);
		if (rc != SQLITE_OK)
		{
			lc::utils::log(_LOGPATH, "From configurator::_fill_db: Error occured trying to insert to: 'GRAPHICS'!");
#if _DEBUG
			std::cout << error_msg << std::endl;
#endif
			return false;
		}

		sql =
			// create table KEYBOARD
			"CREATE TABLE KEYBOARD ("\
			"ID INT PRIMARY KEY NOT NULL,"\
			"NAME TEXT,"
			"VALUE TEXT);"\
		// add value up
			"INSERT INTO KEYBOARD (ID, NAME, VALUE) "\
			"VALUES (1, 'up', 'W');"\
		// add value down
			"INSERT INTO KEYBOARD (ID, NAME, VALUE) "\
			"VALUES (2, 'down', 'S');"\
		// add value left
			"INSERT INTO KEYBOARD (ID, NAME, VALUE) "\
			"VALUES (3, 'left', 'A');"\
		// add value right
			"INSERT INTO KEYBOARD (ID, NAME, VALUE) "\
			"VALUES (4, 'right', 'D');"\
		// add value jump
			"INSERT INTO KEYBOARD (ID, NAME, VALUE) "\
			"VALUES (5, 'jump', 'Space');"\
		// add value interact
			"INSERT INTO KEYBOARD (ID, NAME, VALUE) "\
			"VALUES (6, 'interact', 'F');"\
		// add value character
			"INSERT INTO KEYBOARD (ID, NAME, VALUE) "\
			"VALUES (7, 'character', 'C');"\
		// add value backpack
			"INSERT INTO KEYBOARD (ID, NAME, VALUE) "\
			"VALUES (8, 'backpack', 'I');"\
		// add value skill_1
			"INSERT INTO KEYBOARD (ID, NAME, VALUE) "\
			"VALUES (9, 'skill_1', '1');"\
		// add value skill_2
			"INSERT INTO KEYBOARD (ID, NAME, VALUE) "\
			"VALUES (10, 'skill_2', '2');"\
		// add value skill_3
			"INSERT INTO KEYBOARD (ID, NAME, VALUE) "\
			"VALUES (11, 'skill_3', '3');"\
		// add value skill_4
			"INSERT INTO KEYBOARD (ID, NAME, VALUE) "\
			"VALUES (12, 'skill_4', '4');"\
		// add value skill_5
			"INSERT INTO KEYBOARD (ID, NAME, VALUE) "\
			"VALUES (13, 'skill_5', '5');"\
		// add value skill_6
			"INSERT INTO KEYBOARD (ID, NAME, VALUE) "\
			"VALUES (14, 'skill_6', '6');"\
		// add value skill_7
			"INSERT INTO KEYBOARD (ID, NAME, VALUE) "\
			"VALUES (15, 'skill_7', '7');"\
		// add value skill_8
			"INSERT INTO KEYBOARD (ID, NAME, VALUE) "\
			"VALUES (16, 'skill_8', '8');"\
		// add value skill_9
			"INSERT INTO KEYBOARD (ID, NAME, VALUE) "\
			"VALUES (17, 'skill_9', '9');"\
		// add value skill_10
			"INSERT INTO KEYBOARD (ID, NAME, VALUE) "\
			"VALUES (18, 'skill_10', '0');"\
		// add value pause
			"INSERT INTO KEYBOARD (ID, NAME, VALUE) "\
			"VALUES (19, 'pause', 'Escape');";
		rc = sqlite3_exec(db, sql.c_str(), NULL, 0, &error_msg);
		if (rc != SQLITE_OK)
		{
			lc::utils::log(_LOGPATH, "From configurator::_fill_db: Error occured trying to insert to: 'KEYBOARD'!");
#if _DEBUG
			std::cout << error_msg << std::endl;
#endif
			return false;
		}

		return true;
	}

	sf::Keyboard::Key configurator::convert_str_to_key(std::string const& key)
	{
		return _convert_str_to_key(key);
	}

	std::string configurator::convert_key_to_str(sf::Keyboard::Key const& key)
	{
		return _convert_key_to_str(key);
	}

	sf::Keyboard::Key configurator::_convert_str_to_key(std::string const& key)
	{
		if (key.empty())
		{
			return sf::Keyboard::Unknown;
		}

		if (key == "Unknown")
		{
			return sf::Keyboard::Unknown;
		}

		if (key == "A")
		{
			return sf::Keyboard::A;
		}

		if (key == "B")
		{
			return sf::Keyboard::B;
		}

		if (key == "C")
		{
			return sf::Keyboard::C;
		}

		if (key == "D")
		{
			return sf::Keyboard::D;
		}

		if (key == "E")
		{
			return sf::Keyboard::E;
		}

		if (key == "F")
		{
			return sf::Keyboard::F;
		}

		if (key == "G")
		{
			return sf::Keyboard::G;
		}

		if (key == "H")
		{
			return sf::Keyboard::H;
		}

		if (key == "I")
		{
			return sf::Keyboard::I;
		}

		if (key == "J")
		{
			return sf::Keyboard::J;
		}

		if (key == "K")
		{
			return sf::Keyboard::K;
		}

		if (key == "L")
		{
			return sf::Keyboard::L;
		}

		if (key == "M")
		{
			return sf::Keyboard::M;
		}

		if (key == "N")
		{
			return sf::Keyboard::N;
		}

		if (key == "O")
		{
			return sf::Keyboard::O;
		}

		if (key == "P")
		{
			return sf::Keyboard::P;
		}

		if (key == "Q")
		{
			return sf::Keyboard::Q;
		}

		if (key == "R")
		{
			return sf::Keyboard::R;
		}

		if (key == "S")
		{
			return sf::Keyboard::S;
		}

		if (key == "T")
		{
			return sf::Keyboard::T;
		}

		if (key == "U")
		{
			return sf::Keyboard::U;
		}

		if (key == "V")
		{
			return sf::Keyboard::V;
		}

		if (key == "W")
		{
			return sf::Keyboard::W;
		}

		if (key == "X")
		{
			return sf::Keyboard::X;
		}

		if (key == "Y")
		{
			return sf::Keyboard::Y;
		}

		if (key == "Z")
		{
			return sf::Keyboard::Z;
		}

		if (key == "0")
		{
			return sf::Keyboard::Num0;
		}

		if (key == "1")
		{
			return sf::Keyboard::Num1;
		}

		if (key == "2")
		{
			return sf::Keyboard::Num2;
		}

		if (key == "3")
		{
			return sf::Keyboard::Num3;
		}

		if (key == "4")
		{
			return sf::Keyboard::Num4;
		}

		if (key == "5")
		{
			return sf::Keyboard::Num5;
		}

		if (key == "6")
		{
			return sf::Keyboard::Num6;
		}

		if (key == "7")
		{
			return sf::Keyboard::Num7;
		}

		if (key == "8")
		{
			return sf::Keyboard::Num8;
		}

		if (key == "9")
		{
			return sf::Keyboard::Num9;
		}

		if (key == "Escape")
		{
			return sf::Keyboard::Escape;
		}

		if (key == "LControl")
		{
			return sf::Keyboard::LControl;
		}

		if (key == "LShift")
		{
			return sf::Keyboard::LShift;
		}

		if (key == "LAlt")
		{
			return sf::Keyboard::LAlt;
		}

		if (key == "LSystem")
		{
			return sf::Keyboard::LSystem;
		}

		if (key == "RControl")
		{
			return sf::Keyboard::RControl;
		}

		if (key == "RShift")
		{
			return sf::Keyboard::RShift;
		}

		if (key == "RAlt")
		{
			return sf::Keyboard::RAlt;
		}

		if (key == "RSystem")
		{
			return sf::Keyboard::RSystem;
		}

		if (key == "Menu")
		{
			return sf::Keyboard::Menu;
		}

		if (key == "LBracket")
		{
			return sf::Keyboard::LBracket;
		}

		if (key == "RBracket")
		{
			return sf::Keyboard::RBracket;
		}

		if (key == "SemiColon")
		{
			return sf::Keyboard::SemiColon;
		}

		if (key == "Comma")
		{
			return sf::Keyboard::Comma;
		}

		if (key == "Period")
		{
			return sf::Keyboard::Period;
		}

		if (key == "Quote")
		{
			return sf::Keyboard::Quote;
		}

		if (key == "Slash")
		{
			return sf::Keyboard::Slash;
		}

		if (key == "BackSlash")
		{
			return sf::Keyboard::BackSlash;
		}

		if (key == "Tilde")
		{
			return sf::Keyboard::Tilde;
		}

		if (key == "Equal")
		{
			return sf::Keyboard::Equal;
		}

		if (key == "Dash")
		{
			return sf::Keyboard::Dash;
		}

		if (key == "Space")
		{
			return sf::Keyboard::Space;
		}

		if (key == "Return")
		{
			return sf::Keyboard::Return;
		}

		if (key == "BackSpace")
		{
			return sf::Keyboard::BackSpace;
		}

		if (key == "Tab")
		{
			return sf::Keyboard::Tab;
		}

		if (key == "PageUp")
		{
			return sf::Keyboard::PageUp;
		}

		if (key == "PageDown")
		{
			return sf::Keyboard::PageDown;
		}

		if (key == "End")
		{
			return sf::Keyboard::End;
		}

		if (key == "Home")
		{
			return sf::Keyboard::Home;
		}

		if (key == "Insert")
		{
			return sf::Keyboard::Insert;
		}

		if (key == "Delete")
		{
			return sf::Keyboard::Delete;
		}

		if (key == "Add")
		{
			return sf::Keyboard::Add;
		}

		if (key == "Subtract")
		{
			return sf::Keyboard::Subtract;
		}

		if (key == "Multiply")
		{
			return sf::Keyboard::Multiply;
		}

		if (key == "Divide")
		{
			return sf::Keyboard::Divide;
		}

		if (key == "Left")
		{
			return sf::Keyboard::Left;
		}

		if (key == "Right")
		{
			return sf::Keyboard::Right;
		}

		if (key == "Up")
		{
			return sf::Keyboard::Up;
		}

		if (key == "Down")
		{
			return sf::Keyboard::Down;
		}

		if (key == "Numpad0")
		{
			return sf::Keyboard::Numpad0;
		}

		if (key == "Numpad1")
		{
			return sf::Keyboard::Numpad1;
		}

		if (key == "Numpad2")
		{
			return sf::Keyboard::Numpad2;
		}

		if (key == "Numpad3")
		{
			return sf::Keyboard::Numpad3;
		}

		if (key == "Numpad4")
		{
			return sf::Keyboard::Numpad4;
		}

		if (key == "Numpad5")
		{
			return sf::Keyboard::Numpad5;
		}

		if (key == "Numpad6")
		{
			return sf::Keyboard::Numpad6;
		}

		if (key == "Numpad7")
		{
			return sf::Keyboard::Numpad7;
		}

		if (key == "Numpad8")
		{
			return sf::Keyboard::Numpad8;
		}

		if (key == "Numpad9")
		{
			return sf::Keyboard::Numpad9;
		}

		if (key == "F1")
		{
			return sf::Keyboard::F1;
		}

		if (key == "F2")
		{
			return sf::Keyboard::F2;
		}

		if (key == "F3")
		{
			return sf::Keyboard::F3;
		}

		if (key == "F4")
		{
			return sf::Keyboard::F4;
		}

		if (key == "F5")
		{
			return sf::Keyboard::F5;
		}

		if (key == "F6")
		{
			return sf::Keyboard::F6;
		}

		if (key == "F7")
		{
			return sf::Keyboard::F7;
		}

		if (key == "F8")
		{
			return sf::Keyboard::F8;
		}

		if (key == "F9")
		{
			return sf::Keyboard::F9;
		}

		if (key == "F10")
		{
			return sf::Keyboard::F10;
		}

		if (key == "F11")
		{
			return sf::Keyboard::F11;
		}

		if (key == "F12")
		{
			return sf::Keyboard::F12;
		}

		if (key == "F13")
		{
			return sf::Keyboard::F13;
		}

		if (key == "F14")
		{
			return sf::Keyboard::F14;
		}

		if (key == "F15")
		{
			return sf::Keyboard::F15;
		}

		if (key == "Pause")
		{
			return sf::Keyboard::Pause;
		}

		return sf::Keyboard::Unknown;
	}

	std::string configurator::_convert_key_to_str(sf::Keyboard::Key const& key)
	{
		if (key == (-1))
		{
			return "";
		}

		if (key == sf::Keyboard::Unknown)
		{
			return "";
		}

		if (key == sf::Keyboard::A)
		{
			return "A";
		}

		if (key == sf::Keyboard::B)
		{
			return "B";
		}

		if (key == sf::Keyboard::C)
		{
			return "C";
		}

		if (key == sf::Keyboard::D)
		{
			return "D";
		}

		if (key == sf::Keyboard::E)
		{
			return "E";
		}

		if (key == sf::Keyboard::F)
		{
			return "F";
		}

		if (key == sf::Keyboard::G)
		{
			return "G";
		}

		if (key == sf::Keyboard::H)
		{
			return "H";
		}

		if (key == sf::Keyboard::I)
		{
			return "I";
		}

		if (key == sf::Keyboard::J)
		{
			return "J";
		}

		if (key == sf::Keyboard::K)
		{
			return "K";
		}

		if (key == sf::Keyboard::L)
		{
			return "L";
		}

		if (key == sf::Keyboard::M)
		{
			return "M";
		}

		if (key == sf::Keyboard::N)
		{
			return "N";
		}

		if (key == sf::Keyboard::O)
		{
			return "O";
		}

		if (key == sf::Keyboard::P)
		{
			return "P";
		}

		if (key == sf::Keyboard::Q)
		{
			return "Q";
		}

		if (key == sf::Keyboard::R)
		{
			return "R";
		}

		if (key == sf::Keyboard::S)
		{
			return "S";
		}

		if (key == sf::Keyboard::T)
		{
			return "T";
		}

		if (key == sf::Keyboard::U)
		{
			return "U";
		}

		if (key == sf::Keyboard::V)
		{
			return "V";
		}

		if (key == sf::Keyboard::W)
		{
			return "W";
		}

		if (key == sf::Keyboard::X)
		{
			return "X";
		}

		if (key == sf::Keyboard::Y)
		{
			return "Y";
		}

		if (key == sf::Keyboard::Z)
		{
			return "Z";
		}

		if (key == sf::Keyboard::Num0)
		{
			return "0";
		}

		if (key == sf::Keyboard::Num1)
		{
			return "1";
		}

		if (key == sf::Keyboard::Num2)
		{
			return "2";
		}

		if (key == sf::Keyboard::Num3)
		{
			return "3";
		}

		if (key == sf::Keyboard::Num4)
		{
			return "4";
		}

		if (key == sf::Keyboard::Num5)
		{
			return "5";
		}

		if (key == sf::Keyboard::Num6)
		{
			return "6";
		}

		if (key == sf::Keyboard::Num7)
		{
			return "7";
		}

		if (key == sf::Keyboard::Num8)
		{
			return "8";
		}

		if (key == sf::Keyboard::Num9)
		{
			return "9";
		}

		if (key == sf::Keyboard::Escape)
		{
			return "Escape";
		}

		if (key == sf::Keyboard::LControl)
		{
			return "LControl";
		}

		if (key == sf::Keyboard::LShift)
		{
			return "LShift";
		}

		if (key == sf::Keyboard::LAlt)
		{
			return "LAlt";
		}

		if (key == sf::Keyboard::LSystem)
		{
			return "LSystem";
		}

		if (key == sf::Keyboard::RControl)
		{
			return "RControl";
		}

		if (key == sf::Keyboard::RShift)
		{
			return "RShift";
		}

		if (key == sf::Keyboard::RAlt)
		{
			return "RAlt";
		}

		if (key == sf::Keyboard::RSystem)
		{
			return "RSystem";
		}

		if (key == sf::Keyboard::Menu)
		{
			return "Menu";
		}

		if (key == sf::Keyboard::LBracket)
		{
			return "LBracket";
		}

		if (key == sf::Keyboard::RBracket)
		{
			return "RBracket";
		}

		if (key == sf::Keyboard::SemiColon)
		{
			return "SemiColon";
		}

		if (key == sf::Keyboard::Comma)
		{
			return "Comma";
		}

		if (key == sf::Keyboard::Period)
		{
			return "Period";
		}

		if (key == sf::Keyboard::Quote)
		{
			return "Quote";
		}

		if (key == sf::Keyboard::Slash)
		{
			return "Slash";
		}

		if (key == sf::Keyboard::BackSlash)
		{
			return "BackSlash";
		}

		if (key == sf::Keyboard::Tilde)
		{
			return "Tilde";
		}

		if (key == sf::Keyboard::Equal)
		{
			return "Equal";
		}

		if (key == sf::Keyboard::Dash)
		{
			return "Dash";
		}

		if (key == sf::Keyboard::Space)
		{
			return "Space";
		}

		if (key == sf::Keyboard::Return)
		{
			return "Return";
		}

		if (key == sf::Keyboard::BackSpace)
		{
			return "BackSpace";
		}

		if (key == sf::Keyboard::Tab)
		{
			return "Tab";
		}

		if (key == sf::Keyboard::PageUp)
		{
			return "PageUp";
		}

		if (key == sf::Keyboard::PageDown)
		{
			return "PageDown";
		}

		if (key == sf::Keyboard::End)
		{
			return "End";
		}

		if (key == sf::Keyboard::Home)
		{
			return "Home";
		}

		if (key == sf::Keyboard::Insert)
		{
			return "Insert";
		}

		if (key == sf::Keyboard::Delete)
		{
			return "Delete";
		}

		if (key == sf::Keyboard::Add)
		{
			return "Add";
		}

		if (key == sf::Keyboard::Subtract)
		{
			return "Subtract";
		}

		if (key == sf::Keyboard::Multiply)
		{
			return "Multiply";
		}

		if (key == sf::Keyboard::Divide)
		{
			return "Divide";
		}

		if (key == sf::Keyboard::Left)
		{
			return "Left";
		}

		if (key == sf::Keyboard::Right)
		{
			return "Right";
		}

		if (key == sf::Keyboard::Up)
		{
			return "Up";
		}

		if (key == sf::Keyboard::Down)
		{
			return "Down";
		}

		if (key == sf::Keyboard::Numpad0)
		{
			return "Numpad0";
		}

		if (key == sf::Keyboard::Numpad1)
		{
			return "Numpad1";
		}

		if (key == sf::Keyboard::Numpad2)
		{
			return "Numpad2";
		}

		if (key == sf::Keyboard::Numpad3)
		{
			return "Numpad3";
		}

		if (key == sf::Keyboard::Numpad4)
		{
			return "Numpad4";
		}

		if (key == sf::Keyboard::Numpad5)
		{
			return "Numpad5";
		}

		if (key == sf::Keyboard::Numpad6)
		{
			return "Numpad6";
		}

		if (key == sf::Keyboard::Numpad7)
		{
			return "Numpad7";
		}

		if (key == sf::Keyboard::Numpad8)
		{
			return "Numpad8";
		}

		if (key == sf::Keyboard::Numpad9)
		{
			return "Numpad9";
		}

		if (key == sf::Keyboard::F1)
		{
			return "F1";
		}

		if (key == sf::Keyboard::F2)
		{
			return "F2";
		}

		if (key == sf::Keyboard::F3)
		{
			return "F3";
		}

		if (key == sf::Keyboard::F4)
		{
			return "F4";
		}

		if (key == sf::Keyboard::F5)
		{
			return "F5";
		}

		if (key == sf::Keyboard::F6)
		{
			return "F6";
		}

		if (key == sf::Keyboard::F7)
		{
			return "F7";
		}

		if (key == sf::Keyboard::F8)
		{
			return "F8";
		}

		if (key == sf::Keyboard::F9)
		{
			return "F9";
		}

		if (key == sf::Keyboard::F10)
		{
			return "F10";
		}

		if (key == sf::Keyboard::F11)
		{
			return "F11";
		}

		if (key == sf::Keyboard::F12)
		{
			return "F12";
		}

		if (key == sf::Keyboard::F13)
		{
			return "F13";
		}

		if (key == sf::Keyboard::F14)
		{
			return "F14";
		}

		if (key == sf::Keyboard::F15)
		{
			return "F15";
		}

		if (key == sf::Keyboard::Pause)
		{
			return "Pause";
		}

		return "";
	}

	bool configurator::_upd_ini_key(CSimpleIniA& conf, sf::Keyboard::Key const& setting, std::string const& table, std::string const& option)
	{
		SI_Error rc = conf.SetValue( /* section */ table.c_str(), /* name */ option.c_str(), /* value */ _convert_key_to_str(setting).c_str());

		if (rc < 0)
		{
			lc::utils::log(_LOGPATH, "From configurator::_upd_key: Error occured trying to update key with value '" + table + ": " + option + "'!");
			return false;
		}

		return true;
	}

	bool configurator::_upd_db_key(sqlite3 *db, sf::Keyboard::Key const& setting, std::string const& table, std::string const& option)
	{
		// sql statement tmp
		std::string sql;

		// error message
		char *error_msg;

		// return code for testing if successful
		int rc;

		// get key as string
		std::string key = _convert_key_to_str(setting);

		if (key == "")
		{
			lc::utils::log(_LOGPATH, "From configurator::_upd_key: Error occured trying to update key with value '" + table + ": " + option + "'!");
			return false;
		}

		sql =
			// update value
			"UPDATE " + table + " SET VALUE = '" + key + "' WHERE NAME = '" + option + "'";
		// execute
		rc = sqlite3_exec(db, sql.c_str(), NULL, 0, &error_msg);
		if (rc != SQLITE_OK)
		{
			lc::utils::log(_LOGPATH, "From configurator::_upd_key: Error occured trying to update key with value '" + table + ": " + option + "'!");
#if _DEBUG
			std::cout << error_msg << std::endl;
#endif
			return false;
		}

		return true;
	}
}
