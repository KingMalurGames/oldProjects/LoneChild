#ifndef __LC__CONFIGURATOR__
#define __LC__CONFIGURATOR__

// lc
#include "utils.hpp"
#include "definitions.hpp"

// std
#include <string>
#include <winsqlite/winsqlite3.h>
#include <memory>
#include <io.h>
#include <iostream>
#include <unordered_map>

// lib
#include <SFML/Graphics.hpp>
#include "../include/simpleini_4.17/SimpleIni.h"

// NEngine
#include "../include/NEngine_1.0.0/ninput_manager/ninput_manager.hpp"

namespace lc {

	enum class style
	{
		None = 0,
		Titlebar = 1 << 0,
		Resize = 1 << 1,
		Close = 1 << 2,
		Fullscreen = 1 << 3,

		Default = Titlebar | Resize | Close
	};

	struct audio
	{
		unsigned int general = 100;
		unsigned int music = 100;
		unsigned int effects = 100;
	};

	struct graphics
	{
		bool vertical_sync = false;
		unsigned int width = 1280;
		unsigned int height = 720;
		unsigned int antialiasing = 0;
		style style = style::Close; // to store window style of sfml
	};

	struct general
	{
		std::string title = "Lone Child";
		float splash_time = 2.5f;
	};

	struct paths
	{
		// install path
		std::string install_path = lc::utils::get_working_dir();

		// settings file
#if _USINGDB
		std::string config_path = "./settings.db";
#elif _USINGINI
		std::string config_path = "./settings.ini";
#endif

		// log
		std::string log_path = "./lone_child.log";

		// theme file
		std::string theme = "./conf/theme.thm";

		// logos ( lo_ )
		std::string lo_splash = "./res/lo/logo.png";

		// backgrounds ( bd_ )
		std::string bd_main = "./res/bd/main.png";
		std::string bd_pause = "./res/bd/pause.png";
		std::string bd_load = "./res/bd/load.png";
		std::string bd_audio = "./res/bd/audio.png";
		std::string bd_user = "./res/bd/user.png";
		std::string bd_user_add = "./res/bd/user_add.png";
		std::string bd_user_load = "./res/bd/user_load.png";
		std::string bd_user_del = "./res/bd/user_del.png";
		std::string bd_key = "./res/bd/key.png";
		std::string bd_graphics = "./res/bd/graphics.png";
		std::string bd_save = "./res/bd/save.png";
		std::string bd_save_del = "./res/bd/save_del.png";
		std::string bd_tutorial_load = "./res/bd/tutorial_load.png";
		std::string bd_tutorial_yn = "./res/bd/tutorial_yn.png";

		// fonts ( ft_ )
		std::string ft_exo_std = "./res/ft/exo_std.otf";

		// spritesheets ( st_ )

		// buttons ( bn_ )
		std::string bn_std = "./res/bn/bn_std.png";
		std::string bn_forward = "./res/bn/bn_forward.png";
		std::string bn_backward = "./res/bn/bn_backward.png";
	};

	struct keyboard
	{
		std::unordered_map<std::string, sf::Keyboard::Key> keybindings;

		bool check_existance(std::string const& key)
		{
			if (keybindings.find(key) != keybindings.end())
			{ // exists
				return true;
			}
			else
			{
				return false;
			}
		}

		bool check_existance(sf::Keyboard::Key key)
		{
			for (const auto& it : keybindings)
			{
				if (it.second == key)
				{ // exists
					return true;
				}
			}

			return false;
		}

		void replace(std::string const& key, sf::Keyboard::Key const& new_key)
		{
			if (check_existance(key))
			{
				keybindings.erase(key);
			}

			keybindings.insert(std::make_pair(key, new_key));
		}
	};

	struct settings
	{
		// SFML configuration settings
		sf::ContextSettings context_settings;

		// own configuration variables
		general general; // general settings
		audio audio; // audio settings
		graphics graphics; // graphical settings
		paths paths; // relevant file paths
		keyboard keyboard; // all key bindings just for displaying
	};

	class configurator
	{
	public:
		configurator();

		bool load_ini(std::string const& path_to_config, settings& settings, nengine::ninput_manager::ninput_manager& input);

		bool load_db(std::string const& path_to_config, settings& settings, nengine::ninput_manager::ninput_manager& input);

		template <typename T>
		bool upd_ini(std::string const& path_to_config, T const& setting, std::string const& table, std::string const& option)
		{
#if _DEBUG
			std::chrono::steady_clock::time_point begin = std::chrono::steady_clock::now();
#endif
			// format install path
			std::string string_path_to_config = path_to_config;
			lc::utils::replace_all_substrings(string_path_to_config, "\\", "/");

			// open and read ini file
			CSimpleIniA conf;
			conf.SetUnicode();
			SI_Error rc = conf.LoadFile(string_path_to_config.c_str());

			if (rc < 0) // no ini found
			{
				return false;
			}

			// update ini
			bool rb = _upd_ini(conf, setting, table, option);

			if (rc < 0)
			{
				return false;
			}
			else
			{
				conf.SaveFile(path_to_config.c_str());
			}

#if _DEBUG
			std::chrono::steady_clock::time_point end = std::chrono::steady_clock::now();
			std::cerr << "Updating Ini took: " << std::chrono::duration_cast<std::chrono::milliseconds>(end - begin).count() / 1000.f << " seconds" << std::endl;
#endif
			return rb;
		}

		template <typename T>
		bool upd_db(std::string const& path_to_config, T const& setting, std::string const& table, std::string const& option)
		{
#if _DEBUG
			std::chrono::steady_clock::time_point begin = std::chrono::steady_clock::now();
#endif
			// test if settings table exists
			bool exists;
			if (_access(path_to_config.c_str(), 0) != -1)
			{ // exists
				exists = true;
			}
			else
			{ // does not exists
				exists = false;
			}

			sqlite3 *db; // open connection
			int db_handle = sqlite3_open(path_to_config.c_str(), &db);
			if (db_handle) // if 0 --> false
			{
				return false;
			}

			if (!exists)
			{
				return false;
			}

			// update db
			bool rb = _upd_db(db, setting, table, option);

			// close and return true
			sqlite3_close(db);

#if _DEBUG
			std::chrono::steady_clock::time_point end = std::chrono::steady_clock::now();
			std::cerr << "Updating DB took: " << std::chrono::duration_cast<std::chrono::milliseconds>(end - begin).count() / 1000.f << " seconds" << std::endl;
#endif

			return rb;
		}

		bool upd_ini_key(std::string const& path_to_config, sf::Keyboard::Key const& setting, std::string const& table, std::string const& option);

		bool upd_db_key(std::string const& path_to_config, sf::Keyboard::Key const& setting, std::string const& table, std::string const& option);

		sf::Keyboard::Key convert_str_to_key(std::string const& key);

		std::string convert_key_to_str(sf::Keyboard::Key const& key);

	private:
		bool _load_ini(std::string const& path_to_config, settings& settings, CSimpleIniA& conf, nengine::ninput_manager::ninput_manager& input);

		bool _load_ini_string(std::string const& table, std::string const& option, std::string& setting, CSimpleIniA& conf);

		bool _load_ini_float(std::string const& table, std::string const& option, float& setting, CSimpleIniA& conf, float min, float max);

		bool _load_ini_int(std::string const& table, std::string const& option, unsigned int& setting, CSimpleIniA& conf, unsigned int min, unsigned int max);

		bool _load_ini_bool(std::string const& table, std::string const& option, bool& setting, CSimpleIniA& conf);

		bool _load_ini_key(std::string const& table, std::string const& option, std::string const& bind_name, sf::Keyboard::Key default, CSimpleIniA& conf, nengine::ninput_manager::ninput_manager& input, settings& settings);

		bool _load_db(sqlite3 *db, settings& settings, nengine::ninput_manager::ninput_manager& input);

		void _load_db_string(std::string const& table, sqlite3_stmt* stmt, std::string const& key, std::string const& key_to_read, unsigned int index, std::string& setting);

		void _load_db_float(std::string const& table, sqlite3_stmt* stmt, std::string const& key, std::string const& key_to_read, unsigned int index, float& setting, float min, float max);

		void _load_db_int(std::string const& table, sqlite3_stmt* stmt, std::string const& key, std::string const& key_to_read, unsigned int index, unsigned int& setting, unsigned int min, unsigned int max);

		void _load_db_bool(std::string const& table, sqlite3_stmt* stmt, std::string const& key, std::string const& key_to_read, unsigned int index, bool& setting);

		void _load_db_key(std::string const& table, sqlite3_stmt* stmt, std::string const& key, std::string const& key_to_read, std::string const& bind_name, sf::Keyboard::Key default, nengine::ninput_manager::ninput_manager& input, settings& settings);

		bool _fill_ini(std::string const& path_to_config, CSimpleIniA& conf);

		bool _fill_db(sqlite3 *db);

		sf::Keyboard::Key _convert_str_to_key(std::string const& key);

		std::string _convert_key_to_str(sf::Keyboard::Key const& key);

		bool _upd_ini_key(CSimpleIniA& conf, sf::Keyboard::Key const& setting, std::string const& table, std::string const& option);

		bool _upd_db_key(sqlite3 *db, sf::Keyboard::Key const& setting, std::string const& table, std::string const& option);

		template <typename T>
		bool _upd_ini(CSimpleIniA& conf, T const& setting, std::string const& table, std::string const& option)
		{
			SI_Error rc = conf.SetValue( /* section */ table.c_str(), /* name */ option.c_str(), /* value */ std::to_string(setting).c_str());

			if (rc < 0)
			{
				return false;
			}

			return true;
		}

		template <>
		bool _upd_ini(CSimpleIniA& conf, std::string const& setting, std::string const& table, std::string const& option)
		{
			SI_Error rc = conf.SetValue( /* section */ table.c_str(), /* name */ option.c_str(), /* value */ setting.c_str());

			if (rc < 0)
			{
				return false;
			}

			return true;
		}

		template <typename T>
		bool _upd_db(sqlite3 *db, T const& setting, std::string const& table, std::string const& option)
		{
			// sql statement tmp
			std::string sql;

			// error message
			char *error_msg;

			sql =
				// update value
				"UPDATE " + table + " SET VALUE = '" + std::to_string(setting) + "' WHERE NAME = '" + option + "'";

			// execute
			int rc = sqlite3_exec(db, sql.c_str(), NULL, 0, &error_msg);
			if (rc != SQLITE_OK)
			{
				lc::utils::log(_LOGPATH, "From configurator::_upd_db<T>: '" + std::string(error_msg) + "'!");
#if _DEBUG
				std::cout << error_msg << std::endl;
#endif
				return false;
			}

			return true;
		}

		template <>
		bool _upd_db(sqlite3 *db, std::string const& setting, std::string const& table, std::string const& option)
		{
			// sql statement tmp
			std::string sql;

			// error message
			char *error_msg;

			sql =
				// update value
				"UPDATE " + table + " SET VALUE = '" + setting + "' WHERE NAME = '" + option + "'";

			// execute
			int rc = sqlite3_exec(db, sql.c_str(), NULL, 0, &error_msg);
			if (rc != SQLITE_OK)
			{
				lc::utils::log(_LOGPATH, "From configurator::_upd_db<>: '" + std::string(error_msg) + "'!");
#if _DEBUG
				std::cout << error_msg << std::endl;
#endif
				return false;
			}

			return true;
		}
	};

}

#endif
