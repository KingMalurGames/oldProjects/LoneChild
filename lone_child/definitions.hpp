// which configuration
// boolean
//     0 --> false
//     non 0 --> true
#define _USINGDB 0
#define _USINGINI 1

// clearing resources
#define _CLRINTERVALL 360.f

// time played message
#define _GAMEINTERVALL 3600.f

// graphic options
#define _MINWIDTH 640
#define _MAXWIDTH 1920
#define _MINHEIGHT 360
#define _MAXHEIGHT 1080
#define _WIDTHSTEP 16
#define _HEIGHTSTEP 9
#define _MINANTIALIASING 0
#define _MAXANTIALIASING 32
#define _ANTIALIASINGSTEP 2

// naming schema for buttons, labels etc
//     <classname>_<tguitype>_<tguitext>
// example button
//     options_bn_back
// example label
//     options_graphics_lbl_graphics
// these names can get very long...

// audio options
#define _VOLUMESTEP 5
#define _MINVOLUME 0
#define _MAXVOLUME 100

// keybinding options
#define _KEYINPUTTIME 5.f
#define _KEYTAKENTIME 2.f

// config path
#define _LOGPATH "./lone_child.log"
