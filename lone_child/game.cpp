// lc
#include "game.hpp"
#include "definitions.hpp"
#include "global_message.hpp"

// std

// lib

// NEngine

namespace lc {

	game::game(std::shared_ptr<game_data> data)
		: _data(data)
		, _widgets()
		, _gc()
		, _tp(_GAMEINTERVALL)
	{
	}

	game::~game()
	{
#if _DEBUG
		std::cerr << "destructor: game" << std::endl;
#endif
		_clear_widgets();
	}

	void game::init()
	{
#if _DEBUG
		std::cerr << "init: game" << std::endl;
#endif
		_data->resources.load_font("ft_exo_std", _data->settings.paths.ft_exo_std);
		_load_widgets();
		_gc.restart();
	}

	void game::pause()
	{
		_hide_widgets();
	}

	void game::resume()
	{
		_show_widgets();
	}

	void game::handle()
	{
		sf::Event event;
		while (_data->window.pollEvent(event))
		{
			if (event.type == sf::Event::Closed)
			{
				// only force close
				_data->window.close();
			}

			_data->gui.handleEvent(event);
		}
	}

	void game::update(float dt)
	{
		_check_play_time();
	}

	void game::draw(float dt)
	{
		_data->window.setActive(true);

		_data->window.clear(sf::Color::Black);

		_data->gui.draw();
		
		_data->window.display();

		_data->window.setActive(false);
	}

	void game::_check_play_time()
	{
		if (_gc.getElapsedTime().asSeconds() >= _tp)
		{
			std::string message = "You already played " + std::to_string((int) (_gc.getElapsedTime().asSeconds() / 60) / 60) + " hour(s)!";
			// show message
			// TODO: show message in global message box
#if _DEBUG
			std::cerr << message << std::endl;
#endif
			auto message_box = std::make_shared<lc::global_message>(message, _data->resources.get_font_p("ft_exo_std"), sf::Color::Black, 0, 0, _data->window.getSize().x, _data->window.getSize().y);

			_tp += _GAMEINTERVALL;
		}
	}

	void game::_load_widgets()
	{
	}

	void game::_exit()
	{
	}

	void game::_clear_widgets()
	{
		// delete the states widgets
		for (unsigned int i = 0; i < _widgets.size(); i++)
		{
			_data->gui.remove(_data->gui.get(_widgets.at(i)));
		}
	}

	void game::_hide_widgets()
	{
		// delete the states widgets
		for (unsigned int i = 0; i < _widgets.size(); i++)
		{
			_data->gui.get(_widgets.at(i))->hide();
		}
	}

	void game::_show_widgets()
	{
		// delete the states widgets
		for (unsigned int i = 0; i < _widgets.size(); i++)
		{
			_data->gui.get(_widgets.at(i))->show();
		}
	}
}
