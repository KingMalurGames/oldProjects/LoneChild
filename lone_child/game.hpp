#ifndef __LC__GAME__
#define __LC__GAME__

// lc
#include "application.hpp"

// std
#include <string>
#include <memory>

// lib
#include <SFML/Graphics.hpp>

// NEngine

namespace lc {

	class game : public nengine::nstate_manager::nstate
	{
	public:
		game(std::shared_ptr<game_data> data);
		~game();

		void init();
		void pause();
		void resume();
		void handle();
		void update(float dt);
		void draw(float dt);
		
	private:
		std::shared_ptr<game_data> _data;
		std::vector<std::string> _widgets;
		sf::Clock _gc; // game clock for showing "You now played n minutes. It's time for a pause!"
		float _tp; // time played gets increased with _gc.restart().asSeconds() and displayed as (int) cast

		void _check_play_time();

		void _load_widgets();

		void _exit();

		void _clear_widgets();
		void _hide_widgets();
		void _show_widgets();
	};

}

#endif
