// lc
#include "global_message.hpp"
#include "definitions.hpp"
#include "game.hpp"

// std

// lib

// NEngine

namespace lc {

	global_message::global_message(std::string const& message, std::shared_ptr<const sf::Font> font, sf::Color color, int x, int y, float size_x, float size_y)
		: _widgets()
		, _background(sf::Vector2f(size_x - 1, size_y - 1))
		, _message()
		, _font(std::move(font))
	{
		_background.setFillColor(color);
		_background.setOutlineThickness(1);
		_background.setOutlineColor(sf::Color::White);
		_background.setPosition(sf::Vector2f(x - 1, y - 1));
		_message.setFillColor(sf::Color::White);
		_message.setString(message);
		_message.setPosition(x, y);
		_message.setCharacterSize(20);
	}

	global_message::~global_message()
	{
#if _DEBUG
		std::cerr << "destructor: global_message" << std::endl;
#endif
	}
}
