#ifndef __LC__GLOBAL_MASSAGE__
#define __LC__GLOBAL_MASSAGE__

// lc
#include "application.hpp"

// std
#include <string>
#include <memory>

// lib
#include <SFML/Graphics.hpp>

// NEngine

namespace lc {

	class global_message : public sf::Drawable
	{
	public:
		global_message(std::string const& message, std::shared_ptr<const sf::Font> font, sf::Color color, int x, int y, float size_x, float size_y);
		~global_message();

	private:
		std::vector<std::string> _widgets;
		sf::RectangleShape _background;
		sf::Text _message;
		std::shared_ptr<const sf::Font> _font;

		virtual void draw(sf::RenderTarget& target, sf::RenderStates states) const
		{
			target.draw(_background, states);
			target.draw(_message, states);
		}
	};

}

#endif
