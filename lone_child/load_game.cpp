// lc
#include "load_game.hpp"
#include "definitions.hpp"
#include "game.hpp"

// std

// lib

// NEngine

namespace lc {

	load_game::load_game(std::shared_ptr<game_data> data)
		: _data(data)
		, _widgets()
	{
	}

	load_game::~load_game()
	{
#if _DEBUG
		std::cerr << "destructor: load_game" << std::endl;
#endif
		_clear_widgets();
	}

	void load_game::init()
	{
#if _DEBUG
		std::cerr << "init: load_game" << std::endl;
#endif
	}

	void load_game::pause()
	{
		_hide_widgets();
	}

	void load_game::resume()
	{
		_show_widgets();
	}

	void load_game::handle()
	{
		sf::Event event;
		while (_data->window.pollEvent(event))
		{
			if (event.type == sf::Event::Closed)
			{
				// only force close
				_data->window.close();
			}

			_data->gui.handleEvent(event);
		}
	}

	void load_game::update(float dt)
	{
	}

	void load_game::draw(float dt)
	{
		_data->window.setActive(true);

		_data->window.clear(sf::Color::Black);

		_data->gui.draw();

		_data->window.display();

		_data->window.setActive(false);
	}

	void load_game::_load_widgets()
	{
		// load theme
		tgui::Theme::Ptr theme = tgui::Theme::create(_data->settings.paths.theme);

		// y start point, get's increased by step every widget row
		unsigned int y = 20;
		unsigned int row = 0;
		unsigned int step = 40;

		// Label New Game

		// TextBox Name

		// Selector class
	}

	void load_game::_exit()
	{
		_data->states.remove();
		_clear_widgets();
	}

	void load_game::_clear_widgets()
	{
		// delete the states widgets
		for (unsigned int i = 0; i < _widgets.size(); i++)
		{
			_data->gui.remove(_data->gui.get(_widgets.at(i)));
		}
	}

	void load_game::_hide_widgets()
	{
		// delete the states widgets
		for (unsigned int i = 0; i < _widgets.size(); i++)
		{
			_data->gui.get(_widgets.at(i))->hide();
		}
	}

	void load_game::_show_widgets()
	{
		// delete the states widgets
		for (unsigned int i = 0; i < _widgets.size(); i++)
		{
			_data->gui.get(_widgets.at(i))->show();
		}
	}
}
