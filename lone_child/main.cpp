// lc
#include "application.hpp"

// std
#include <memory>

// lib

// NEngine

int main()
{
	std::unique_ptr<lc::application> _application = std::make_unique<lc::application>();

	return _application->run();
}
