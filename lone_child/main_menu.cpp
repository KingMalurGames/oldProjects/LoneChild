// lc
#include "main_menu.hpp"
#include "options.hpp"
#include "load_game.hpp"
#include "game.hpp"

// std

// lib
#include <SFML/Graphics.hpp>
#include <TGUI/TGUI.hpp>

// NEngine
#include "../include/NEngine_1.0.0/nstate_manager/nstate.hpp"

namespace lc {

	main_menu::main_menu(std::shared_ptr<game_data> data)
		: _data(data)
		, _clock()
	{
	}

	main_menu::~main_menu()
	{
#if _DEBUG
		std::cerr << "destructor: main_menu" << std::endl;
#endif
		_clear_widgets();
		_data->window.close();
	}

	void main_menu::init()
	{
#if _DEBUG
		std::cerr << "init: main_menu" << std::endl;
#endif
		// resources
		_data->resources.load_texture("bd_main", _data->settings.paths.bd_main);
		_data->resources.load_font("ft_exo_std", _data->settings.paths.ft_exo_std);
		_data->resources.load_texture("bn_std", _data->settings.paths.bn_std);
		_data->resources.load_texture("bn_forward", _data->settings.paths.bn_forward);
		_data->resources.load_texture("bn_backward", _data->settings.paths.bn_backward);

		// gui
		_load_widgets();
	}

	void main_menu::pause()
	{
		_hide_widgets();
	}

	void main_menu::resume()
	{
		_show_widgets();
	}

	void main_menu::handle()
	{
		sf::Event event;
		while (_data->window.pollEvent(event))
		{
			if (event.type == sf::Event::Closed)
			{
				// only force close
				_data->window.close();
			}

			_data->gui.handleEvent(event);
		}
	}

	void main_menu::update(float dt)
	{
	}

	void main_menu::draw(float dt)
	{
		_data->window.setActive(true);

		_data->window.clear(sf::Color::Black);

		_data->gui.draw();

		_data->window.display();

		_data->window.setActive(false);
	}

	void main_menu::_load_widgets()
	{
		// load theme
		tgui::Theme::Ptr theme = tgui::Theme::create(_data->settings.paths.theme);

		// y start point, get's increased by step every widget row
		unsigned int y = 20;
		unsigned int row = 0;
		unsigned int step = 40;
		unsigned int offset = 5;

		// label Options
		tgui::Label::Ptr main_menu_lbl_main_menu = theme->load("Label");
		main_menu_lbl_main_menu->setText("Main Menu");
		main_menu_lbl_main_menu->setTextSize(40);
		main_menu_lbl_main_menu->setPosition(tgui::Layout2d(tgui::bindWidth(_data->gui) / 2.0 - tgui::bindWidth(main_menu_lbl_main_menu) / 2.0, y + step * row));
		_data->gui.add(main_menu_lbl_main_menu, "main_menu_lbl_main_menu");
		_widgets.push_back("main_menu_lbl_main_menu");

		row++;
		row++;
		row++;

		// button New Game
		tgui::Button::Ptr main_menu_bn_new_game = tgui::Button::create();
		main_menu_bn_new_game->setText("New Game");
		main_menu_bn_new_game->setPosition(tgui::Layout2d(tgui::bindWidth(_data->gui) / 2.0 - tgui::bindWidth(main_menu_bn_new_game) - offset, y + step * row));
		main_menu_bn_new_game->connect("pressed", &main_menu::_new_game, this);
		_data->gui.add(main_menu_bn_new_game, "main_menu_bn_new_game");
		_widgets.push_back("main_menu_bn_new_game");

		// button Load Game
		tgui::Button::Ptr main_menu_bn_load_game = tgui::Button::create();
		main_menu_bn_load_game->setText("Load Game");
		main_menu_bn_load_game->setPosition(tgui::Layout2d(tgui::bindWidth(_data->gui) / 2.0 + offset, y + step * row));
		main_menu_bn_load_game->connect("pressed", &main_menu::_load_game, this);
		_data->gui.add(main_menu_bn_load_game, "main_menu_bn_load_game");
		_widgets.push_back("main_menu_bn_load_game");

		row++;

		// button Options
		tgui::Button::Ptr main_menu_bn_options = tgui::Button::create();
		main_menu_bn_options->setText("Options");
		main_menu_bn_options->setPosition(tgui::Layout2d(tgui::bindWidth(_data->gui) / 2.0 - tgui::bindWidth(main_menu_bn_options) / 2.0, y + step * row));
		main_menu_bn_options->connect("pressed", &main_menu::_options, this);
		_data->gui.add(main_menu_bn_options, "main_menu_bn_options");
		_widgets.push_back("main_menu_bn_options");

		row++;

		// button Exit
		tgui::Button::Ptr main_menu_bn_exit = tgui::Button::create();
		main_menu_bn_exit->setText("Exit");
		main_menu_bn_exit->setPosition(tgui::Layout2d(tgui::bindWidth(_data->gui) / 2.0 - tgui::bindWidth(main_menu_bn_exit) / 2.0, y + step * row));
		main_menu_bn_exit->connect("pressed", &main_menu::_exit, this);
		_data->gui.add(main_menu_bn_exit, "main_menu_bn_exit");
		_widgets.push_back("main_menu_bn_exit");
	}

	void main_menu::_new_game()
	{
		_data->states.add(std::unique_ptr<nengine::nstate_manager::nstate>(new lc::game(_data)), false);
	}

	void main_menu::_load_game()
	{
		_data->states.add(std::unique_ptr<nengine::nstate_manager::nstate>(new lc::load_game(_data)), false);
	}

	void main_menu::_options()
	{
		_data->states.add(std::unique_ptr<nengine::nstate_manager::nstate>(new lc::options(_data)), false);
	}

	void main_menu::_exit()
	{
		_data->window.close();
		_clear_widgets();
	}

	void main_menu::_clear_widgets()
	{
		// delete the states widgets
		for (unsigned int i = 0; i < _widgets.size(); i++)
		{
			_data->gui.remove(_data->gui.get(_widgets.at(i)));
		}
	}

	void main_menu::_hide_widgets()
	{
		// delete the states widgets
		for (unsigned int i = 0; i < _widgets.size(); i++)
		{
			_data->gui.get(_widgets.at(i))->hide();
		}
	}

	void main_menu::_show_widgets()
	{
		// delete the states widgets
		for (unsigned int i = 0; i < _widgets.size(); i++)
		{
			_data->gui.get(_widgets.at(i))->show();
		}
	}
}
