#ifndef __LC__MAIN_MENU__
#define __LC__MAIN_MENU__

// lc
#include "application.hpp"

// std
#include <memory>
#include <vector>

// lib
#include <SFML/Graphics.hpp>

// NEngine
#include "../include/NEngine_1.0.0/nstate_manager/nstate.hpp"

namespace lc {

	class main_menu : public nengine::nstate_manager::nstate
	{
	public:
		main_menu(std::shared_ptr<game_data> data);
		~main_menu();
		void init();
		void pause();
		void resume();
		void handle();
		void update(float dt);
		void draw(float dt);

	private:
		std::shared_ptr<game_data> _data;
		sf::Clock _clock;
		std::vector<std::string> _widgets;

		void _load_widgets();

		void _new_game();
		void _load_game();
		void _options();
		void _exit();

		void _clear_widgets();
		void _hide_widgets();
		void _show_widgets();
	};

}

#endif
