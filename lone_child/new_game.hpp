#ifndef __LC__NEW_GAME__
#define __LC__NEW_GAME__

// lc
#include "application.hpp"

// std
#include <string>
#include <memory>

// lib
#include <SFML/Graphics.hpp>

// NEngine

namespace lc {

	class new_game : public nengine::nstate_manager::nstate
	{
	public:
		new_game(std::shared_ptr<game_data> data);
		~new_game();

		void init();
		void pause();
		void resume();
		void handle();
		void update(float dt);
		void draw(float dt);

	private:
		std::shared_ptr<game_data> _data;
		std::vector<std::string> _widgets;

		void _load_widgets();
		void _load_widgets_new_game();
		void _load_widgets_load_game();

		void _exit();

		void _clear_widgets();
		void _hide_widgets();
		void _show_widgets();
	};

}

#endif
