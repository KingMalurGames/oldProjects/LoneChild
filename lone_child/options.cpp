// lc
#include "definitions.hpp"
#include "options.hpp"
#include "options_graphics.hpp"
#include "options_audio.hpp"
#include "options_keybindings.hpp"

// std

// lib
#include <SFML/Graphics.hpp>
#include <TGUI/TGUI.hpp>

// NEngine
#include "../include/NEngine_1.0.0/nstate_manager/nstate.hpp"

namespace lc
{
	options::options(std::shared_ptr<game_data> data)
		: _data(data)
	{
	}

	options::~options()
	{
#if _DEBUG
		std::cerr << "destructor: options" << std::endl;
#endif
		_clear_widgets();
	}

	void options::init()
	{
#if _DEBUG
		std::cerr << "init: options" << std::endl;
#endif
		// resources

		// gui
		_load_widgets();
	}

	void options::pause()
	{
		_hide_widgets();
	}

	void options::resume()
	{
		_show_widgets();
	}

	void options::handle()
	{
		sf::Event event;
		while (_data->window.pollEvent(event))
		{
			if (event.type == sf::Event::Closed)
			{
				// only force close
				_data->window.close();
			}

			_data->gui.handleEvent(event);
		}
	}

	void options::update(float dt)
	{
	}

	void options::draw(float dt)
	{
		_data->window.setActive(true);

		_data->window.clear(sf::Color::Black);

		_data->gui.draw();

		_data->window.display();

		_data->window.setActive(false);
	}

	void options::_load_widgets()
	{
		// load theme
		tgui::Theme::Ptr theme = tgui::Theme::create(_data->settings.paths.theme);

		// y start point, get's increased by step every widget row
		unsigned int y = 20;
		unsigned int row = 0;
		unsigned int step = 40;

		// label Options
		tgui::Label::Ptr options_lbl_options = theme->load("Label");
		options_lbl_options->setText("Options");
		options_lbl_options->setTextSize(40);
		options_lbl_options->setPosition(tgui::Layout2d(tgui::bindWidth(_data->gui) / 2.0 - tgui::bindWidth(options_lbl_options) / 2.0, y + step * row));
		_data->gui.add(options_lbl_options, "options_lbl_options");
		_widgets.push_back("options_lbl_options");

		row++;
		row++;
		row++;

		// button Graphics
		tgui::Button::Ptr options_bn_graphics = tgui::Button::create();
		options_bn_graphics->setText("Graphics");
		options_bn_graphics->setPosition(tgui::Layout2d(tgui::bindWidth(_data->gui) / 2.0 - tgui::bindWidth(options_bn_graphics) / 2.0, y + step * row));
		options_bn_graphics->connect("pressed", &options::_graphics, this);
		_data->gui.add(options_bn_graphics, "options_bn_graphics");
		_widgets.push_back("options_bn_graphics");

		row++;

		// button Audio
		tgui::Button::Ptr options_bn_audio = tgui::Button::create();
		options_bn_audio->setText("Audio");
		options_bn_audio->setPosition(tgui::Layout2d(tgui::bindWidth(_data->gui) / 2.0 - tgui::bindWidth(options_bn_audio) / 2.0, y + step * row));
		options_bn_audio->connect("pressed", &options::_audio, this);
		_data->gui.add(options_bn_audio, "options_bn_audio");
		_widgets.push_back("options_bn_audio");

		row++;

		// button Keybindings
		tgui::Button::Ptr options_bn_keybindings = tgui::Button::create();
		options_bn_keybindings->setText("Keybindings");
		options_bn_keybindings->setPosition(tgui::Layout2d(tgui::bindWidth(_data->gui) / 2.0 - tgui::bindWidth(options_bn_keybindings) / 2.0, y + step * row));
		options_bn_keybindings->connect("pressed", &options::_keybindings, this);
		_data->gui.add(options_bn_keybindings, "options_bn_keybindings");
		_widgets.push_back("options_bn_keybindings");

		row++;
		row++;
		row++;

		// button back
		tgui::Button::Ptr options_bn_back = tgui::Button::create();
		options_bn_back->setText("Back");
		options_bn_back->setPosition(tgui::Layout2d(tgui::bindWidth(_data->gui) / 2.0 - tgui::bindWidth(options_bn_back) / 2.0, y + step * row));
		options_bn_back->connect("pressed", &options::_exit, this);
		_data->gui.add(options_bn_back, "options_bn_back");
		_widgets.push_back("options_bn_back");
	}

	void options::_exit()
	{
		_data->states.remove();
		_clear_widgets();
	}

	void options::_clear_widgets()
	{
		// delete the states widgets
		for (unsigned int i = 0; i < _widgets.size(); i++)
		{
			_data->gui.remove(_data->gui.get(_widgets.at(i)));
		}
	}

	void options::_hide_widgets()
	{
		// delete the states widgets
		for (unsigned int i = 0; i < _widgets.size(); i++)
		{
			_data->gui.get(_widgets.at(i))->hide();
		}
	}

	void options::_show_widgets()
	{
		// delete the states widgets
		for (unsigned int i = 0; i < _widgets.size(); i++)
		{
			_data->gui.get(_widgets.at(i))->show();
		}
	}

	void options::_graphics()
	{
		_data->states.add(std::unique_ptr<nengine::nstate_manager::nstate>(new lc::options_graphics(_data)), false);
	}

	void options::_audio()
	{
		_data->states.add(std::unique_ptr<nengine::nstate_manager::nstate>(new lc::options_audio(_data)), false);
	}

	void options::_keybindings()
	{
		_data->states.add(std::unique_ptr<nengine::nstate_manager::nstate>(new lc::options_keybindings(_data)), false);
	}
}
