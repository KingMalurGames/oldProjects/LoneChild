#ifndef __LC__OPTIONS__
#define __LC__OPTIONS__

// lc
#include "application.hpp"

// std
#include <memory>

// lib
#include <SFML/Graphics.hpp>

// NEngine
#include "../include/NEngine_1.0.0/nstate_manager/nstate.hpp"

namespace lc
{

	class options : public nengine::nstate_manager::nstate
	{
	public:
		options(std::shared_ptr<game_data> data);
		~options();
		void init();
		void pause();
		void resume();
		void handle();
		void update(float dt);
		void draw(float dt);

	private:
		std::shared_ptr<game_data> _data;
		std::vector<std::string> _widgets;

		void _load_widgets();

		void _exit();

		void _clear_widgets();
		void _hide_widgets();
		void _show_widgets();

		void _graphics();
		void _audio();
		void _keybindings();
	};

}

#endif
