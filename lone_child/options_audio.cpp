// lc
#include "options_audio.hpp"
#include "definitions.hpp"

// std

// lib
#include <SFML/Graphics.hpp>
#include <TGUI/TGUI.hpp>

// NEngine
#include "../include/NEngine_1.0.0/nstate_manager/nstate.hpp"

namespace lc {

	options_audio::options_audio(std::shared_ptr<game_data> data)
		: _data(data)
		, _general_volume(_data->settings.audio.general)
		, _music_volume(_data->settings.audio.music)
		, _effects_volume(_data->settings.audio.effects)
	{
	}

	options_audio::~options_audio()
	{
#if _DEBUG
		std::cerr << "destructor: options_audio" << std::endl;
#endif
		_clear_widgets();
	}

	void options_audio::init()
	{
#if _DEBUG
		std::cerr << "init: options_audio" << std::endl;
#endif
		// resources

		// gui
		_load_widgets();
	}

	void options_audio::pause()
	{
		_hide_widgets();
	}

	void options_audio::resume()
	{
		_show_widgets();
	}

	void options_audio::handle()
	{
		sf::Event event;
		while (_data->window.pollEvent(event))
		{
			if (event.type == sf::Event::Closed)
			{
				// only force close
				_data->window.close();
			}

			_data->gui.handleEvent(event);
		}
	}

	void options_audio::update(float dt)
	{
	}

	void options_audio::draw(float dt)
	{
		_data->window.setActive(true);

		_data->window.clear(sf::Color::Black);

		_data->gui.draw();

		_data->window.display();

		_data->window.setActive(false);
	}

	void options_audio::_load_widgets()
	{
		// load theme
		tgui::Theme::Ptr theme = tgui::Theme::create(_data->settings.paths.theme);

		// y start point, get's increased by step every widget row
		unsigned int y = 20;
		unsigned int row = 0;
		unsigned int step = 40;

		// label Audio
		tgui::Label::Ptr options_audio_lbl_options = theme->load("Label");
		options_audio_lbl_options->setText("Audio");
		options_audio_lbl_options->setTextSize(40);
		options_audio_lbl_options->setPosition(tgui::Layout2d(tgui::bindWidth(_data->gui) / 2.0 - tgui::bindWidth(options_audio_lbl_options) / 2.0, y + step * row));
		_data->gui.add(options_audio_lbl_options, "options_audio_lbl_options");
		_widgets.push_back("options_audio_lbl_options");

		row++;
		row++;
		row++;

		// label General Volume
		tgui::Label::Ptr options_audio_lbl_general_volume = theme->load("Label");
		options_audio_lbl_general_volume->setText("General Volume");
		options_audio_lbl_general_volume->setPosition(tgui::Layout2d(tgui::bindWidth(_data->gui) / 2.0 - tgui::bindWidth(options_audio_lbl_general_volume) / 2.0, y + step * row));
		_data->gui.add(options_audio_lbl_general_volume, "options_audio_lbl_general_volume");
		_widgets.push_back("options_audio_lbl_general_volume");

		row++;

		// button left General Volume
		tgui::Button::Ptr options_audio_bn_left_general_volume = tgui::Button::create();
		options_audio_bn_left_general_volume->setText("<");
		options_audio_bn_left_general_volume->setPosition(tgui::Layout2d(tgui::bindWidth(_data->gui) / 2.0 - tgui::bindWidth(options_audio_bn_left_general_volume) / 2.0 - 120, y + step * row));
		options_audio_bn_left_general_volume->connect("pressed", &options_audio::_decrease_general_volume, this);
		_data->gui.add(options_audio_bn_left_general_volume, "options_audio_bn_left_general_volume");
		_widgets.push_back("options_audio_bn_left_general_volume");

		// label amount General Volume
		tgui::Label::Ptr options_audio_lbl_amount_general_volume = theme->load("Label");
		options_audio_lbl_amount_general_volume->setText(std::to_string(_general_volume));
		options_audio_lbl_amount_general_volume->setPosition(tgui::Layout2d(tgui::bindWidth(_data->gui) / 2.0 - tgui::bindWidth(options_audio_lbl_amount_general_volume) / 2.0, y + step * row));
		_data->gui.add(options_audio_lbl_amount_general_volume, "options_audio_lbl_amount_general_volume");
		_widgets.push_back("options_audio_lbl_amount_general_volume");

		// button right General Volume
		tgui::Button::Ptr options_audio_bn_right_general_volume = tgui::Button::create();
		options_audio_bn_right_general_volume->setText(">");
		options_audio_bn_right_general_volume->setPosition(tgui::Layout2d(tgui::bindWidth(_data->gui) / 2.0 - tgui::bindWidth(options_audio_bn_right_general_volume) / 2.0 + 120, y + step * row));
		options_audio_bn_right_general_volume->connect("pressed", &options_audio::_increase_general_volume, this);
		_data->gui.add(options_audio_bn_right_general_volume, "options_audio_bn_right_general_volume");
		_widgets.push_back("options_audio_bn_right_general_volume");

		row++;
		row++;

		// label Music Volume
		tgui::Label::Ptr options_audio_lbl_music_volume = theme->load("Label");
		options_audio_lbl_music_volume->setText("Music Volume");
		options_audio_lbl_music_volume->setPosition(tgui::Layout2d(tgui::bindWidth(_data->gui) / 2.0 - tgui::bindWidth(options_audio_lbl_music_volume) / 2.0, y + step * row));
		_data->gui.add(options_audio_lbl_music_volume, "options_audio_lbl_music_volume");
		_widgets.push_back("options_audio_lbl_music_volume");

		row++;

		// button left Music Volume
		tgui::Button::Ptr options_audio_bn_left_music_volume = tgui::Button::create();
		options_audio_bn_left_music_volume->setText("<");
		options_audio_bn_left_music_volume->setPosition(tgui::Layout2d(tgui::bindWidth(_data->gui) / 2.0 - tgui::bindWidth(options_audio_bn_left_music_volume) / 2.0 - 120, y + step * row));
		options_audio_bn_left_music_volume->connect("pressed", &options_audio::_decrease_music_volume, this);
		_data->gui.add(options_audio_bn_left_music_volume, "options_audio_bn_left_music_volume");
		_widgets.push_back("options_audio_bn_left_music_volume");

		// label amount Music Volume
		tgui::Label::Ptr options_audio_lbl_amount_music_volume = theme->load("Label");
		options_audio_lbl_amount_music_volume->setText(std::to_string(_music_volume));
		options_audio_lbl_amount_music_volume->setPosition(tgui::Layout2d(tgui::bindWidth(_data->gui) / 2.0 - tgui::bindWidth(options_audio_lbl_amount_music_volume) / 2.0, y + step * row));
		_data->gui.add(options_audio_lbl_amount_music_volume, "options_audio_lbl_amount_music_volume");
		_widgets.push_back("options_audio_lbl_amount_music_volume");

		// button right Music Volume
		tgui::Button::Ptr options_audio_bn_right_music_volume = tgui::Button::create();
		options_audio_bn_right_music_volume->setText(">");
		options_audio_bn_right_music_volume->setPosition(tgui::Layout2d(tgui::bindWidth(_data->gui) / 2.0 - tgui::bindWidth(options_audio_bn_right_music_volume) / 2.0 + 120, y + step * row));
		options_audio_bn_right_music_volume->connect("pressed", &options_audio::_increase_music_volume, this);
		_data->gui.add(options_audio_bn_right_music_volume, "options_audio_bn_right_music_volume");
		_widgets.push_back("options_audio_bn_right_music_volume");

		row++;
		row++;

		// label Effects Volume
		tgui::Label::Ptr options_audio_lbl_effects_volume = theme->load("Label");
		options_audio_lbl_effects_volume->setText("Effects Volume");
		options_audio_lbl_effects_volume->setPosition(tgui::Layout2d(tgui::bindWidth(_data->gui) / 2.0 - tgui::bindWidth(options_audio_lbl_effects_volume) / 2.0, y + step * row));
		_data->gui.add(options_audio_lbl_effects_volume, "options_audio_lbl_effects_volume");
		_widgets.push_back("options_audio_lbl_effects_volume");

		row++;

		// button left Effects Volume
		tgui::Button::Ptr options_audio_bn_left_effects_volume = tgui::Button::create();
		options_audio_bn_left_effects_volume->setText("<");
		options_audio_bn_left_effects_volume->setPosition(tgui::Layout2d(tgui::bindWidth(_data->gui) / 2.0 - tgui::bindWidth(options_audio_bn_left_effects_volume) / 2.0 - 120, y + step * row));
		options_audio_bn_left_effects_volume->connect("pressed", &options_audio::_decrease_effects_volume, this);
		_data->gui.add(options_audio_bn_left_effects_volume, "options_audio_bn_left_effects_volume");
		_widgets.push_back("options_audio_bn_left_effects_volume");

		// label amount Effects Volume
		tgui::Label::Ptr options_audio_lbl_amount_effects_volume = theme->load("Label");
		options_audio_lbl_amount_effects_volume->setText(std::to_string(_effects_volume));
		options_audio_lbl_amount_effects_volume->setPosition(tgui::Layout2d(tgui::bindWidth(_data->gui) / 2.0 - tgui::bindWidth(options_audio_lbl_amount_effects_volume) / 2.0, y + step * row));
		_data->gui.add(options_audio_lbl_amount_effects_volume, "options_audio_lbl_amount_effects_volume");
		_widgets.push_back("options_audio_lbl_amount_effects_volume");

		// button right Effects Volume
		tgui::Button::Ptr options_audio_bn_right_effects_volume = tgui::Button::create();
		options_audio_bn_right_effects_volume->setText(">");
		options_audio_bn_right_effects_volume->setPosition(tgui::Layout2d(tgui::bindWidth(_data->gui) / 2.0 - tgui::bindWidth(options_audio_bn_right_effects_volume) / 2.0 + 120, y + step * row));
		options_audio_bn_right_effects_volume->connect("pressed", &options_audio::_increase_effects_volume, this);
		_data->gui.add(options_audio_bn_right_effects_volume, "options_audio_bn_right_effects_volume");
		_widgets.push_back("options_audio_bn_right_effects_volume");

		row++;
		row++;
		row++;

		// button back
		tgui::Button::Ptr options_audio_bn_back = tgui::Button::create();
		options_audio_bn_back->setText("Back");
		options_audio_bn_back->setPosition(tgui::Layout2d(tgui::bindWidth(_data->gui) / 2.0 - tgui::bindWidth(options_audio_bn_back) / 2.0, y + step * row));
		options_audio_bn_back->connect("pressed", &options_audio::_exit, this);
		_data->gui.add(options_audio_bn_back, "options_audio_bn_back");
		_widgets.push_back("options_audio_bn_back");

		row++;

		// button save
		tgui::Button::Ptr options_audio_bn_save = tgui::Button::create();
		options_audio_bn_save->setText("Save and Back");
		options_audio_bn_save->setPosition(tgui::Layout2d(tgui::bindWidth(_data->gui) / 2.0 - tgui::bindWidth(options_audio_bn_save) / 2.0, y + step * row));
		options_audio_bn_save->connect("pressed", &options_audio::_save, this);
		_data->gui.add(options_audio_bn_save, "options_audio_bn_save");
		_widgets.push_back("options_audio_bn_save");
	}

	void options_audio::_exit()
	{
		_data->states.remove();
		_clear_widgets();
	}

	void options_audio::_clear_widgets()
	{
		// delete the states widgets
		for (unsigned int i = 0; i < _widgets.size(); i++)
		{
			_data->gui.remove(_data->gui.get(_widgets.at(i)));
		}
	}

	void options_audio::_hide_widgets()
	{
		// delete the states widgets
		for (unsigned int i = 0; i < _widgets.size(); i++)
		{
			_data->gui.get(_widgets.at(i))->hide();
		}
	}

	void options_audio::_show_widgets()
	{
		// delete the states widgets
		for (unsigned int i = 0; i < _widgets.size(); i++)
		{
			_data->gui.get(_widgets.at(i))->show();
		}
	}

	void options_audio::_increase_effects_volume()
	{
		// volume increases 5
		if (_effects_volume < _MAXVOLUME)
		{
			_effects_volume += _VOLUMESTEP;
		}

		tgui::Label::Ptr options_audio_lbl_amount_effects_volume = _data->gui.get<tgui::Label>("options_audio_lbl_amount_effects_volume");
		options_audio_lbl_amount_effects_volume->setText(std::to_string(_effects_volume));
	}

	void options_audio::_decrease_effects_volume()
	{
		// volume decreases 5
		if (_effects_volume > _MINVOLUME)
		{
			_effects_volume -= _VOLUMESTEP;
		}

		tgui::Label::Ptr options_audio_lbl_amount_effects_volume = _data->gui.get<tgui::Label>("options_audio_lbl_amount_effects_volume");
		options_audio_lbl_amount_effects_volume->setText(std::to_string(_effects_volume));
	}

	void options_audio::_increase_music_volume()
	{
		// volume increases 5
		if (_music_volume < _MAXVOLUME)
		{
			_music_volume += _VOLUMESTEP;
		}

		tgui::Label::Ptr options_audio_lbl_amount_music_volume = _data->gui.get<tgui::Label>("options_audio_lbl_amount_music_volume");
		options_audio_lbl_amount_music_volume->setText(std::to_string(_music_volume));
	}

	void options_audio::_decrease_music_volume()
	{
		// volume decreases 5
		if (_music_volume > _MINVOLUME)
		{
			_music_volume -= _VOLUMESTEP;
		}

		tgui::Label::Ptr options_audio_lbl_amount_music_volume = _data->gui.get<tgui::Label>("options_audio_lbl_amount_music_volume");
		options_audio_lbl_amount_music_volume->setText(std::to_string(_music_volume));
	}

	void options_audio::_increase_general_volume()
	{
		// volume increases 5
		if (_general_volume < _MAXVOLUME)
		{
			_general_volume += _VOLUMESTEP;
		}

		tgui::Label::Ptr options_audio_lbl_amount_general_volume = _data->gui.get<tgui::Label>("options_audio_lbl_amount_general_volume");
		options_audio_lbl_amount_general_volume->setText(std::to_string(_general_volume));
	}

	void options_audio::_decrease_general_volume()
	{
		// volume decreases 5
		if (_general_volume > _MINVOLUME)
		{
			_general_volume -= _VOLUMESTEP;
		}

		tgui::Label::Ptr options_audio_lbl_amount_general_volume = _data->gui.get<tgui::Label>("options_audio_lbl_amount_general_volume");
		options_audio_lbl_amount_general_volume->setText(std::to_string(_general_volume));
	}


	void options_audio::_save()
	{
#if _DEBUG
		std::cerr << "saving: options" << std::endl;
#endif
		// set values in settings
		_data->settings.audio.general = _general_volume;
		_data->settings.audio.music = _music_volume;
		_data->settings.audio.effects = _effects_volume;

#if _USINGDB
		bool rb;
		rb = _data->config.upd_db(_data->settings.paths.config_path, _general_volume, "AUDIO", "general");
		if (!rb)
		{
			lc::utils::log(_LOGPATH, "From options_audio::_save: Error occured trying to update db with value 'AUDIO: general'!");
#if _DEBUG
			std::cerr << "Error updating DB" << std::endl;
#endif
		}
		rb = _data->config.upd_db(_data->settings.paths.config_path, _music_volume, "AUDIO", "music");
		if (!rb)
		{
			lc::utils::log(_LOGPATH, "From options_audio::_save: Error occured trying to update db with value 'AUDIO: music'!");
#if _DEBUG
			std::cerr << "Error updating DB" << std::endl;
#endif
		}
		rb = _data->config.upd_db(_data->settings.paths.config_path, _effects_volume, "AUDIO", "effects");
		if (!rb)
		{
			lc::utils::log(_LOGPATH, "From options_audio::_save: Error occured trying to update db with value 'AUDIO: effects'!");
#if _DEBUG
			std::cerr << "Error updating DB" << std::endl;
#endif
		}
#elif _USINGINI
		bool rb;
		rb = _data->config.upd_ini(_data->settings.paths.config_path, _general_volume, "AUDIO", "general");
		if (!rb)
		{
			lc::utils::log(_LOGPATH, "From options_audio::_save: Error occured trying to update ini with value 'AUDIO: general'!");
#if _DEBUG
			std::cerr << "Error updating INI" << std::endl;
#endif
		}
		rb = _data->config.upd_ini(_data->settings.paths.config_path, _effects_volume, "AUDIO", "effects");
		if (!rb)
		{
			lc::utils::log(_LOGPATH, "From options_audio::_save: Error occured trying to update ini with value 'AUDIO: effects'!");
#if _DEBUG
			std::cerr << "Error updating INI" << std::endl;
#endif
		}
		rb = _data->config.upd_ini(_data->settings.paths.config_path, _music_volume, "AUDIO", "music");
		if (!rb)
		{
			lc::utils::log(_LOGPATH, "From options_audio::_save: Error occured trying to update ini with value 'AUDIO: music'!");
#if _DEBUG
			std::cerr << "Error updating INI" << std::endl;
#endif
		}
#endif
		_exit();
	}
}
