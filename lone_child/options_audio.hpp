#ifndef __LC__OPTIONS_AUDIO__
#define __LC__OPTIONS_AUDIO__

// lc
#include "application.hpp"

// std
#include <memory>

// lib
#include <SFML/Graphics.hpp>

// NEngine
#include "../include/NEngine_1.0.0/nstate_manager/nstate.hpp"

namespace lc {

	class options_audio : public nengine::nstate_manager::nstate
	{
	public:
		options_audio(std::shared_ptr<game_data> data);
		~options_audio();
		void init();
		void pause();
		void resume();
		void handle();
		void update(float dt);
		void draw(float dt);

	private:
		std::shared_ptr<game_data> _data;
		std::vector<std::string> _widgets;

		void _load_widgets();

		void _exit();

		void _clear_widgets();
		void _hide_widgets();
		void _show_widgets();

		void _increase_general_volume();
		void _decrease_general_volume();
		unsigned int _general_volume;

		void _increase_music_volume();
		void _decrease_music_volume();
		unsigned int _music_volume;

		void _increase_effects_volume();
		void _decrease_effects_volume();
		unsigned int _effects_volume;

		void _save();
	};

}

#endif
