// lc
#include "options_graphics.hpp"
#include "definitions.hpp"

// std
#include <exception>

// lib
#include <SFML/Graphics.hpp>
#include <TGUI/TGUI.hpp>

// NEngine
#include "../include/NEngine_1.0.0/nstate_manager/nstate.hpp"

namespace lc {

	options_graphics::options_graphics(std::shared_ptr<game_data> data)
		: _data(data)
		, _style_changed(false)
		, _style_original(_data->settings.graphics.style)
		, _antialiasing(_data->settings.graphics.antialiasing)
		, _width(_data->settings.graphics.width)
		, _height(_data->settings.graphics.height)
	{
	}

	options_graphics::~options_graphics()
	{
#if _DEBUG
		std::cerr << "destructor: options_graphics" << std::endl;
#endif
		_clear_widgets();
	}

	void options_graphics::init()
	{
#if _DEBUG
		std::cerr << "init: options_graphics" << std::endl;
#endif
		// resources

		// gui
		_load_widgets();
	}

	void options_graphics::pause()
	{
		_hide_widgets();
	}

	void options_graphics::resume()
	{
		_show_widgets();
	}

	void options_graphics::handle()
	{
		sf::Event event;
		while (_data->window.pollEvent(event))
		{
			if (event.type == sf::Event::Closed)
			{
				// only force close
				_data->window.close();
			}

			_data->gui.handleEvent(event);
		}
	}

	void options_graphics::update(float dt)
	{
		try
		{
			tgui::CheckBox::Ptr options_graphics_cx_fullscreen = _data->gui.get<tgui::CheckBox>("options_graphics_cx_fullscreen");
			if (!_style_changed)
			{
				lc::style style_tmp;

				if(options_graphics_cx_fullscreen) // test against nullptr
				{
					if (options_graphics_cx_fullscreen->isChecked())
					{
						style_tmp = lc::style::Fullscreen;
					}
					else
					{
						style_tmp = lc::style::Close;
					}

					if (_style_original != style_tmp)
					{
						_style_changed = true;
						tgui::Label::Ptr options_graphics_lbl_changed = _data->gui.get<tgui::Label>("options_graphics_lbl_changed");
						options_graphics_lbl_changed->setText("To see the effect of your change please save and restart the game!");
					}
				}
			}
		}
		catch (...)
		{
		}
	}

	void options_graphics::draw(float dt)
	{
		_data->window.setActive(true);

		_data->window.clear(sf::Color::Black);

		_data->gui.draw();

		_data->window.display();

		_data->window.setActive(false);
	}

	void options_graphics::_load_widgets()
	{
		// load theme
		tgui::Theme::Ptr theme = tgui::Theme::create(_data->settings.paths.theme);

		// y start point, get's increased by step every widget row
		unsigned int y = 20;
		unsigned int row = 0;
		unsigned int step = 40;

		// label Changed
		tgui::Label::Ptr options_graphics_lbl_changed = theme->load("Label");
		options_graphics_lbl_changed->setText("");
		options_graphics_lbl_changed->setPosition(tgui::Layout2d(10, _data->window.getSize().y - options_graphics_lbl_changed->getTextSize() * 1.5));
		_data->gui.add(options_graphics_lbl_changed, "options_graphics_lbl_changed");
		_widgets.push_back("options_graphics_lbl_changed");

		// label Options
		tgui::Label::Ptr options_graphics_lbl_options = theme->load("Label");
		options_graphics_lbl_options->setText("Graphics");
		options_graphics_lbl_options->setTextSize(40);
		options_graphics_lbl_options->setPosition(tgui::Layout2d(tgui::bindWidth(_data->gui) / 2.0 - tgui::bindWidth(options_graphics_lbl_options) / 2.0, y + step * row));
		_data->gui.add(options_graphics_lbl_options, "options_graphics_lbl_options");
		_widgets.push_back("options_graphics_lbl_options");

		row++;
		row++;
		row++;

		// label Antialiasing
		tgui::Label::Ptr options_graphics_lbl_antialiasing = theme->load("Label");
		options_graphics_lbl_antialiasing->setText("Antialiasing");
		options_graphics_lbl_antialiasing->setPosition(tgui::Layout2d(tgui::bindWidth(_data->gui) / 2.0 - tgui::bindWidth(options_graphics_lbl_antialiasing) / 2.0, y + step * row));
		_data->gui.add(options_graphics_lbl_antialiasing, "options_graphics_lbl_antialiasing");
		_widgets.push_back("options_graphics_lbl_antialiasing");

		row++;

		// button left Antialiasing
		tgui::Button::Ptr options_graphics_bn_left_antialiasing = tgui::Button::create();
		options_graphics_bn_left_antialiasing->setText("<");
		options_graphics_bn_left_antialiasing->setPosition(tgui::Layout2d(tgui::bindWidth(_data->gui) / 2.0 - tgui::bindWidth(options_graphics_bn_left_antialiasing) / 2.0 - 120, y + step * row));
		options_graphics_bn_left_antialiasing->connect("pressed", &options_graphics::_decrease_antialiasing, this);
		_data->gui.add(options_graphics_bn_left_antialiasing, "options_graphics_bn_left_antialiasing");
		_widgets.push_back("options_graphics_bn_left_antialiasing");

		// label amount Antialiasing
		tgui::Label::Ptr options_graphics_lbl_amount_antialiasing = theme->load("Label");
		options_graphics_lbl_amount_antialiasing->setText(std::to_string(_antialiasing));
		options_graphics_lbl_amount_antialiasing->setPosition(tgui::Layout2d(tgui::bindWidth(_data->gui) / 2.0 - tgui::bindWidth(options_graphics_lbl_amount_antialiasing) / 2.0, y + step * row));
		_data->gui.add(options_graphics_lbl_amount_antialiasing, "options_graphics_lbl_amount_antialiasing");
		_widgets.push_back("options_graphics_lbl_amount_antialiasing");

		// button right Antialiasing
		tgui::Button::Ptr options_graphics_bn_right_antialiasing = tgui::Button::create();
		options_graphics_bn_right_antialiasing->setText(">");
		options_graphics_bn_right_antialiasing->setPosition(tgui::Layout2d(tgui::bindWidth(_data->gui) / 2.0 - tgui::bindWidth(options_graphics_bn_right_antialiasing) / 2.0 + 120, y + step * row));
		options_graphics_bn_right_antialiasing->connect("pressed", &options_graphics::_increase_antialiasing, this);
		_data->gui.add(options_graphics_bn_right_antialiasing, "options_graphics_bn_right_antialiasing");
		_widgets.push_back("options_graphics_bn_right_antialiasing");

		row++;
		row++;

		// label Resolution
		tgui::Label::Ptr options_graphics_lbl_resolution = theme->load("Label");
		options_graphics_lbl_resolution->setText("Resolution");
		options_graphics_lbl_resolution->setPosition(tgui::Layout2d(tgui::bindWidth(_data->gui) / 2.0 - tgui::bindWidth(options_graphics_lbl_resolution) / 2.0, y + step * row));
		_data->gui.add(options_graphics_lbl_resolution, "options_graphics_lbl_resolution");
		_widgets.push_back("options_graphics_lbl_resolution");

		row++;

		// button left Resolution
		tgui::Button::Ptr options_graphics_bn_left_resolution = tgui::Button::create();
		options_graphics_bn_left_resolution->setText("<");
		options_graphics_bn_left_resolution->setPosition(tgui::Layout2d(tgui::bindWidth(_data->gui) / 2.0 - tgui::bindWidth(options_graphics_bn_left_resolution) / 2.0 - 120, y + step * row));
		options_graphics_bn_left_resolution->connect("pressed", &options_graphics::_decrease_resolution, this);
		_data->gui.add(options_graphics_bn_left_resolution, "options_graphics_bn_left_resolution");
		_widgets.push_back("options_graphics_bn_left_resolution");

		// label amount Resolution
		tgui::Label::Ptr options_graphics_lbl_amount_resolution = theme->load("Label");
		options_graphics_lbl_amount_resolution->setText(std::string(std::to_string(_width) + " * " + std::to_string(_height)));
		options_graphics_lbl_amount_resolution->setPosition(tgui::Layout2d(tgui::bindWidth(_data->gui) / 2.0 - tgui::bindWidth(options_graphics_lbl_amount_resolution) / 2.0, y + step * row));
		_data->gui.add(options_graphics_lbl_amount_resolution, "options_graphics_lbl_amount_resolution");
		_widgets.push_back("options_graphics_lbl_amount_resolution");

		// button right Resolution
		tgui::Button::Ptr options_graphics_bn_right_resolution = tgui::Button::create();
		options_graphics_bn_right_resolution->setText(">");
		options_graphics_bn_right_resolution->setPosition(tgui::Layout2d(tgui::bindWidth(_data->gui) / 2.0 - tgui::bindWidth(options_graphics_bn_right_resolution) / 2.0 + 120, y + step * row));
		options_graphics_bn_right_resolution->connect("pressed", &options_graphics::_increase_resolution, this);
		_data->gui.add(options_graphics_bn_right_resolution, "options_graphics_bn_right_resolution");
		_widgets.push_back("options_graphics_bn_right_resolution");

		row++;
		row++;

		// Checkbox Vertical Sync
		tgui::CheckBox::Ptr options_graphics_cx_vertical_sync = theme->load("CheckBox");
		options_graphics_cx_vertical_sync->setText("Vertical Sync");
		options_graphics_cx_vertical_sync->setPosition(tgui::Layout2d(tgui::bindWidth(_data->gui) / 2.0 - tgui::bindWidth(options_graphics_cx_vertical_sync) / 2.0, y + step * row));
		if (_data->settings.graphics.vertical_sync)
		{
			options_graphics_cx_vertical_sync->check();
		}
		_data->gui.add(options_graphics_cx_vertical_sync, "options_graphics_cx_vertical_sync");
		_widgets.push_back("options_graphics_cx_vertical_sync");

		row++;
		row++;

		// Checkbox Fullscreen
		tgui::CheckBox::Ptr options_graphics_cx_fullscreen = theme->load("CheckBox");
		options_graphics_cx_fullscreen->setText("Fullscreen");
		options_graphics_cx_fullscreen->setPosition(tgui::Layout2d(tgui::bindWidth(_data->gui) / 2.0 - tgui::bindWidth(options_graphics_cx_fullscreen) / 2.0, y + step * row));
		if (_data->settings.graphics.style == lc::style::Fullscreen)
		{
			options_graphics_cx_fullscreen->check();
		}
		_data->gui.add(options_graphics_cx_fullscreen, "options_graphics_cx_fullscreen");
		_widgets.push_back("options_graphics_cx_fullscreen");

		row++;
		row++;
		row++;

		// button back
		tgui::Button::Ptr options_graphics_bn_back = tgui::Button::create();
		options_graphics_bn_back->setText("Back");
		options_graphics_bn_back->setPosition(tgui::Layout2d(tgui::bindWidth(_data->gui) / 2.0 - tgui::bindWidth(options_graphics_bn_back) / 2.0, y + step * row));
		options_graphics_bn_back->connect("pressed", &options_graphics::_exit, this);
		_data->gui.add(options_graphics_bn_back, "options_graphics_bn_back");
		_widgets.push_back("options_graphics_bn_back");

		row++;

		// button save
		tgui::Button::Ptr options_graphics_bn_save = tgui::Button::create();
		options_graphics_bn_save->setText("Save and Back");
		options_graphics_bn_save->setPosition(tgui::Layout2d(tgui::bindWidth(_data->gui) / 2.0 - tgui::bindWidth(options_graphics_bn_save) / 2.0, y + step * row));
		options_graphics_bn_save->connect("pressed", &options_graphics::_save, this);
		_data->gui.add(options_graphics_bn_save, "options_graphics_bn_save");
		_widgets.push_back("options_graphics_bn_save");
	}

	void options_graphics::_exit()
	{
		_data->states.remove();
		_clear_widgets();
	}

	void options_graphics::_clear_widgets()
	{
		// delete the states widgets
		for (unsigned int i = 0; i < _widgets.size(); i++)
		{
			_data->gui.remove(_data->gui.get(_widgets.at(i)));
		}
	}

	void options_graphics::_hide_widgets()
	{
		// delete the states widgets
		for (unsigned int i = 0; i < _widgets.size(); i++)
		{
			_data->gui.get(_widgets.at(i))->hide();
		}
	}

	void options_graphics::_show_widgets()
	{
		// delete the states widgets
		for (unsigned int i = 0; i < _widgets.size(); i++)
		{
			_data->gui.get(_widgets.at(i))->show();
		}
	}

	void options_graphics::_increase_resolution()
	{
		// width increases 16
		if (_width < _MAXWIDTH)
		{
			_width += _WIDTHSTEP;
		}

		// height increases 9
		if (_height < _MAXHEIGHT)
		{
			_height += _HEIGHTSTEP;
		}

		tgui::Label::Ptr options_graphics_lbl_amount_resolution = _data->gui.get<tgui::Label>("options_graphics_lbl_amount_resolution");
		options_graphics_lbl_amount_resolution->setText(std::string(std::to_string(_width) + " * " + std::to_string(_height)));
		tgui::Label::Ptr options_graphics_lbl_changed = _data->gui.get<tgui::Label>("options_graphics_lbl_changed");
		options_graphics_lbl_changed->setText("To see the effect of your change please save and restart the game!");
	}

	void options_graphics::_decrease_resolution()
	{
		// width decreases 16
		if (_width > _MINWIDTH)
		{
			_width -= _WIDTHSTEP;
		}

		// height decreases 9
		if (_height > _MINHEIGHT)
		{
			_height -= _HEIGHTSTEP;
		}

		tgui::Label::Ptr options_graphics_lbl_amount_resolution = _data->gui.get<tgui::Label>("options_graphics_lbl_amount_resolution");
		options_graphics_lbl_amount_resolution->setText(std::string(std::to_string(_width) + " * " + std::to_string(_height)));
		tgui::Label::Ptr options_graphics_lbl_changed = _data->gui.get<tgui::Label>("options_graphics_lbl_changed");
		options_graphics_lbl_changed->setText("To see the effect of your change please save and restart the game!");
	}

	void options_graphics::_increase_antialiasing()
	{
		if (_antialiasing < _MAXANTIALIASING)
		{
			if (_antialiasing == _MINANTIALIASING)
			{
				_antialiasing = _ANTIALIASINGSTEP;
			}
			else
			{
				_antialiasing *= _ANTIALIASINGSTEP;
			}

			tgui::Label::Ptr options_graphics_lbl_amount_antialiasing = _data->gui.get<tgui::Label>("options_graphics_lbl_amount_antialiasing");
			options_graphics_lbl_amount_antialiasing->setText(std::to_string(_antialiasing));
			tgui::Label::Ptr options_graphics_lbl_changed = _data->gui.get<tgui::Label>("options_graphics_lbl_changed");
			options_graphics_lbl_changed->setText("To see the effect of your change please save and restart the game!");
		}
	}

	void options_graphics::_decrease_antialiasing()
	{
		if (_antialiasing > _MINANTIALIASING)
		{
			if (_antialiasing == _ANTIALIASINGSTEP)
			{
				_antialiasing = _MINANTIALIASING;
			}
			else
			{
				_antialiasing /= _ANTIALIASINGSTEP;
			}
			tgui::Label::Ptr options_graphics_lbl_amount_antialiasing = _data->gui.get<tgui::Label>("options_graphics_lbl_amount_antialiasing");
			options_graphics_lbl_amount_antialiasing->setText(std::to_string(_antialiasing));
			tgui::Label::Ptr options_graphics_lbl_changed = _data->gui.get<tgui::Label>("options_graphics_lbl_changed");
			options_graphics_lbl_changed->setText("To see the effect of your change please save and restart the game!");
		}
	}

	void options_graphics::_save()
	{
#if _DEBUG
		std::cerr << "saving: options" << std::endl;
#endif
		// get value of checkbox vertical_sync
		int tmp = 0;
		tgui::CheckBox::Ptr options_graphics_cx_vertical_sync = _data->gui.get<tgui::CheckBox>("options_graphics_cx_vertical_sync");
		if (options_graphics_cx_vertical_sync->isChecked())
		{
			tmp = 1;
		}
		else
		{
			tmp = 0;
		}
		_data->window.setVerticalSyncEnabled(options_graphics_cx_vertical_sync->isChecked());
		_data->settings.graphics.vertical_sync = options_graphics_cx_vertical_sync->isChecked();

		// get value of checkbox fullscreen
		std::string style_string;
		tgui::CheckBox::Ptr options_graphics_cx_fullscreen = _data->gui.get<tgui::CheckBox>("options_graphics_cx_fullscreen");
		if (options_graphics_cx_fullscreen->isChecked())
		{
			style_string = "Fullscreen";
			_data->settings.graphics.style = lc::style::Fullscreen;
		}
		else
		{ // Close is default
			style_string = "Close";
			_data->settings.graphics.style = lc::style::Close;
		}

#if _USINGDB
		bool rb;
		rb = _data->config.upd_db(_data->settings.paths.config_path, _antialiasing, "GRAPHICS", "antialiasing");
		if (!rb)
		{
			lc::utils::log(_LOGPATH, "From options_graphics::_save: Error occured trying to update db with value 'GRAPHICS: antialiasing'!");
#if _DEBUG
			std::cerr << "Error updating DB" << std::endl;
#endif
		}
		rb = _data->config.upd_db(_data->settings.paths.config_path, _width, "GRAPHICS", "width");
		if (!rb)
		{
			lc::utils::log(_LOGPATH, "From options_graphics::_save: Error occured trying to update db with value 'GRAPHICS: width'!");
#if _DEBUG
			std::cerr << "Error updating DB" << std::endl;
#endif
		}
		rb = _data->config.upd_db(_data->settings.paths.config_path, _height, "GRAPHICS", "height");
		if (!rb)
		{
			lc::utils::log(_LOGPATH, "From options_graphics::_save: Error occured trying to update db with value 'GRAPHICS: height'!");
#if _DEBUG
			std::cerr << "Error updating DB" << std::endl;
#endif
		}
		rb = _data->config.upd_db(_data->settings.paths.config_path, tmp, "GRAPHICS", "vertical_sync");
		if (!rb)
		{
			lc::utils::log(_LOGPATH, "From options_graphics::_save: Error occured trying to update db with value 'GRAPHICS: vertical_sync'!");
#if _DEBUG
			std::cerr << "Error updating DB" << std::endl;
#endif
		}
		rb = _data->config.upd_db(_data->settings.paths.config_path, style_string, "GRAPHICS", "style");
		if (!rb)
		{
			lc::utils::log(_LOGPATH, "From options_graphics::_save: Error occured trying to update db with value 'GRAPHICS: style'!");
#if _DEBUG
			std::cerr << "Error updating INI" << std::endl;
#endif
		}
#elif _USINGINI
		bool rb;
		rb = _data->config.upd_ini(_data->settings.paths.config_path, _antialiasing, "GRAPHICS", "antialiasing");
		if (!rb)
		{
			lc::utils::log(_LOGPATH, "From options_graphics::_save: Error occured trying to update ini with value 'GRAPHICS: antialiasing'!");
#if _DEBUG
			std::cerr << "Error updating INI" << std::endl;
#endif
	}
		rb = _data->config.upd_ini(_data->settings.paths.config_path, _width, "GRAPHICS", "width");
		if (!rb)
		{
			lc::utils::log(_LOGPATH, "From options_graphics::_save: Error occured trying to update ini with value 'GRAPHICS: width'!");
#if _DEBUG
			std::cerr << "Error updating INI" << std::endl;
#endif
		}
		rb = _data->config.upd_ini(_data->settings.paths.config_path, _height, "GRAPHICS", "height");
		if (!rb)
		{
			lc::utils::log(_LOGPATH, "From options_graphics::_save: Error occured trying to update ini with value 'GRAPHICS: height'!");
#if _DEBUG
			std::cerr << "Error updating INI" << std::endl;
#endif
		}
		rb = _data->config.upd_ini(_data->settings.paths.config_path, tmp, "GRAPHICS", "vertical_sync");
		if (!rb)
		{
			lc::utils::log(_LOGPATH, "From options_graphics::_save: Error occured trying to update ini with value 'GRAPHICS: vertical_sync'!");
#if _DEBUG
			std::cerr << "Error updating INI" << std::endl;
#endif
		}
		rb = _data->config.upd_ini(_data->settings.paths.config_path, style_string, "GRAPHICS", "style");
		if (!rb)
		{
			lc::utils::log(_LOGPATH, "From options_graphics::_save: Error occured trying to update ini with value 'GRAPHICS: style'!");
#if _DEBUG
			std::cerr << "Error updating INI" << std::endl;
#endif
		}
#endif
		_exit();
	}
}
