#ifndef __LC__OPTIONS_GRAPHICS__
#define __LC__OPTIONS_GRAPHICS__

// lc
#include "application.hpp"
#include "configurator.hpp"

// std
#include <memory>

// lib
#include <SFML/Graphics.hpp>

// NEngine
#include "../include/NEngine_1.0.0/nstate_manager/nstate.hpp"

namespace lc {

	class options_graphics : public nengine::nstate_manager::nstate
	{
	public:
		options_graphics(std::shared_ptr<game_data> data);
		~options_graphics();
		void init();
		void pause();
		void resume();
		void handle();
		void update(float dt);
		void draw(float dt);

	private:
		std::shared_ptr<game_data> _data;
		std::vector<std::string> _widgets;
		bool _style_changed;
		lc::style _style_original;

		void _load_widgets();

		void _exit();

		void _clear_widgets();
		void _hide_widgets();
		void _show_widgets();

		void _increase_resolution();
		void _decrease_resolution();
		unsigned int _width;
		unsigned int _height;

		void _increase_antialiasing();
		void _decrease_antialiasing();
		unsigned int _antialiasing;

		void _save();
	};

}

#endif
