// lc
#include "options_keybindings.hpp"
#include "definitions.hpp"

// std
#include <exception>

// lib
#include <SFML/Graphics.hpp>
#include <TGUI/TGUI.hpp>

// NEngine
#include "../include/NEngine_1.0.0/nstate_manager/nstate.hpp"

namespace lc {

	options_keybindings::options_keybindings(std::shared_ptr<game_data> data)
		: _data(data)
		, _widgets()
		, _move_at_scroll()
		, _visible_area()
		, _key_taken_clock()
		, _scrolled_up(false)
		, _scrolled_down(false)
	{
	}

	options_keybindings::~options_keybindings()
	{
#if _DEBUG
		std::cerr << "destructor: options_graphics" << std::endl;
#endif
		_clear_widgets();
	}

	void options_keybindings::init()
	{
#if _DEBUG
		std::cerr << "init: options_graphics" << std::endl;
#endif
		// resources

		// gui
		_load_widgets();
	}

	void options_keybindings::pause()
	{
		_hide_widgets();
	}

	void options_keybindings::resume()
	{
		_show_widgets();
	}

	void options_keybindings::handle()
	{
		_scrolled_up = false; // has to happen before to prevent infinite scrolling
		_scrolled_down = false; // they would not get resetted if inside event loop and no event occurred

		sf::Event event;
		while (_data->window.pollEvent(event))
		{
			if (event.type == sf::Event::Closed)
			{
				// only force close
				_data->window.close();
			}

			if (event.type == sf::Event::MouseWheelScrolled)
			{
				if (event.mouseWheelScroll.delta > 0)
				{ // scroll up --> content has to move down
					_scrolled_up = true;
				}

				if (event.mouseWheelScroll.delta < 0)
				{ // scroll down --> content has to move up
					_scrolled_down = true;
				}
			}

			_data->gui.handleEvent(event);
		}
	}

	void options_keybindings::update(float dt)
	{
		if (_scrolled_up)
		{ // scroll up --> content has to move down
			_move_widgets(300 * dt);
		}

		if (_scrolled_down)
		{ // scroll down --> content has to move up
			_move_widgets(300 * dt * (-1));
		}

		if (_key_taken_clock.getElapsedTime().asSeconds() >= _KEYTAKENTIME)
		{
			try
			{
				if (_data->gui.get<tgui::Label>("otions_keybindings_lbl_key_taken"))
				{
					if (_data->gui.get<tgui::Label>("otions_keybindings_lbl_key_taken")->getText().toAnsiString() != "")
					{
						_data->gui.get<tgui::Label>("otions_keybindings_lbl_key_taken")->setText(" ");
					}
				}
			}
			catch (...)
			{
				lc::utils::log(_LOGPATH, "From options_keybindings::_move_widgets: Error occured trying to access 'otions_keybindings_lbl_key_taken'!");
			}
		}
	}

	void options_keybindings::_move_widgets(float y_amount)
	{
		for (unsigned int i = 0; i < _move_at_scroll.size(); i++)
		{
			if(y_amount != 0.f)
			{
				try
				{ // move has a memory leak --> so use setPosition !!!
					_data->gui.get(_move_at_scroll.at(i).first)->setPosition(_data->gui.get(_move_at_scroll.at(i).first)->getPosition().x, _data->gui.get(_move_at_scroll.at(i).first)->getPosition().y + y_amount);
					_data->gui.get(_move_at_scroll.at(i).second)->setPosition(_data->gui.get(_move_at_scroll.at(i).second)->getPosition().x, _data->gui.get(_move_at_scroll.at(i).second)->getPosition().y + y_amount);
				}
				catch (...)
				{
					lc::utils::log(_LOGPATH, "From options_keybindings::_move_widgets: Error occured trying to access '_data->gui.get(_move_at_scroll.at(i).first)'!");
					lc::utils::log(_LOGPATH, "From options_keybindings::_move_widgets: Error occured trying to access '_data->gui.get(_move_at_scroll.at(i).second)'!");
				}
			}

			// test for being inside visible area rectangle
			if (((_data->gui.get(_move_at_scroll.at(i).first)->getAbsolutePosition().y >= _visible_area.getPosition().y) && // under top
				(_data->gui.get(_move_at_scroll.at(i).first)->getAbsolutePosition().y + _data->gui.get(_move_at_scroll.at(i).first)->getFullSize().y <= _visible_area.getPosition().y + _visible_area.getSize().y)) || // over bottom
				((_data->gui.get(_move_at_scroll.at(i).second)->getAbsolutePosition().y >= _visible_area.getPosition().y) && // under top
				(_data->gui.get(_move_at_scroll.at(i).second)->getAbsolutePosition().y + _data->gui.get(_move_at_scroll.at(i).second)->getFullSize().y <= _visible_area.getPosition().y + _visible_area.getSize().y))) // over bottom
			{
				if ((!_data->gui.get(_move_at_scroll.at(i).first)->isVisible()) && 
					(!_data->gui.get(_move_at_scroll.at(i).second)->isVisible()))
				{
					try
					{
						// label and button
						_data->gui.get(_move_at_scroll.at(i).first)->show();
						_data->gui.get(_move_at_scroll.at(i).second)->show();
					}
					catch (...)
					{
						lc::utils::log(_LOGPATH, "From options_keybindings::_move_widgets: Error occured trying to access '_data->gui.get(_move_at_scroll.at(i).first)->show()'!");
						lc::utils::log(_LOGPATH, "From options_keybindings::_move_widgets: Error occured trying to access '_data->gui.get(_move_at_scroll.at(i).second)->show()'!");
					}
				}
			}
			else
			{
				if ((_data->gui.get(_move_at_scroll.at(i).first)->isVisible()) &&
					(_data->gui.get(_move_at_scroll.at(i).second)->isVisible()))
				{
					try
					{
						// label and button
						_data->gui.get(_move_at_scroll.at(i).first)->hide();
						_data->gui.get(_move_at_scroll.at(i).second)->hide();
					}
					catch (...)
					{
						lc::utils::log(_LOGPATH, "From options_keybindings::_move_widgets: Error occured trying to access '_data->gui.get(_move_at_scroll.at(i).first)->hide()'!");
						lc::utils::log(_LOGPATH, "From options_keybindings::_move_widgets: Error occured trying to access '_data->gui.get(_move_at_scroll.at(i).second)->hide()'!");
					}
				}
			}
		}
	}

	void options_keybindings::draw(float dt)
	{
		_data->window.setActive(true);

		_data->window.clear(sf::Color::Black);

		_data->gui.draw();

		_data->window.display();

		_data->window.setActive(false);
	}

	void options_keybindings::_load_widgets()
	{
		// load theme
		tgui::Theme::Ptr theme = tgui::Theme::create(_data->settings.paths.theme);

		// y start point, get's increased by step every widget row
		unsigned int y = 20;
		unsigned int row = 0;
		unsigned int step = 40;

		// label input
		tgui::Label::Ptr otions_keybindings_lbl_input = theme->load("Label");
		otions_keybindings_lbl_input->setText("");
		otions_keybindings_lbl_input->setPosition(tgui::Layout2d(10, _data->window.getSize().y - otions_keybindings_lbl_input->getTextSize() * 1.5));
		_data->gui.add(otions_keybindings_lbl_input, "otions_keybindings_lbl_input");
		_widgets.push_back("otions_keybindings_lbl_input");

		// label key_taken
		tgui::Label::Ptr otions_keybindings_lbl_key_taken = theme->load("Label");
		otions_keybindings_lbl_key_taken->setText("");
		otions_keybindings_lbl_key_taken->setPosition(tgui::Layout2d(10, _data->gui.get("otions_keybindings_lbl_input")->getAbsolutePosition().y - otions_keybindings_lbl_key_taken->getTextSize() * 1.5));
		_data->gui.add(otions_keybindings_lbl_key_taken, "otions_keybindings_lbl_key_taken");
		_widgets.push_back("otions_keybindings_lbl_key_taken");

		// label Options
		tgui::Label::Ptr options_keybindings_lbl_options = theme->load("Label");
		options_keybindings_lbl_options->setText("Keybindings");
		options_keybindings_lbl_options->setTextSize(40);
		options_keybindings_lbl_options->setPosition(tgui::Layout2d(tgui::bindWidth(_data->gui) / 2.0 - tgui::bindWidth(options_keybindings_lbl_options) / 2.0, y + step * row));
		_data->gui.add(options_keybindings_lbl_options, "options_keybindings_lbl_options");
		_widgets.push_back("options_keybindings_lbl_options");

		// button back
		tgui::Button::Ptr options_keybindings_bn_back = tgui::Button::create();
		options_keybindings_bn_back->setText("Back");
		options_keybindings_bn_back->setPosition(tgui::Layout2d(tgui::bindWidth(_data->gui) / 2.0 - tgui::bindWidth(options_keybindings_bn_back) / 2.0, tgui::bindHeight(_data->gui) - tgui::bindHeight(options_keybindings_bn_back) * 1.5));
		options_keybindings_bn_back->connect("pressed", &options_keybindings::_exit, this);
		_data->gui.add(options_keybindings_bn_back, "options_keybindings_bn_back");
		_widgets.push_back("options_keybindings_bn_back");

		_visible_area.setPosition(sf::Vector2f(0, y + step * row + tgui::bindHeight(options_keybindings_lbl_options).getValue() + y));
		_visible_area.setSize(sf::Vector2f(_data->window.getSize().x, _data->window.getSize().y - y + step * row - tgui::bindHeight(options_keybindings_lbl_options).getValue() - tgui::bindHeight(options_keybindings_bn_back).getValue() * 1.5 - 2 * y));

		row++;
		row++;
		row++;

		// label up
		tgui::Label::Ptr options_keybindings_lbl_up = theme->load("Label");
		options_keybindings_lbl_up->setText("Up");
		options_keybindings_lbl_up->setPosition(tgui::Layout2d(tgui::bindWidth(_data->gui) / 2.0 - tgui::bindWidth(options_keybindings_lbl_up) - 120, y + step * row));
		_data->gui.add(options_keybindings_lbl_up, "options_keybindings_lbl_up");
		_widgets.push_back("options_keybindings_lbl_up");

		// button up
		tgui::Button::Ptr options_keybindings_bn_up = tgui::Button::create();
		options_keybindings_bn_up->setText(_data->config.convert_key_to_str((_data->settings.keyboard.check_existance("up")) ? _data->settings.keyboard.keybindings.at("up") : sf::Keyboard::Unknown));
		options_keybindings_bn_up->setPosition(tgui::Layout2d(tgui::bindWidth(_data->gui) / 2.0, y + step * row));
		options_keybindings_bn_up->connect("pressed", &options_keybindings::_get_key, this, "up", "options_keybindings_bn_up");
		_data->gui.add(options_keybindings_bn_up, "options_keybindings_bn_up");
		_widgets.push_back("options_keybindings_bn_up");
		_move_at_scroll.push_back(std::make_pair("options_keybindings_lbl_up", "options_keybindings_bn_up"));

		row++;

		// label down
		tgui::Label::Ptr options_keybindings_lbl_down = theme->load("Label");
		options_keybindings_lbl_down->setText("Down");
		options_keybindings_lbl_down->setPosition(tgui::Layout2d(tgui::bindWidth(_data->gui) / 2.0 - tgui::bindWidth(options_keybindings_lbl_down) - 120, y + step * row));
		_data->gui.add(options_keybindings_lbl_down, "options_keybindings_lbl_down");
		_widgets.push_back("options_keybindings_lbl_down");

		// button down
		tgui::Button::Ptr options_keybindings_bn_down = tgui::Button::create();
		options_keybindings_bn_down->setText(_data->config.convert_key_to_str((_data->settings.keyboard.check_existance("down")) ? _data->settings.keyboard.keybindings.at("down") : sf::Keyboard::Unknown));
		options_keybindings_bn_down->setPosition(tgui::Layout2d(tgui::bindWidth(_data->gui) / 2.0, y + step * row));
		options_keybindings_bn_down->connect("pressed", &options_keybindings::_get_key, this, "down", "options_keybindings_bn_down");
		_data->gui.add(options_keybindings_bn_down, "options_keybindings_bn_down");
		_widgets.push_back("options_keybindings_bn_down");
		_move_at_scroll.push_back(std::make_pair("options_keybindings_lbl_down", "options_keybindings_bn_down"));

		row++;

		// label Left
		tgui::Label::Ptr options_keybindings_lbl_left = theme->load("Label");
		options_keybindings_lbl_left->setText("Left");
		options_keybindings_lbl_left->setPosition(tgui::Layout2d(tgui::bindWidth(_data->gui) / 2.0 - tgui::bindWidth(options_keybindings_lbl_left) - 120, y + step * row));
		_data->gui.add(options_keybindings_lbl_left, "options_keybindings_lbl_left");
		_widgets.push_back("options_keybindings_lbl_left");

		// button left
		tgui::Button::Ptr options_keybindings_bn_left = tgui::Button::create();
		options_keybindings_bn_left->setText(_data->config.convert_key_to_str((_data->settings.keyboard.check_existance("left")) ? _data->settings.keyboard.keybindings.at("left") : sf::Keyboard::Unknown));
		options_keybindings_bn_left->setPosition(tgui::Layout2d(tgui::bindWidth(_data->gui) / 2.0, y + step * row));
		options_keybindings_bn_left->connect("pressed", &options_keybindings::_get_key, this, "left", "options_keybindings_bn_left");
		_data->gui.add(options_keybindings_bn_left, "options_keybindings_bn_left");
		_widgets.push_back("options_keybindings_bn_left");
		_move_at_scroll.push_back(std::make_pair("options_keybindings_lbl_left", "options_keybindings_bn_left"));

		row++;

		// label right
		tgui::Label::Ptr options_keybindings_lbl_right = theme->load("Label");
		options_keybindings_lbl_right->setText("Right");
		options_keybindings_lbl_right->setPosition(tgui::Layout2d(tgui::bindWidth(_data->gui) / 2.0 - tgui::bindWidth(options_keybindings_lbl_right) - 120, y + step * row));
		_data->gui.add(options_keybindings_lbl_right, "options_keybindings_lbl_right");
		_widgets.push_back("options_keybindings_lbl_right");

		// button right
		tgui::Button::Ptr options_keybindings_bn_right = tgui::Button::create();
		options_keybindings_bn_right->setText(_data->config.convert_key_to_str((_data->settings.keyboard.check_existance("right")) ? _data->settings.keyboard.keybindings.at("right") : sf::Keyboard::Unknown));
		options_keybindings_bn_right->setPosition(tgui::Layout2d(tgui::bindWidth(_data->gui) / 2.0, y + step * row));
		options_keybindings_bn_right->connect("pressed", &options_keybindings::_get_key, this, "right", "options_keybindings_bn_right");
		_data->gui.add(options_keybindings_bn_right, "options_keybindings_bn_right");
		_widgets.push_back("options_keybindings_bn_right");
		_move_at_scroll.push_back(std::make_pair("options_keybindings_lbl_right", "options_keybindings_bn_right"));

		row++;

		// label jump
		tgui::Label::Ptr options_keybindings_lbl_jump = theme->load("Label");
		options_keybindings_lbl_jump->setText("Jump");
		options_keybindings_lbl_jump->setPosition(tgui::Layout2d(tgui::bindWidth(_data->gui) / 2.0 - tgui::bindWidth(options_keybindings_lbl_jump) - 120, y + step * row));
		_data->gui.add(options_keybindings_lbl_jump, "options_keybindings_lbl_jump");
		_widgets.push_back("options_keybindings_lbl_jump");

		// button jump
		tgui::Button::Ptr options_keybindings_bn_jump = tgui::Button::create();
		options_keybindings_bn_jump->setText(_data->config.convert_key_to_str((_data->settings.keyboard.check_existance("jump")) ? _data->settings.keyboard.keybindings.at("jump") : sf::Keyboard::Unknown));
		options_keybindings_bn_jump->setPosition(tgui::Layout2d(tgui::bindWidth(_data->gui) / 2.0, y + step * row));
		options_keybindings_bn_jump->connect("pressed", &options_keybindings::_get_key, this, "jump", "options_keybindings_bn_jump");
		_data->gui.add(options_keybindings_bn_jump, "options_keybindings_bn_jump");
		_widgets.push_back("options_keybindings_bn_jump");
		_move_at_scroll.push_back(std::make_pair("options_keybindings_lbl_jump", "options_keybindings_bn_jump"));

		row++;

		// label interact
		tgui::Label::Ptr options_keybindings_lbl_interact = theme->load("Label");
		options_keybindings_lbl_interact->setText("Interact");
		options_keybindings_lbl_interact->setPosition(tgui::Layout2d(tgui::bindWidth(_data->gui) / 2.0 - tgui::bindWidth(options_keybindings_lbl_interact) - 120, y + step * row));
		_data->gui.add(options_keybindings_lbl_interact, "options_keybindings_lbl_interact");
		_widgets.push_back("options_keybindings_lbl_interact");

		// button interact
		tgui::Button::Ptr options_keybindings_bn_interact = tgui::Button::create();
		options_keybindings_bn_interact->setText(_data->config.convert_key_to_str((_data->settings.keyboard.check_existance("interact")) ? _data->settings.keyboard.keybindings.at("interact") : sf::Keyboard::Unknown));
		options_keybindings_bn_interact->setPosition(tgui::Layout2d(tgui::bindWidth(_data->gui) / 2.0, y + step * row));
		options_keybindings_bn_interact->connect("pressed", &options_keybindings::_get_key, this, "interact", "options_keybindings_bn_interact");
		_data->gui.add(options_keybindings_bn_interact, "options_keybindings_bn_interact");
		_widgets.push_back("options_keybindings_bn_interact");
		_move_at_scroll.push_back(std::make_pair("options_keybindings_lbl_interact", "options_keybindings_bn_interact"));

		row++;

		// label character
		tgui::Label::Ptr options_keybindings_lbl_character = theme->load("Label");
		options_keybindings_lbl_character->setText("Character");
		options_keybindings_lbl_character->setPosition(tgui::Layout2d(tgui::bindWidth(_data->gui) / 2.0 - tgui::bindWidth(options_keybindings_lbl_character) - 120, y + step * row));
		_data->gui.add(options_keybindings_lbl_character, "options_keybindings_lbl_character");
		_widgets.push_back("options_keybindings_lbl_character");

		// button character
		tgui::Button::Ptr options_keybindings_bn_character = tgui::Button::create();
		options_keybindings_bn_character->setText(_data->config.convert_key_to_str((_data->settings.keyboard.check_existance("character")) ? _data->settings.keyboard.keybindings.at("character") : sf::Keyboard::Unknown));
		options_keybindings_bn_character->setPosition(tgui::Layout2d(tgui::bindWidth(_data->gui) / 2.0, y + step * row));
		options_keybindings_bn_character->connect("pressed", &options_keybindings::_get_key, this, "character", "options_keybindings_bn_character");
		_data->gui.add(options_keybindings_bn_character, "options_keybindings_bn_character");
		_widgets.push_back("options_keybindings_bn_character");
		_move_at_scroll.push_back(std::make_pair("options_keybindings_lbl_character", "options_keybindings_bn_character"));

		row++;

		// label backpack
		tgui::Label::Ptr options_keybindings_lbl_backpack = theme->load("Label");
		options_keybindings_lbl_backpack->setText("Backpack");
		options_keybindings_lbl_backpack->setPosition(tgui::Layout2d(tgui::bindWidth(_data->gui) / 2.0 - tgui::bindWidth(options_keybindings_lbl_backpack) - 120, y + step * row));
		_data->gui.add(options_keybindings_lbl_backpack, "options_keybindings_lbl_backpack");
		_widgets.push_back("options_keybindings_lbl_backpack");

		// button backpack
		tgui::Button::Ptr options_keybindings_bn_backpack = tgui::Button::create();
		options_keybindings_bn_backpack->setText(_data->config.convert_key_to_str((_data->settings.keyboard.check_existance("backpack")) ? _data->settings.keyboard.keybindings.at("backpack") : sf::Keyboard::Unknown));
		options_keybindings_bn_backpack->setPosition(tgui::Layout2d(tgui::bindWidth(_data->gui) / 2.0, y + step * row));
		options_keybindings_bn_backpack->connect("pressed", &options_keybindings::_get_key, this, "backpack", "options_keybindings_bn_backpack");
		_data->gui.add(options_keybindings_bn_backpack, "options_keybindings_bn_backpack");
		_widgets.push_back("options_keybindings_bn_backpack");
		_move_at_scroll.push_back(std::make_pair("options_keybindings_lbl_backpack", "options_keybindings_bn_backpack"));

		row++;

		// label pause
		tgui::Label::Ptr options_keybindings_lbl_pause = theme->load("Label");
		options_keybindings_lbl_pause->setText("Pause");
		options_keybindings_lbl_pause->setPosition(tgui::Layout2d(tgui::bindWidth(_data->gui) / 2.0 - tgui::bindWidth(options_keybindings_lbl_pause) - 120, y + step * row));
		_data->gui.add(options_keybindings_lbl_pause, "options_keybindings_lbl_pause");
		_widgets.push_back("options_keybindings_lbl_pause");

		// button pause
		tgui::Button::Ptr options_keybindings_bn_pause = tgui::Button::create();
		options_keybindings_bn_pause->setText(_data->config.convert_key_to_str((_data->settings.keyboard.check_existance("pause")) ? _data->settings.keyboard.keybindings.at("pause") : sf::Keyboard::Unknown));
		options_keybindings_bn_pause->setPosition(tgui::Layout2d(tgui::bindWidth(_data->gui) / 2.0, y + step * row));
		options_keybindings_bn_pause->connect("pressed", &options_keybindings::_get_key, this, "pause", "options_keybindings_bn_pause");
		_data->gui.add(options_keybindings_bn_pause, "options_keybindings_bn_pause");
		_widgets.push_back("options_keybindings_bn_pause");
		_move_at_scroll.push_back(std::make_pair("options_keybindings_lbl_pause", "options_keybindings_bn_pause"));

		row++;

		// label skill_1
		tgui::Label::Ptr options_keybindings_lbl_skill_1 = theme->load("Label");
		options_keybindings_lbl_skill_1->setText("Skill 1");
		options_keybindings_lbl_skill_1->setPosition(tgui::Layout2d(tgui::bindWidth(_data->gui) / 2.0 - tgui::bindWidth(options_keybindings_lbl_skill_1) - 120, y + step * row));
		_data->gui.add(options_keybindings_lbl_skill_1, "options_keybindings_lbl_skill_1");
		_widgets.push_back("options_keybindings_lbl_skill_1");

		// button skill_1
		tgui::Button::Ptr options_keybindings_bn_skill_1 = tgui::Button::create();
		options_keybindings_bn_skill_1->setText(_data->config.convert_key_to_str((_data->settings.keyboard.check_existance("skill_1")) ? _data->settings.keyboard.keybindings.at("skill_1") : sf::Keyboard::Unknown));
		options_keybindings_bn_skill_1->setPosition(tgui::Layout2d(tgui::bindWidth(_data->gui) / 2.0, y + step * row));
		options_keybindings_bn_skill_1->connect("pressed", &options_keybindings::_get_key, this, "skill_1", "options_keybindings_bn_skill_1");
		_data->gui.add(options_keybindings_bn_skill_1, "options_keybindings_bn_skill_1");
		_widgets.push_back("options_keybindings_bn_skill_1");
		_move_at_scroll.push_back(std::make_pair("options_keybindings_lbl_skill_1", "options_keybindings_bn_skill_1"));

		row++;

		// label skill_2
		tgui::Label::Ptr options_keybindings_lbl_skill_2 = theme->load("Label");
		options_keybindings_lbl_skill_2->setText("Skill 2");
		options_keybindings_lbl_skill_2->setPosition(tgui::Layout2d(tgui::bindWidth(_data->gui) / 2.0 - tgui::bindWidth(options_keybindings_lbl_skill_2) - 120, y + step * row));
		_data->gui.add(options_keybindings_lbl_skill_2, "options_keybindings_lbl_skill_2");
		_widgets.push_back("options_keybindings_lbl_skill_2");

		// button skill_2
		tgui::Button::Ptr options_keybindings_bn_skill_2 = tgui::Button::create();
		options_keybindings_bn_skill_2->setText(_data->config.convert_key_to_str((_data->settings.keyboard.check_existance("skill_2")) ? _data->settings.keyboard.keybindings.at("skill_2") : sf::Keyboard::Unknown));
		options_keybindings_bn_skill_2->setPosition(tgui::Layout2d(tgui::bindWidth(_data->gui) / 2.0, y + step * row));
		options_keybindings_bn_skill_2->connect("pressed", &options_keybindings::_get_key, this, "skill_2", "options_keybindings_bn_skill_2");
		_data->gui.add(options_keybindings_bn_skill_2, "options_keybindings_bn_skill_2");
		_widgets.push_back("options_keybindings_bn_skill_2");
		_move_at_scroll.push_back(std::make_pair("options_keybindings_lbl_skill_2", "options_keybindings_bn_skill_2"));

		row++;

		// label skill_3
		tgui::Label::Ptr options_keybindings_lbl_skill_3 = theme->load("Label");
		options_keybindings_lbl_skill_3->setText("Skill 3");
		options_keybindings_lbl_skill_3->setPosition(tgui::Layout2d(tgui::bindWidth(_data->gui) / 2.0 - tgui::bindWidth(options_keybindings_lbl_skill_3) - 120, y + step * row));
		_data->gui.add(options_keybindings_lbl_skill_3, "options_keybindings_lbl_skill_3");
		_widgets.push_back("options_keybindings_lbl_skill_3");

		// button skill_3
		tgui::Button::Ptr options_keybindings_bn_skill_3 = tgui::Button::create();
		options_keybindings_bn_skill_3->setText(_data->config.convert_key_to_str((_data->settings.keyboard.check_existance("skill_3")) ? _data->settings.keyboard.keybindings.at("skill_3") : sf::Keyboard::Unknown));
		options_keybindings_bn_skill_3->setPosition(tgui::Layout2d(tgui::bindWidth(_data->gui) / 2.0, y + step * row));
		options_keybindings_bn_skill_3->connect("pressed", &options_keybindings::_get_key, this, "skill_3", "options_keybindings_bn_skill_3");
		_data->gui.add(options_keybindings_bn_skill_3, "options_keybindings_bn_skill_3");
		_widgets.push_back("options_keybindings_bn_skill_3");
		_move_at_scroll.push_back(std::make_pair("options_keybindings_lbl_skill_3", "options_keybindings_bn_skill_3"));

		row++;

		// label skill_4
		tgui::Label::Ptr options_keybindings_lbl_skill_4 = theme->load("Label");
		options_keybindings_lbl_skill_4->setText("Skill 4");
		options_keybindings_lbl_skill_4->setPosition(tgui::Layout2d(tgui::bindWidth(_data->gui) / 2.0 - tgui::bindWidth(options_keybindings_lbl_skill_4) - 120, y + step * row));
		_data->gui.add(options_keybindings_lbl_skill_4, "options_keybindings_lbl_skill_4");
		_widgets.push_back("options_keybindings_lbl_skill_4");

		// button skill_4
		tgui::Button::Ptr options_keybindings_bn_skill_4 = tgui::Button::create();
		options_keybindings_bn_skill_4->setText(_data->config.convert_key_to_str((_data->settings.keyboard.check_existance("skill_4")) ? _data->settings.keyboard.keybindings.at("skill_4") : sf::Keyboard::Unknown));
		options_keybindings_bn_skill_4->setPosition(tgui::Layout2d(tgui::bindWidth(_data->gui) / 2.0, y + step * row));
		options_keybindings_bn_skill_4->connect("pressed", &options_keybindings::_get_key, this, "skill_4", "options_keybindings_bn_skill_4");
		_data->gui.add(options_keybindings_bn_skill_4, "options_keybindings_bn_skill_4");
		_widgets.push_back("options_keybindings_bn_skill_4");
		_move_at_scroll.push_back(std::make_pair("options_keybindings_lbl_skill_4", "options_keybindings_bn_skill_4"));

		row++;

		// label skill_5
		tgui::Label::Ptr options_keybindings_lbl_skill_5 = theme->load("Label");
		options_keybindings_lbl_skill_5->setText("Skill 5");
		options_keybindings_lbl_skill_5->setPosition(tgui::Layout2d(tgui::bindWidth(_data->gui) / 2.0 - tgui::bindWidth(options_keybindings_lbl_skill_5) - 120, y + step * row));
		_data->gui.add(options_keybindings_lbl_skill_5, "options_keybindings_lbl_skill_5");
		_widgets.push_back("options_keybindings_lbl_skill_5");

		// button skill_5
		tgui::Button::Ptr options_keybindings_bn_skill_5 = tgui::Button::create();
		options_keybindings_bn_skill_5->setText(_data->config.convert_key_to_str((_data->settings.keyboard.check_existance("skill_5")) ? _data->settings.keyboard.keybindings.at("skill_5") : sf::Keyboard::Unknown));
		options_keybindings_bn_skill_5->setPosition(tgui::Layout2d(tgui::bindWidth(_data->gui) / 2.0, y + step * row));
		options_keybindings_bn_skill_5->connect("pressed", &options_keybindings::_get_key, this, "skill_5", "options_keybindings_bn_skill_5");
		_data->gui.add(options_keybindings_bn_skill_5, "options_keybindings_bn_skill_5");
		_widgets.push_back("options_keybindings_bn_skill_5");
		_move_at_scroll.push_back(std::make_pair("options_keybindings_lbl_skill_5", "options_keybindings_bn_skill_5"));

		row++;

		// label skill_6
		tgui::Label::Ptr options_keybindings_lbl_skill_6 = theme->load("Label");
		options_keybindings_lbl_skill_6->setText("Skill 6");
		options_keybindings_lbl_skill_6->setPosition(tgui::Layout2d(tgui::bindWidth(_data->gui) / 2.0 - tgui::bindWidth(options_keybindings_lbl_skill_6) - 120, y + step * row));
		_data->gui.add(options_keybindings_lbl_skill_6, "options_keybindings_lbl_skill_6");
		_widgets.push_back("options_keybindings_lbl_skill_6");

		// button skill_6
		tgui::Button::Ptr options_keybindings_bn_skill_6 = tgui::Button::create();
		options_keybindings_bn_skill_6->setText(_data->config.convert_key_to_str((_data->settings.keyboard.check_existance("skill_6")) ? _data->settings.keyboard.keybindings.at("skill_6") : sf::Keyboard::Unknown));
		options_keybindings_bn_skill_6->setPosition(tgui::Layout2d(tgui::bindWidth(_data->gui) / 2.0, y + step * row));
		options_keybindings_bn_skill_6->connect("pressed", &options_keybindings::_get_key, this, "skill_6", "options_keybindings_bn_skill_6");
		_data->gui.add(options_keybindings_bn_skill_6, "options_keybindings_bn_skill_6");
		_widgets.push_back("options_keybindings_bn_skill_6");
		_move_at_scroll.push_back(std::make_pair("options_keybindings_lbl_skill_6", "options_keybindings_bn_skill_6"));

		row++;

		// label skill_7
		tgui::Label::Ptr options_keybindings_lbl_skill_7 = theme->load("Label");
		options_keybindings_lbl_skill_7->setText("Skill 7");
		options_keybindings_lbl_skill_7->setPosition(tgui::Layout2d(tgui::bindWidth(_data->gui) / 2.0 - tgui::bindWidth(options_keybindings_lbl_skill_7) - 120, y + step * row));
		_data->gui.add(options_keybindings_lbl_skill_7, "options_keybindings_lbl_skill_7");
		_widgets.push_back("options_keybindings_lbl_skill_7");

		// button skill_7
		tgui::Button::Ptr options_keybindings_bn_skill_7 = tgui::Button::create();
		options_keybindings_bn_skill_7->setText(_data->config.convert_key_to_str((_data->settings.keyboard.check_existance("skill_7")) ? _data->settings.keyboard.keybindings.at("skill_7") : sf::Keyboard::Unknown));
		options_keybindings_bn_skill_7->setPosition(tgui::Layout2d(tgui::bindWidth(_data->gui) / 2.0, y + step * row));
		options_keybindings_bn_skill_7->connect("pressed", &options_keybindings::_get_key, this, "skill_7", "options_keybindings_bn_skill_7");
		_data->gui.add(options_keybindings_bn_skill_7, "options_keybindings_bn_skill_7");
		_widgets.push_back("options_keybindings_bn_skill_7");
		_move_at_scroll.push_back(std::make_pair("options_keybindings_lbl_skill_7", "options_keybindings_bn_skill_7"));

		row++;

		// label skill_8
		tgui::Label::Ptr options_keybindings_lbl_skill_8 = theme->load("Label");
		options_keybindings_lbl_skill_8->setText("Skill 8");
		options_keybindings_lbl_skill_8->setPosition(tgui::Layout2d(tgui::bindWidth(_data->gui) / 2.0 - tgui::bindWidth(options_keybindings_lbl_skill_8) - 120, y + step * row));
		_data->gui.add(options_keybindings_lbl_skill_8, "options_keybindings_lbl_skill_8");
		_widgets.push_back("options_keybindings_lbl_skill_8");

		// button skill_8
		tgui::Button::Ptr options_keybindings_bn_skill_8 = tgui::Button::create();
		options_keybindings_bn_skill_8->setText(_data->config.convert_key_to_str((_data->settings.keyboard.check_existance("skill_8")) ? _data->settings.keyboard.keybindings.at("skill_8") : sf::Keyboard::Unknown));
		options_keybindings_bn_skill_8->setPosition(tgui::Layout2d(tgui::bindWidth(_data->gui) / 2.0, y + step * row));
		options_keybindings_bn_skill_8->connect("pressed", &options_keybindings::_get_key, this, "skill_8", "options_keybindings_bn_skill_8");
		_data->gui.add(options_keybindings_bn_skill_8, "options_keybindings_bn_skill_8");
		_widgets.push_back("options_keybindings_bn_skill_8");
		_move_at_scroll.push_back(std::make_pair("options_keybindings_lbl_skill_8", "options_keybindings_bn_skill_8"));

		row++;

		// label skill_9
		tgui::Label::Ptr options_keybindings_lbl_skill_9 = theme->load("Label");
		options_keybindings_lbl_skill_9->setText("Skill 9");
		options_keybindings_lbl_skill_9->setPosition(tgui::Layout2d(tgui::bindWidth(_data->gui) / 2.0 - tgui::bindWidth(options_keybindings_lbl_skill_9) - 120, y + step * row));
		_data->gui.add(options_keybindings_lbl_skill_9, "options_keybindings_lbl_skill_9");
		_widgets.push_back("options_keybindings_lbl_skill_9");

		// button skill_9
		tgui::Button::Ptr options_keybindings_bn_skill_9 = tgui::Button::create();
		options_keybindings_bn_skill_9->setText(_data->config.convert_key_to_str((_data->settings.keyboard.check_existance("skill_9")) ? _data->settings.keyboard.keybindings.at("skill_9") : sf::Keyboard::Unknown));
		options_keybindings_bn_skill_9->setPosition(tgui::Layout2d(tgui::bindWidth(_data->gui) / 2.0, y + step * row));
		options_keybindings_bn_skill_9->connect("pressed", &options_keybindings::_get_key, this, "skill_9", "options_keybindings_bn_skill_9");
		_data->gui.add(options_keybindings_bn_skill_9, "options_keybindings_bn_skill_9");
		_widgets.push_back("options_keybindings_bn_skill_9");
		_move_at_scroll.push_back(std::make_pair("options_keybindings_lbl_skill_9", "options_keybindings_bn_skill_9"));

		row++;

		// label skill_10
		tgui::Label::Ptr options_keybindings_lbl_skill_10 = theme->load("Label");
		options_keybindings_lbl_skill_10->setText("Skill 10");
		options_keybindings_lbl_skill_10->setPosition(tgui::Layout2d(tgui::bindWidth(_data->gui) / 2.0 - tgui::bindWidth(options_keybindings_lbl_skill_10) - 120, y + step * row));
		_data->gui.add(options_keybindings_lbl_skill_10, "options_keybindings_lbl_skill_10");
		_widgets.push_back("options_keybindings_lbl_skill_10");

		// button skill_10
		tgui::Button::Ptr options_keybindings_bn_skill_10 = tgui::Button::create();
		options_keybindings_bn_skill_10->setText(_data->config.convert_key_to_str((_data->settings.keyboard.check_existance("skill_10")) ? _data->settings.keyboard.keybindings.at("skill_10") : sf::Keyboard::Unknown));
		options_keybindings_bn_skill_10->setPosition(tgui::Layout2d(tgui::bindWidth(_data->gui) / 2.0, y + step * row));
		options_keybindings_bn_skill_10->connect("pressed", &options_keybindings::_get_key, this, "skill_10", "options_keybindings_bn_skill_10");
		_data->gui.add(options_keybindings_bn_skill_10, "options_keybindings_bn_skill_10");
		_widgets.push_back("options_keybindings_bn_skill_10");
		_move_at_scroll.push_back(std::make_pair("options_keybindings_lbl_skill_10", "options_keybindings_bn_skill_10"));

		_move_widgets(0.f); // move by 0 and hide all widgets outsie of visible area
	}

	void options_keybindings::_exit()
	{
		_data->states.remove();
		_clear_widgets();
	}

	void options_keybindings::_clear_widgets()
	{
		// delete the states widgets
		for (unsigned int i = 0; i < _widgets.size(); i++)
		{
			_data->gui.remove(_data->gui.get(_widgets.at(i)));
		}
	}

	void options_keybindings::_hide_widgets()
	{
		// delete the states widgets
		for (unsigned int i = 0; i < _widgets.size(); i++)
		{
			_data->gui.get(_widgets.at(i))->hide();
		}
	}

	void options_keybindings::_show_widgets()
	{
		// delete the states widgets
		for (unsigned int i = 0; i < _widgets.size(); i++)
		{
			_data->gui.get(_widgets.at(i))->show();
		}
	}

	void options_keybindings::_get_key(std::string const& action, std::string const& button)
	{
		tgui::Label::Ptr otions_keybindings_lbl_input = _data->gui.get<tgui::Label>("otions_keybindings_lbl_input");
		otions_keybindings_lbl_input->setText("Please press a keyboard key!");

		bool key_pressed = false;
		sf::Keyboard::Key tmp;

		sf::Clock ilb_clock; // infinite loop break clock

		ilb_clock.restart();

		while ((!key_pressed) && (ilb_clock.getElapsedTime().asSeconds() <= _KEYINPUTTIME))
		{
			// set text with countdown
			otions_keybindings_lbl_input->setText("Please press a keyboard key! (" + lc::utils::string_padding(std::to_string(_KEYINPUTTIME - ilb_clock.getElapsedTime().asSeconds()), 4, ' ') + "s)");
			draw(0.f); // draw gui to display text

			tmp = _get_key_input();

			if (tmp != sf::Keyboard::Unknown)
			{ // key was pressed
				key_pressed = true;

				if(!_is_key_taken(tmp))
				{ // key is already taken
					_data->input.change_bind(action, tmp);
					_data->settings.keyboard.replace(action, tmp);
#if _USINGDB
					_data->config.upd_db_key(_data->settings.paths.config_path, tmp, "KEYBOARD", action);
#elif _USINGINI
					_data->config.upd_ini_key(_data->settings.paths.config_path, tmp, "KEYBOARD", action);
#endif
				}
				else
				{
					_key_taken_clock.restart();
					_data->gui.get<tgui::Label>("otions_keybindings_lbl_key_taken")->setText("Key: " + _data->config.convert_key_to_str(tmp) + " already taken!");
				}
			}
		}

		if (key_pressed)
		{ // set button text new
			tgui::Button::Ptr button_to_change = _data->gui.get<tgui::Button>(button);
			button_to_change->setText(_data->config.convert_key_to_str((_data->settings.keyboard.check_existance(action)) ? _data->settings.keyboard.keybindings.at(action) : sf::Keyboard::Unknown));
		}

		otions_keybindings_lbl_input->setText("");
	}

	bool options_keybindings::_is_key_taken(sf::Keyboard::Key const& key)
	{
		if (_data->settings.keyboard.check_existance(key))
		{
			return true;
		}

		return false;
	}

	sf::Keyboard::Key options_keybindings::_get_key_input()
	{
		if (sf::Keyboard::isKeyPressed(sf::Keyboard::A))
		{
			return sf::Keyboard::A;
		}

		if (sf::Keyboard::isKeyPressed(sf::Keyboard::B))
		{
			return sf::Keyboard::B;
		}

		if (sf::Keyboard::isKeyPressed(sf::Keyboard::C))
		{
			return sf::Keyboard::C;
		}

		if (sf::Keyboard::isKeyPressed(sf::Keyboard::D))
		{
			return sf::Keyboard::D;
		}

		if (sf::Keyboard::isKeyPressed(sf::Keyboard::E))
		{
			return sf::Keyboard::E;
		}

		if (sf::Keyboard::isKeyPressed(sf::Keyboard::F))
		{
			return sf::Keyboard::F;
		}

		if (sf::Keyboard::isKeyPressed(sf::Keyboard::G))
		{
			return sf::Keyboard::G;
		}

		if (sf::Keyboard::isKeyPressed(sf::Keyboard::H))
		{
			return sf::Keyboard::H;
		}

		if (sf::Keyboard::isKeyPressed(sf::Keyboard::I))
		{
			return sf::Keyboard::I;
		}

		if (sf::Keyboard::isKeyPressed(sf::Keyboard::J))
		{
			return sf::Keyboard::J;
		}

		if (sf::Keyboard::isKeyPressed(sf::Keyboard::K))
		{
			return sf::Keyboard::K;
		}

		if (sf::Keyboard::isKeyPressed(sf::Keyboard::L))
		{
			return sf::Keyboard::L;
		}

		if (sf::Keyboard::isKeyPressed(sf::Keyboard::M))
		{
			return sf::Keyboard::M;
		}

		if (sf::Keyboard::isKeyPressed(sf::Keyboard::N))
		{
			return sf::Keyboard::N;
		}

		if (sf::Keyboard::isKeyPressed(sf::Keyboard::O))
		{
			return sf::Keyboard::O;
		}

		if (sf::Keyboard::isKeyPressed(sf::Keyboard::P))
		{
			return sf::Keyboard::P;
		}

		if (sf::Keyboard::isKeyPressed(sf::Keyboard::Q))
		{
			return sf::Keyboard::Q;
		}

		if (sf::Keyboard::isKeyPressed(sf::Keyboard::R))
		{
			return sf::Keyboard::R;
		}

		if (sf::Keyboard::isKeyPressed(sf::Keyboard::S))
		{
			return sf::Keyboard::S;
		}

		if (sf::Keyboard::isKeyPressed(sf::Keyboard::T))
		{
			return sf::Keyboard::T;
		}

		if (sf::Keyboard::isKeyPressed(sf::Keyboard::U))
		{
			return sf::Keyboard::U;
		}

		if (sf::Keyboard::isKeyPressed(sf::Keyboard::V))
		{
			return sf::Keyboard::V;
		}

		if (sf::Keyboard::isKeyPressed(sf::Keyboard::W))
		{
			return sf::Keyboard::W;
		}

		if (sf::Keyboard::isKeyPressed(sf::Keyboard::X))
		{
			return sf::Keyboard::X;
		}

		if (sf::Keyboard::isKeyPressed(sf::Keyboard::Y))
		{
			return sf::Keyboard::Y;
		}

		if (sf::Keyboard::isKeyPressed(sf::Keyboard::Z))
		{
			return sf::Keyboard::Z;
		}

		if (sf::Keyboard::isKeyPressed(sf::Keyboard::Num0))
		{
			return sf::Keyboard::Num0;
		}

		if (sf::Keyboard::isKeyPressed(sf::Keyboard::Num1))
		{
			return sf::Keyboard::Num1;
		}

		if (sf::Keyboard::isKeyPressed(sf::Keyboard::Num2))
		{
			return sf::Keyboard::Num2;
		}

		if (sf::Keyboard::isKeyPressed(sf::Keyboard::Num3))
		{
			return sf::Keyboard::Num3;
		}

		if (sf::Keyboard::isKeyPressed(sf::Keyboard::Num4))
		{
			return sf::Keyboard::Num4;
		}

		if (sf::Keyboard::isKeyPressed(sf::Keyboard::Num5))
		{
			return sf::Keyboard::Num5;
		}

		if (sf::Keyboard::isKeyPressed(sf::Keyboard::Num6))
		{
			return sf::Keyboard::Num6;
		}

		if (sf::Keyboard::isKeyPressed(sf::Keyboard::Num7))
		{
			return sf::Keyboard::Num7;
		}

		if (sf::Keyboard::isKeyPressed(sf::Keyboard::Num8))
		{
			return sf::Keyboard::Num8;
		}

		if (sf::Keyboard::isKeyPressed(sf::Keyboard::Num9))
		{
			return sf::Keyboard::Num9;
		}

		if (sf::Keyboard::isKeyPressed(sf::Keyboard::Escape))
		{
			return sf::Keyboard::Escape;
		}

		if (sf::Keyboard::isKeyPressed(sf::Keyboard::LControl))
		{
			return sf::Keyboard::LControl;
		}

		if (sf::Keyboard::isKeyPressed(sf::Keyboard::LShift))
		{
			return sf::Keyboard::LShift;
		}

		if (sf::Keyboard::isKeyPressed(sf::Keyboard::LAlt))
		{
			return sf::Keyboard::LAlt;
		}

		if (sf::Keyboard::isKeyPressed(sf::Keyboard::LSystem))
		{
			return sf::Keyboard::LSystem;
		}

		if (sf::Keyboard::isKeyPressed(sf::Keyboard::RControl))
		{
			return sf::Keyboard::RControl;
		}

		if (sf::Keyboard::isKeyPressed(sf::Keyboard::RShift))
		{
			return sf::Keyboard::RShift;
		}

		if (sf::Keyboard::isKeyPressed(sf::Keyboard::RAlt))
		{
			return sf::Keyboard::RAlt;
		}

		if (sf::Keyboard::isKeyPressed(sf::Keyboard::RSystem))
		{
			return sf::Keyboard::RSystem;
		}

		if (sf::Keyboard::isKeyPressed(sf::Keyboard::Menu))
		{
			return sf::Keyboard::Menu;
		}

		if (sf::Keyboard::isKeyPressed(sf::Keyboard::LBracket))
		{
			return sf::Keyboard::LBracket;
		}

		if (sf::Keyboard::isKeyPressed(sf::Keyboard::RBracket))
		{
			return sf::Keyboard::RBracket;
		}

		if (sf::Keyboard::isKeyPressed(sf::Keyboard::SemiColon))
		{
			return sf::Keyboard::SemiColon;
		}

		if (sf::Keyboard::isKeyPressed(sf::Keyboard::Comma))
		{
			return sf::Keyboard::Comma;
		}

		if (sf::Keyboard::isKeyPressed(sf::Keyboard::Period))
		{
			return sf::Keyboard::Period;
		}

		if (sf::Keyboard::isKeyPressed(sf::Keyboard::Quote))
		{
			return sf::Keyboard::Quote;
		}

		if (sf::Keyboard::isKeyPressed(sf::Keyboard::Slash))
		{
			return sf::Keyboard::Slash;
		}

		if (sf::Keyboard::isKeyPressed(sf::Keyboard::BackSlash))
		{
			return sf::Keyboard::BackSlash;
		}

		if (sf::Keyboard::isKeyPressed(sf::Keyboard::Tilde))
		{
			return sf::Keyboard::Tilde;
		}

		if (sf::Keyboard::isKeyPressed(sf::Keyboard::Equal))
		{
			return sf::Keyboard::Equal;
		}

		if (sf::Keyboard::isKeyPressed(sf::Keyboard::Dash))
		{
			return sf::Keyboard::Dash;
		}

		if (sf::Keyboard::isKeyPressed(sf::Keyboard::Space))
		{
			return sf::Keyboard::Space;
		}

		if (sf::Keyboard::isKeyPressed(sf::Keyboard::Return))
		{
			return sf::Keyboard::Return;
		}

		if (sf::Keyboard::isKeyPressed(sf::Keyboard::BackSpace))
		{
			return sf::Keyboard::BackSpace;
		}

		if (sf::Keyboard::isKeyPressed(sf::Keyboard::Tab))
		{
			return sf::Keyboard::Tab;
		}

		if (sf::Keyboard::isKeyPressed(sf::Keyboard::PageUp))
		{
			return sf::Keyboard::PageUp;
		}

		if (sf::Keyboard::isKeyPressed(sf::Keyboard::PageDown))
		{
			return sf::Keyboard::PageDown;
		}

		if (sf::Keyboard::isKeyPressed(sf::Keyboard::End))
		{
			return sf::Keyboard::End;
		}

		if (sf::Keyboard::isKeyPressed(sf::Keyboard::Home))
		{
			return sf::Keyboard::Home;
		}

		if (sf::Keyboard::isKeyPressed(sf::Keyboard::Insert))
		{
			return sf::Keyboard::Insert;
		}

		if (sf::Keyboard::isKeyPressed(sf::Keyboard::Delete))
		{
			return sf::Keyboard::Delete;
		}

		if (sf::Keyboard::isKeyPressed(sf::Keyboard::Add))
		{
			return sf::Keyboard::Add;
		}

		if (sf::Keyboard::isKeyPressed(sf::Keyboard::Subtract))
		{
			return sf::Keyboard::Subtract;
		}

		if (sf::Keyboard::isKeyPressed(sf::Keyboard::Multiply))
		{
			return sf::Keyboard::Multiply;
		}

		if (sf::Keyboard::isKeyPressed(sf::Keyboard::Divide))
		{
			return sf::Keyboard::Divide;
		}

		if (sf::Keyboard::isKeyPressed(sf::Keyboard::Left))
		{
			return sf::Keyboard::Left;
		}

		if (sf::Keyboard::isKeyPressed(sf::Keyboard::Right))
		{
			return sf::Keyboard::Right;
		}

		if (sf::Keyboard::isKeyPressed(sf::Keyboard::Up))
		{
			return sf::Keyboard::Up;
		}

		if (sf::Keyboard::isKeyPressed(sf::Keyboard::Down))
		{
			return sf::Keyboard::Down;
		}

		if (sf::Keyboard::isKeyPressed(sf::Keyboard::Numpad0))
		{
			return sf::Keyboard::Numpad0;
		}

		if (sf::Keyboard::isKeyPressed(sf::Keyboard::Numpad1))
		{
			return sf::Keyboard::Numpad1;
		}

		if (sf::Keyboard::isKeyPressed(sf::Keyboard::Numpad2))
		{
			return sf::Keyboard::Numpad2;
		}

		if (sf::Keyboard::isKeyPressed(sf::Keyboard::Numpad3))
		{
			return sf::Keyboard::Numpad3;
		}

		if (sf::Keyboard::isKeyPressed(sf::Keyboard::Numpad4))
		{
			return sf::Keyboard::Numpad4;
		}

		if (sf::Keyboard::isKeyPressed(sf::Keyboard::Numpad5))
		{
			return sf::Keyboard::Numpad5;
		}

		if (sf::Keyboard::isKeyPressed(sf::Keyboard::Numpad6))
		{
			return sf::Keyboard::Numpad6;
		}

		if (sf::Keyboard::isKeyPressed(sf::Keyboard::Numpad7))
		{
			return sf::Keyboard::Numpad7;
		}

		if (sf::Keyboard::isKeyPressed(sf::Keyboard::Numpad8))
		{
			return sf::Keyboard::Numpad8;
		}

		if (sf::Keyboard::isKeyPressed(sf::Keyboard::Numpad9))
		{
			return sf::Keyboard::Numpad9;
		}

		if (sf::Keyboard::isKeyPressed(sf::Keyboard::F1))
		{
			return sf::Keyboard::F1;
		}

		if (sf::Keyboard::isKeyPressed(sf::Keyboard::F2))
		{
			return sf::Keyboard::F2;
		}

		if (sf::Keyboard::isKeyPressed(sf::Keyboard::F3))
		{
			return sf::Keyboard::F3;
		}

		if (sf::Keyboard::isKeyPressed(sf::Keyboard::F4))
		{
			return sf::Keyboard::F4;
		}

		if (sf::Keyboard::isKeyPressed(sf::Keyboard::F5))
		{
			return sf::Keyboard::F5;
		}

		if (sf::Keyboard::isKeyPressed(sf::Keyboard::F6))
		{
			return sf::Keyboard::F6;
		}

		if (sf::Keyboard::isKeyPressed(sf::Keyboard::F7))
		{
			return sf::Keyboard::F7;
		}

		if (sf::Keyboard::isKeyPressed(sf::Keyboard::F8))
		{
			return sf::Keyboard::F8;
		}

		if (sf::Keyboard::isKeyPressed(sf::Keyboard::F9))
		{
			return sf::Keyboard::F9;
		}

		if (sf::Keyboard::isKeyPressed(sf::Keyboard::F10))
		{
			return sf::Keyboard::F10;
		}

		if (sf::Keyboard::isKeyPressed(sf::Keyboard::F11))
		{
			return sf::Keyboard::F11;
		}

		if (sf::Keyboard::isKeyPressed(sf::Keyboard::F12))
		{
			return sf::Keyboard::F12;
		}

		if (sf::Keyboard::isKeyPressed(sf::Keyboard::F13))
		{
			return sf::Keyboard::F13;
		}

		if (sf::Keyboard::isKeyPressed(sf::Keyboard::F14))
		{
			return sf::Keyboard::F14;
		}

		if (sf::Keyboard::isKeyPressed(sf::Keyboard::F15))
		{
			return sf::Keyboard::F15;
		}

		if (sf::Keyboard::isKeyPressed(sf::Keyboard::Pause))
		{
			return sf::Keyboard::Pause;
		}

		return sf::Keyboard::Unknown;
	}
}
