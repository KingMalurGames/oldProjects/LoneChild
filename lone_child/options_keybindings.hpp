#ifndef __LC__OPTIONS_KEYBINDINGS__
#define __LC__OPTIONS_KEYBINDINGS__

// lc
#include "application.hpp"

// std
#include <memory>

// lib
#include <SFML/Graphics.hpp>

// NEngine
#include "../include/NEngine_1.0.0/nstate_manager/nstate.hpp"

namespace lc {

	class options_keybindings : public nengine::nstate_manager::nstate
	{
	public:
		options_keybindings(std::shared_ptr<game_data> data);
		~options_keybindings();
		void init();
		void pause();
		void resume();
		void handle();
		void update(float dt);
		void draw(float dt);

	private:
		std::shared_ptr<game_data> _data;
		std::vector<std::string> _widgets;
		std::vector<std::pair<std::string, std::string>> _move_at_scroll; // pair of two strings because two widgets belong together
		sf::RectangleShape _visible_area;
		sf::Clock _key_taken_clock;
		bool _scrolled_up;
		bool _scrolled_down;

		void _load_widgets();

		void _exit();

		void _clear_widgets();
		void _hide_widgets();
		void _show_widgets();
		void _move_widgets(float y_amount);

		void _get_key(std::string const& action, std::string const& button);
		sf::Keyboard::Key _get_key_input();
		bool _is_key_taken(sf::Keyboard::Key const& key);
	};

}

#endif
