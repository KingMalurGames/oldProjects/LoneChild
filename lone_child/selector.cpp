// lc
#include "selector.hpp"

// std

// lib
#include <TGUI/TGUI.hpp>

// NEngine

namespace lc {

	selector::selector(std::shared_ptr<game_data> data)
		: _data(data)
		, _widgets()
		, _items()
	{}

	selector::~selector()
	{
#if _DEBUG
		std::cerr << "destructor: selector" << std::endl;
#endif
		_clear_widgets();
	}

	void selector::load()
	{
#if _DEBUG
		std::cerr << "load: selector" << std::endl;
#endif
		// resources

		// gui
		_load_widgets();
	}

	void selector::hide()
	{}

	void selector::show()
	{}

	void selector::clear()
	{}

	void selector::clear_widgets()
	{}

	void selector::draw()
	{
		_data->window.setActive(true);

		//_data->window.clear(sf::Color::Black);

		//_data->gui.draw();

		//_data->window.display();

		_data->window.setActive(false);
	}

	void selector::add_item(std::string name, std::shared_ptr<sf::Texture> texture, sf::Vector2f position, sf::Vector2f size)
	{
		auto new_item = std::make_shared<selector_item>();
		new_item->name = name;
		new_item->texture = std::move(texture);
		new_item->sprite.setPosition(position);
		lc::utils::ratio ratio = lc::utils::get_scale_ratio(texture->getSize().x, texture->getSize().y, size.x, size.y);
		new_item->sprite.scale(ratio.x, ratio.y);
		new_item->size = size;
		_items.insert(std::make_pair(name, new_item));
	}

	selector_item selector::get_selected_item()
	{
		return *_items.at(_active_item);
	}

	void selector::_load_widgets()
	{
	}

	void selector::_clear_widgets()
	{
		// delete the states widgets
		for (unsigned int i = 0; i < _widgets.size(); i++)
		{
			_data->gui.remove(_data->gui.get(_widgets.at(i)));
		}
	}

	void selector::_hide_widgets()
	{
		// delete the states widgets
		for (unsigned int i = 0; i < _widgets.size(); i++)
		{
			_data->gui.get(_widgets.at(i))->hide();
		}
	}

	void selector::_show_widgets()
	{
		// delete the states widgets
		for (unsigned int i = 0; i < _widgets.size(); i++)
		{
			_data->gui.get(_widgets.at(i))->show();
		}
	}

}
