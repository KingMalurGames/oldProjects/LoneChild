#ifndef __LC__SELECTOR__
#define __LC__SELECTOR__

// lc
#include "application.hpp"

// std
#include <string>
#include <memory>

// lib
#include <SFML/Graphics.hpp>

// NEngine

namespace lc {

	struct selector_item
	{
		std::string name;
		sf::Sprite sprite;
		std::shared_ptr<sf::Texture> texture;
		sf::Vector2f position;
		sf::Vector2f size;
	};

	class selector
	{
	public:
		selector(std::shared_ptr<game_data> data);
		~selector();

		void load();
		void hide();
		void show();
		void clear();
		void clear_widgets();
		void draw();
		void add_item(std::string name, std::shared_ptr<sf::Texture> texture, sf::Vector2f position, sf::Vector2f size);
		selector_item get_selected_item();
	private:
		std::shared_ptr<game_data> _data;
		std::vector<std::string> _widgets;
		std::unordered_map<std::string, std::shared_ptr<selector_item>> _items;
		std::string _active_item;

		void _load_widgets();
		void _load_widgets_new_game();
		void _load_widgets_load_game();

		void _clear_widgets();
		void _hide_widgets();
		void _show_widgets();
	};

}

#endif
