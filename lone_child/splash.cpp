// lc
#include "splash.hpp"
#include "main_menu.hpp"
#include "utils.hpp"

// std
#include <iostream>
#include <thread>

// lib
#include <SFML/Graphics.hpp>

// NEngine
#include "../include/NEngine_1.0.0/nstate_manager/nstate.hpp"

namespace lc {

	splash::splash(std::shared_ptr<game_data> data)
		: _data(data)
		, _clock()
		, _logo()
	{
	}

	void splash::init()
	{
		// resources
		_data->resources.load_texture("lo_splash", _data->settings.paths.lo_splash);

		// members
		_logo.setTexture(_data->resources.get_texture("lo_splash"));
		lc::utils::ratio ratio =  lc::utils::get_scale_ratio(_data->resources.get_texture("lo_splash").getSize().x, _data->resources.get_texture("lo_splash").getSize().y, _data->window.getSize().x, _data->window.getSize().y);
		_logo.scale(sf::Vector2f(ratio.x, ratio.y));

		// TODO: load extended config here
	}

	void splash::pause()
	{
		// no pause
	}

	void splash::resume()
	{
		// no resume
	}

	void splash::handle()
	{
		sf::Event event;
		while (_data->window.pollEvent(event))
		{
			if (event.type == sf::Event::Closed)
			{
				// only force close
				_data->window.close();
			}
		}
	}

	void splash::update(float dt)
	{
		if (_clock.getElapsedTime().asSeconds() >= _data->settings.general.splash_time)
		{
			// start main menu and replace splash screen
			_data->states.add(std::unique_ptr<nengine::nstate_manager::nstate>(new lc::main_menu(_data)), true);
		}
	}

	void splash::draw(float dt)
	{
		_data->window.setActive(true);

		_data->window.clear(sf::Color::Black);

		_data->window.draw(_logo);

		_data->window.display();

		_data->window.setActive(false);
	}
}
