#ifndef __LC__SPLASH__
#define __LC__SPLASH__

// lc
#include "application.hpp"

// std
#include <memory>

// lib
#include <SFML/Graphics.hpp>

// NEngine
#include "../include/NEngine_1.0.0/nstate_manager/nstate.hpp"

namespace lc {

	class splash : public nengine::nstate_manager::nstate
	{
	public:
		splash(std::shared_ptr<game_data> data);
		void init();
		void pause();
		void resume();
		void handle();
		void update(float dt);
		void draw(float dt);

	private:
		std::shared_ptr<game_data> _data;
		sf::Clock _clock;
		sf::Sprite _logo;
	};

}

#endif
