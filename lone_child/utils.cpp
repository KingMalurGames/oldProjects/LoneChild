#include "utils.hpp"

// lc

// std
#include <regex>
#include <cctype>
#include <fstream>

// lib

// NEngine

namespace lc {

	namespace utils {

		void system(std::string const& command)
		{
			std::system(command.c_str());
		}

		ratio get_scale_ratio(unsigned int org_x, unsigned int org_y, unsigned int new_x, unsigned int new_y)
		{
			ratio tmp;

			tmp.x = (float) (new_x * 1.f) / (org_x * 1.f);
			tmp.y = (float) (new_y * 1.f) / (org_y * 1.f);

			return tmp;
		}

		bool file_existance(std::string const& path_to_file)
		{
			std::fstream f(path_to_file.c_str());
			return f.good();
		}

		void log(std::string const& path_to_log, std::string const& message)
		{
			// append to log
			std::ofstream log;

			log.open(path_to_log, std::ios_base::app);
			log << message;
		}

		void mkdir(std::string const& dir)
		{
#ifdef _WIN64 // Windows (64-bit)
			std::string command = "mkdir " + std::regex_replace(dir, std::regex("/"), "\\") + " 2> nul";
			lc::utils::system(command);
#elif _WIN32 // Windows (32-bit)
			std::string command = "mkdir " + std::regex_replace(dir, std::regex("/"), "\\") + " 2> nul";
			lc::utils::system(command);
#elif __APPLE__
#include "TargetConditionals.h"
#define TARGET_OS_OSX 1 // OSX
			lc::utils::system("mkdir -p " + dir.c_str());
#elif __linux // Linux
			lc::utils::system("mkdir -p " + dir.c_str() + " &>/dev/null");
#elif __unix // Unix
			lc::utils::system("mkdir -p " + dir.c_str() + " &>/dev/null");
#elif __posix // POSIX
			lc::utils::system("mkdir -p " + dir.c_str() + " &>/dev/null");
#endif
		}

		bool str_equal(std::string const& s1, std::string const& s2)
		{
			return std::strcmp(s1.c_str(), s2.c_str());
		}

		bool c_str_equal(const char *c1, const char *c2)
		{
			return std::strcmp(c1, c2);
		}

		void replace_all_substrings(std::string& str, std::string const& from, std::string const& to)
		{
			if (from.empty())
			{
				return;
			}

			if (str.empty())
			{
				return;
			}

			size_t start_pos = 0;
			while ((start_pos = str.find(from, start_pos)) != std::string::npos)
			{
				str.replace(start_pos, from.length(), to); // replace from start position n long with to
				start_pos += to.length();
			}
		}

		std::string string_padding(std::string const& original, unsigned int length, char const& fill_char)
		{
			std::string tmp = original;
			tmp.resize(size_t(length), fill_char);
			return tmp;
		}

		bool string_is_number(std::string const& s)
		{
			std::string::const_iterator it = s.begin();
			while (it != s.end() && std::isdigit(*it))
			{
				++it;
			}
			return !s.empty() && it == s.end();
		}

#if defined _WIN32 || defined _WIN64
#define NOMINMAX // for not including windows min and max functions
#include <windows.h>
#include <Shlwapi.h>

		std::string get_working_dir()
		{
			HMODULE hModule = GetModuleHandle(nullptr); // get base address
			if (!hModule) // check if it worked
			{
				return "";
			}

			char path[256]; // following functions need char array
			GetModuleFileName(hModule, path, sizeof(path)); // get base address as human readable

															// remove file from directory
			std::string cwd = std::string(path);
			if (cwd.empty())
			{
				return "";
			}
			std::reverse(cwd.begin(), cwd.end());
			if ((cwd.at(0) == '/') || (cwd.at(0) == '\\'))
			{
				std::reverse(cwd.begin(), cwd.end());
				return cwd;
			}
			unsigned int pos = 0;
			while ((cwd.at(0) != '/') && (cwd.at(0) != '\\'))
			{
				cwd.erase(0, 1); // replace from start position n long with to
			}
			std::reverse(cwd.begin(), cwd.end());

			return std::string(cwd);
		}

		std::vector<std::string> get_file_list(std::string const& dir, std::string const& pattern)
		{
			std::vector<std::string> files;

			// check if search pattern allowed
			if ((dir.empty()) || (pattern.empty()))
			{
				return files;
			}

			// check for correct format
			std::string path;
			if (std::to_string(dir.back()) != "/")
			{
				path = dir + "/" + pattern;
			}
			else
			{
				path = dir + pattern;
			}

			WIN32_FIND_DATA data; // windows specific "data" construct
			HANDLE found = FindFirstFile(path.c_str(), &data); // try to find first file

															   // if nothing found
			if (found == INVALID_HANDLE_VALUE)
			{
				return files;
			}

			do
			{
				if (!(data.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY)) // if no directory
				{
					files.emplace_back(data.cFileName);
				}
			} while (FindNextFile(found, &data)); // while a new file is found

			FindClose(found); // deallocate

			return files;
		}
#elif defined __linux || defined __unix || defined __posix
#include <unistd.h>
#include <dirent.h>

		std::string get_working_dir()
		{
			char cwd[1024]; // current working directory
			if (!getcwd(cwd, sizeof(cwd)) // get dir
			{
				return "";
			}
			return std::string(cwd) + std::string("/");
		}

		std::vector<std::string> get_file_list(std::string const& dir, std::string const& pattern)
		{
			std::vector<std::string> files;

			// check if search pattern allowed
			if ((dir.empty()) || (pattern.empty()))
			{
				return files;
			}

			DIR *dpdf; // directory pointer
			dpdf = opendir(dir.c_str()); // open dir

			if (!dpdf) // not opened
			{
				return files;
			}

			// some formatting for linux
			std::string s_pattern = pattern;
			if (s_pattern.at(0) == "*")
			{
				s_pattern.erase(s_pattern.begin());
			}
			if (s_pattern.at(s_pattern.length() - 1) == "*")
			{
				search.pop_back();
			}

			struct dirent *epdf; // data storage struct for reading
			while (epdf = readdir(dpdf))
			{
				if (epdf->d_type == DT_DIR) // if directory
				{
					continue;
				}

				// nope, totally a file
				std::string name = epdf->d_name;
				if (pattern != "*.*") // test stuff for non standard pattern
				{
					if (name.length() < s_pattern.length()) // name smaller than pattern -> no valid file
					{
						continue;
					}

					if (s_pattern.at(0) == ".") // hidden files
					{
						if (name.compare(name.length() - s_pattern.length(), s_pattern.length(), s_pattern) != 0)
						{
							continue;
						}
					}
					else if (name.find(s_pattern) == std::string::npos)
					{
						continue;
					}
				}
				files.emplace_back(name);
			}
			closedir(dpdf);
			return files;
		}
#endif

	}

}
