#ifndef __LC__UTILS__
#define __LC__UTILS__

// lc

// std
#include <string>
#include <vector>

// lib

// NEngine

namespace lc {

	namespace utils {

		struct ratio
		{
			float x;
			float y;
		};

		void system(std::string const& command);

		void mkdir(std::string const& dir);

		bool file_existance(std::string const& path_to_file);

		void log(std::string const& path_to_log, std::string const& message);

		bool str_equal(std::string const& s1, std::string const& s2);

		bool c_str_equal(const char *c1, const char *c2);

		void replace_all_substrings(std::string& str, std::string const& from, std::string const& to);

		std::string string_padding(std::string const& original, unsigned int length, char const& fill_char);

		bool string_is_number(std::string const& s);

		ratio get_scale_ratio(unsigned int org_x, unsigned int org_y, unsigned int new_x, unsigned int new_y);

#if defined _WIN32 || defined _WIN64
#define NOMINMAX // for not including windows min and max functions
#include <windows.h>
#include <Shlwapi.h>
		std::string get_working_dir();

		std::vector<std::string> get_file_list(std::string const& dir, std::string const& pattern = "*.*");
#elif defined __linux || defined __unix || defined __posix
#include <unistd.h>
#include <dirent.h>
		std::string get_working_dir();

		std::vector<std::string> get_file_list(std::string const& dir, std::string const& pattern = "*.*");
#endif

}

}

#endif
